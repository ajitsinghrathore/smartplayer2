package com.asr.smartaplayer

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.FragmentActivity
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SmartPlayerApplication: Application() {


    var currentActivity : Activity? = null

    var launcher : ActivityResultLauncher<IntentSenderRequest>? = null

    var intentLauncher: ActivityResultLauncher<Intent>? = null

    var currentIntentLauncherCallback: ((ActivityResult?) -> Unit)? = null

    var currentLauncherCallback: ((ActivityResult?) -> Unit)? = null




    fun executeOnIntentSender(intentSenderRequest: IntentSenderRequest, callback : (ActivityResult?) -> Unit){
        launcher?.let {
            currentLauncherCallback = callback
            it.launch(intentSenderRequest)
        }
    }

    fun executeIntentForResult(intent: Intent, callback: (ActivityResult?) -> Unit){
        Log.d("DRIVE", "executeIntentForResult: $currentActivity $intent")
        intentLauncher?.let {
            currentIntentLauncherCallback = callback
            it.launch(intent)
        }
    }

    private fun isActivityFromMyPackage(intent: Intent?): Boolean {
        // Check if the intent's component belongs to your application's package
        val componentName = intent?.component
        return componentName != null && componentName.className.startsWith(packageName)
    }

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                if (!isActivityFromMyPackage(activity.intent)) {
                    return
                }
                Log.d("DRIVE", "onActivityCreated: creating $activity ${activity.packageName}")
                currentActivity = activity
                if (activity is FragmentActivity){
                   launcher =  activity.registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
                       Log.d("DRIVE*", "onActivityCreated: $it")
                       currentLauncherCallback?.let { it1 -> it1(it) }
                    }
                    intentLauncher = activity.registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
                       currentIntentLauncherCallback?.let { it1 -> it1(it) }
                    }
                }
            }

            override fun onActivityStarted(activity: Activity) {
            }

            override fun onActivityResumed(activity: Activity) {
            }

            override fun onActivityPaused(activity: Activity) {
            }

            override fun onActivityStopped(activity: Activity) {
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityDestroyed(activity: Activity) {
                if (currentActivity == activity) {
                    Log.d("DRIVE", "onActivityDestroyed: destroying $activity")
                    currentActivity = null
                }
            }
        })


    }


}