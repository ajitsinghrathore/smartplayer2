package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.media3.ui.PlayerView


class FadePlayerView : PlayerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        // If the view is visible, apply a fade in animation
        if (visibility == VISIBLE) {
            animate().alpha(1f).setDuration(500).start()
        } else {
            animate().alpha(0f).setDuration(500).start()
        }
    }
}
