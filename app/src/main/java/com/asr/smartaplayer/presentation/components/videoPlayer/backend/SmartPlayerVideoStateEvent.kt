package com.asr.smartaplayer.presentation.components.videoPlayer.backend

enum class SmartPlayerVideoStateEvent {
     PLAYBACK_STARTED,
     PLAYBACK_START_REQUEST,
    PLAYBACK_PAUSED,
    PLAYBACK_RESUMED,
    PLAYBACK_STOPPED_REQUEST
}