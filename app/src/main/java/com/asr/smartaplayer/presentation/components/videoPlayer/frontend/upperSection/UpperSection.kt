package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.upperSection

import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.showToast


@Composable
fun UpperSection(
    modifier: Modifier,
    onExitImmersiveModeRequested: () -> Unit,
    onDrowsyStateDetectionClicked: (Boolean) -> Unit,
    onCommentShowClicked: (Boolean) -> Unit,
    onPIPRequest: () -> Unit,
    isDrowsyEnabled: Boolean,
    isCommentsEnabled: Boolean,
    showSubtitleSettings: () -> Unit,
    showVideoEffectsSettings: () -> Unit
    ) {


    Box(
        modifier = modifier
            .background(
                Brush.verticalGradient(
                    listOf(MaterialTheme.colorScheme.background, Color.Transparent)
                )
            )
            .padding(PaddingValues(start = 20.dp, end = 20.dp, top = 10.dp)),
        contentAlignment = Alignment.TopCenter
    ) {

        Row {
            GeneralIconButton(
                modifier = Modifier
                    .height(50.dp)
                    .aspectRatio(1f),
                onClick = onExitImmersiveModeRequested,
                iconId = R.drawable.baseline_arrow_back_ios_new_24,
                iconColor = MaterialTheme.colorScheme.onBackground,
                contentDescription = "exit immersive mode",
                paddingValues = PaddingValues(14.dp)
            )

            Row(
                modifier = Modifier
                    .weight(1f),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {

                GeneralIconButton(
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f),
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    onClick = {
                        onPIPRequest()
                    },
                    paddingValues = PaddingValues(14.dp),
                    contentDescription = "Enter picture in picture mode",
                    iconId = R.drawable.baseline_picture_in_picture_24
                )
                GeneralIconButton(
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f),
                    iconColor = if (isCommentsEnabled) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onBackground,
                    onClick = { onCommentShowClicked(!isCommentsEnabled) },
                    contentDescription = "Video Comments",
                    paddingValues = PaddingValues(14.dp),
                    iconId = R.drawable.baseline_comment_24
                )
                GeneralIconButton(
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f),
                    iconColor = if (isDrowsyEnabled) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onBackground,
                    onClick = { onDrowsyStateDetectionClicked(!isDrowsyEnabled) },
                    contentDescription = "Drowsy state detection",
                    iconId = R.drawable.baseline_face_24,
                    paddingValues = PaddingValues(14.dp)
                )

                GeneralIconButton(
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f),
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    onClick =showSubtitleSettings,
                    contentDescription = "select subtitle file",
                    iconId = R.drawable.icons8_subtitles_24,
                    paddingValues = PaddingValues(14.dp)
                )

                GeneralIconButton(
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f),
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    onClick =showVideoEffectsSettings,
                    contentDescription = "Apply visual effects",
                    iconId = R.drawable.icons8_visual_effects_24,
                    paddingValues = PaddingValues(14.dp)
                )


            }
        }

    }

}
