package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.lowerSection

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayBackSeekBar
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayBackSegmentsData
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.humanReadableDuration
import dev.vivvvek.seeker.Segment


@Composable
fun LowerSectionForPreview(
    modifier: Modifier,
    totalDuration: Long,
    currentValue: Long,
    bufferedPosition: Long,
    videoComments: List<VideoComment>,
    addComment: (String, Long) -> Unit,
    seekStarted: () -> Unit,
    seekEnded: () -> Unit,
    seekTo: (Long) -> Unit,
) {

    Column(
        modifier = modifier
            .background(
                Brush.verticalGradient(
                    listOf(Color.Transparent, MaterialTheme.colorScheme.background)
                )
            )
            .padding(bottom = 5.dp, start = 10.dp, end = 10.dp)
            .fillMaxWidth(),
        verticalArrangement = Arrangement.Bottom,
    ) {


        val segmentsData = remember(key1 = videoComments) {
            val allSegments = mutableListOf<Segment>()
            var minComment = Long.MAX_VALUE
            var maxComment = Long.MIN_VALUE
            for (comment in videoComments) {
                allSegments.add(
                    Segment(
                        name = comment.comment,
                        start = comment.playBackPosition.toFloat()
                    )
                )
                minComment = minOf(comment.playBackPosition, minComment)
                maxComment = maxOf(comment.playBackPosition, maxComment)
            }
            allSegments.add(0, Segment(start = 0f, name = "start"))
            PlayBackSegmentsData(
                segments = allSegments,
                minSegmentStart = minComment,
                maxSegmentStart = maxComment
            )
        }


        PlayBackSeekBar(
            modifier = Modifier
                .fillMaxWidth()
                .height(15.dp),
            seekStarted = seekStarted,
            seekEnded = seekEnded,
            seekTo = { seekTo(it.toLong()) },
            endValue = totalDuration.toFloat(),
            currentValue = currentValue.toFloat(),
            bufferedPosition = bufferedPosition.toFloat(),
            thumbRadius = 8.dp,
            trackHeight = 3.dp,
            allSegments = segmentsData.segments
        )

        Spacer(modifier = Modifier.height(5.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = humanReadableDuration(currentValue),
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 12.sp
            )

            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.wrapContentWidth()
            ) {

                GeneralIconButton(
                    modifier = Modifier.height(25.dp),
                    onClick = {
                        val previousComment =
                            segmentsData.segments.lastOrNull { it.start < currentValue - 1000 }
                        if (previousComment != null)
                            seekTo(previousComment.start.toLong())
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = androidx.media3.ui.R.drawable.exo_ic_chevron_left,
                    contentDescription = "go to previous bookmarked position",
                    enabled = currentValue > segmentsData.minSegmentStart
                )
                GeneralIconButton(
                    modifier = Modifier.height(25.dp),
                    onClick = { addComment("", currentValue) },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = R.drawable.baseline_bookmark_add_24,
                    contentDescription = "bookmark this position",
                )
                GeneralIconButton(
                    modifier = Modifier.height(25.dp),
                    onClick = {
                        val nextComment =
                            segmentsData.segments.firstOrNull { it.start > currentValue }
                        if (nextComment != null)
                            seekTo(nextComment.start.toLong())
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = androidx.media3.ui.R.drawable.exo_ic_chevron_right,
                    contentDescription = "go to next bookmarked position",
                    enabled = currentValue < segmentsData.maxSegmentStart
                )
            }

            Text(
                text = humanReadableDuration(totalDuration),
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 12.sp
            )
        }

    }


}



