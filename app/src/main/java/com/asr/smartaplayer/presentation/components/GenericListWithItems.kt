@file:OptIn(ExperimentalMaterialApi::class)

package com.asr.smartaplayer.presentation.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.asr.smartaplayer.R

@Composable
fun <T> GenericListWithItems(
    modifier: Modifier,
    listState: LazyListState,
    listOfItems: List<T>,
    loadMoreItems: (Boolean) -> Unit,
    isLoading: Boolean,
    paddingValues: PaddingValues = PaddingValues(0.dp),
    emptyStateAnimFile: Int = R.raw.no_data_available,
    emptyStateComposable: (@Composable () -> Unit)?  = null,
    headerSpace: Dp? = null,
    loadingItemIndicator: @Composable () -> Unit,
    drawListItem: @Composable (LazyItemScope, Int, T) -> Unit
) {


    val pullRefreshState = rememberPullRefreshState(
        isLoading,
        {
            loadMoreItems(true)
        },
    )

    Box(modifier = modifier.pullRefresh(pullRefreshState)) {

        LazyColumn(
            contentPadding = paddingValues,
            state = listState,
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {


            headerSpace?.let {
                item {
                    Spacer(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(headerSpace)
                    )
                }
            }


            itemsIndexed(items = listOfItems) { index, item ->
                if (index >= listOfItems.size - 10) {
                    loadMoreItems(false)
                }

                drawListItem(this, index, item)
            }

            if (isLoading) {
                items(count = 10) {
                    loadingItemIndicator()
                }
            }


            if (!isLoading && listOfItems.isEmpty()) {

                item {
                    if (emptyStateComposable == null) {
                        val composition by rememberLottieComposition(
                            LottieCompositionSpec.RawRes(
                                emptyStateAnimFile
                            )
                        )
                        LottieAnimation(
                            modifier = modifier,
                            composition = composition,
                            iterations = LottieConstants.IterateForever,
                        )
                    }else{
                        emptyStateComposable()
                    }
                }
            }

        }


        PullRefreshIndicator(
            isLoading,
            pullRefreshState,
            Modifier
                .align(Alignment.TopCenter)
                .padding(top = max(0.dp, headerSpace?:0.dp))
        )

    }


}