@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package com.asr.smartaplayer.presentation.screens.settings.components


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsListMultiSelect(
    modifier: Modifier = Modifier,
    title: String,
    items: List<String>,
    icon: Int?,
    confirmButton: String,
    useSelectedValuesAsSubtitle: Boolean = true,
    descriptionText: String? = null,
    onItemsSelected: (Set<String>) -> Unit,
    currentLySelectedValues: Set<String>
) {


    var showBottomSheet by remember { mutableStateOf(false) }
    val scrollState = rememberScrollState()
    val sheetState = rememberModalBottomSheetState()


    var selectedItems by remember {
        mutableStateOf(currentLySelectedValues)
    }

    val safeSubtitleText = if (useSelectedValuesAsSubtitle) {
        currentLySelectedValues.joinToString(separator = "\n") { it }
    } else descriptionText

    SettingsMenuLink(
        modifier = modifier,
        enabled = true,
        icon = {
            if (icon != null) {
                Image(
                    modifier = Modifier
                        .size(20.dp),
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }
        },
        title = title,
        subtitle = safeSubtitleText,
        onClick = { showBottomSheet = true },
    )

    if (!showBottomSheet) return


    val onAdd: (String) -> Unit = { selectedIndex ->
        val mutable = selectedItems.toMutableSet()
        mutable.add(selectedIndex)
        selectedItems = mutable
    }
    val onRemove: (String) -> Unit = { selectedIndex ->
        val mutable = selectedItems.toMutableSet()
        mutable.remove(selectedIndex)
        selectedItems = mutable
    }



    ModalBottomSheet(
        sheetState = sheetState,
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = {
            showBottomSheet = false
            selectedItems = currentLySelectedValues
        },
    ) {
        Column(modifier = Modifier.padding(20.dp)) {
            Text(
                text = title,
                color = MaterialTheme.colorScheme.onBackground,
                style = MaterialTheme.typography.bodyLarge
            )
            if (descriptionText?.isNotBlank() == true) {
                Spacer(modifier = Modifier.height(10.dp))
                androidx.compose.material.Text(
                    text = descriptionText,
                    color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.8f),
                    style = MaterialTheme.typography.bodySmall
                )
            }
            Column(
                modifier = Modifier.verticalScroll(scrollState)
            ) {
                items.forEachIndexed { _, item ->
                    val isSelected by rememberUpdatedState(newValue = selectedItems.contains(item))
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(56.dp)
                            .toggleable(
                                role = Role.Checkbox,
                                value = isSelected,
                                onValueChange = {
                                    if (isSelected) {
                                        onRemove(item)
                                    } else {
                                        onAdd(item)
                                    }
                                }
                            ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = item,
                            style = MaterialTheme.typography.bodyMedium,
                            modifier = Modifier.weight(1f)
                        )
                        Checkbox(
                            checked = isSelected,
                            onCheckedChange = null
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(10.dp))

            TextButton(
                modifier = Modifier.fillMaxWidth(),
                onClick = {
                    showBottomSheet = false
                    onItemsSelected(selectedItems, )
                },
            ) {
                Text(text = confirmButton)
            }

        }
    }

}