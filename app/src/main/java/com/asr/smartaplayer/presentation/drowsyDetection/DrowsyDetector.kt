package com.asr.smartaplayer.presentation.drowsyDetection

import android.content.Context
import android.util.Log
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class DrowsyDetector {

    private val faceOptions = FaceDetectorOptions
        .Builder()
        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
        .build()


    private val faceDetector = FaceDetection.getClient(faceOptions)


    fun registerFaceAnalysis(context: Context, lifecycleOwner: CameraLifeCycleOwner) {

        val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)

        cameraProviderFuture.addListener(
            {
                val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                val cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA

                val imageAnalyzer = ImageAnalysis.Builder()
                    .build()
                    .also {
                        it.setAnalyzer(
                            cameraExecutor,
                            FaceAnalyzer(
                                isAwake = { awake ->
                                    lifecycleOwner.updateDrowsyState(awake)
                                },
                                faceDetector = faceDetector
                            )
                        )
                    }

                try {
                    // Unbind use cases before rebinding
                    cameraProvider.unbindAll()

                    // Bind use cases to camera
                    cameraProvider.bindToLifecycle(
                        lifecycleOwner, cameraSelector, imageAnalyzer
                    )

                } catch (exc: Exception) {
                    Log.e("DROWSY", "Use case binding failed", exc)
                }


            },
            ContextCompat.getMainExecutor(context)
        )


    }


}