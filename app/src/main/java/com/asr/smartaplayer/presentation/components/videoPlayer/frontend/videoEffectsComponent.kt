package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.VisualEffectsSettings
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SeekBar
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsCheckbox

@Composable
fun VideoEffectsSettings(
    modifier: Modifier,
    visualEffectsSettings: VisualEffectsSettings,
    onVisualEffectsConfigUpdate: (VisualEffectsSettings) -> Unit
) {


    Column(modifier = modifier) {

        SettingsCheckbox(
            title = "Enable/Disable Visual effects",
            onCheckedChange = {
                onVisualEffectsConfigUpdate(
                    visualEffectsSettings.copy(enabled = !visualEffectsSettings.enabled)
                )
            },
            checked = visualEffectsSettings.enabled,
            icon = R.drawable.icons8_visual_effects_24,
            enabled = false
        )

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically
        ) {
            GeneralIconButton(
                modifier = Modifier.size(50.dp),
                enabled = visualEffectsSettings.enabled,
                onClick = {
                    onVisualEffectsConfigUpdate(visualEffectsSettings.copy(grayScaleEnabled = !visualEffectsSettings.grayScaleEnabled))
                },
                iconColor = if (visualEffectsSettings.grayScaleEnabled) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.icons8_grayscale_50,
                contentDescription = "adjust grayscale",
                backGround = if (visualEffectsSettings.grayScaleEnabled) MaterialTheme.colorScheme.surface else null,
                paddingValues = PaddingValues(14.dp)
            )

            GeneralIconButton(
                modifier = Modifier.size(50.dp),
                enabled = visualEffectsSettings.enabled,
                onClick = {
                    val updatedConfig =
                        visualEffectsSettings.copy(invertedColorsEnabled = !visualEffectsSettings.invertedColorsEnabled)
                    onVisualEffectsConfigUpdate(updatedConfig)
                },
                iconColor = if (visualEffectsSettings.invertedColorsEnabled) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.icons8_invert_colors_50,
                contentDescription = "adjust invert colors settings",
                backGround = if (visualEffectsSettings.invertedColorsEnabled) MaterialTheme.colorScheme.surface else null,
                paddingValues = PaddingValues(14.dp)
            )

            val currentRotation = visualEffectsSettings.videoTotalRotation
            val rotationEnabled = currentRotation != 0f

            GeneralIconButton(
                modifier = Modifier.size(50.dp),
                enabled = visualEffectsSettings.enabled,
                onClick = {
                    var updatedRotation = currentRotation + 90f
                    if (updatedRotation > 360f) {
                        updatedRotation = 0f
                    }
                    val updatedConfig =
                        visualEffectsSettings.copy(videoTotalRotation = updatedRotation)
                    onVisualEffectsConfigUpdate(updatedConfig)
                },
                iconColor = if (rotationEnabled) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.baseline_rotate_90_degrees_cw_24,
                contentDescription = "rotate video",
                backGround = if (rotationEnabled) MaterialTheme.colorScheme.surface else null,
                paddingValues = PaddingValues(14.dp)
            )

        }

        Spacer(modifier = Modifier.height(10.dp))


        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = rememberAsyncImagePainter(model = R.drawable.icons8_contrast_24),
                contentDescription = "adjust contrast",
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(50.dp).padding(14.dp)
            )

            Spacer(modifier = Modifier.width(10.dp))

            val tempContrastSize = remember {
                mutableFloatStateOf(visualEffectsSettings.contrast)
            }

            SeekBar(
                modifier = Modifier.weight(1f),
                endValue = 1f,
                currentValue = tempContrastSize.floatValue,
                startValue = -1f,
                seekStarted = {},
                enabled = visualEffectsSettings.enabled,
                seekTo = {
                    tempContrastSize.floatValue = it
                },
                seekEnded = {
                    onVisualEffectsConfigUpdate(
                        visualEffectsSettings.copy(contrast = tempContrastSize.floatValue)
                    )
                },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp)
            )
        }

        Spacer(modifier = Modifier.height(10.dp))


        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = rememberAsyncImagePainter(model = R.drawable.icons8_hue_50),
                contentDescription = "adjust hue",
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(50.dp).padding(14.dp)
            )

            Spacer(modifier = Modifier.width(10.dp))

            val tempHue = remember {
                mutableFloatStateOf(visualEffectsSettings.hue)
            }

            SeekBar(
                modifier = Modifier.weight(1f),
                endValue = 360f,
                currentValue = tempHue.floatValue,
                startValue = 0f,
                seekStarted = {},
                enabled = visualEffectsSettings.enabled,
                seekTo = {
                    tempHue.floatValue = it
                },
                seekEnded = {
                    onVisualEffectsConfigUpdate(
                        visualEffectsSettings.copy(hue = tempHue.floatValue)
                    )
                },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp)
            )
        }


        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = rememberAsyncImagePainter(model = R.drawable.icons8_saturation_48),
                contentDescription = "adjust saturation",
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(50.dp).padding(14.dp)
            )

            Spacer(modifier = Modifier.width(10.dp))

            val tempSaturation = remember {
                mutableFloatStateOf(visualEffectsSettings.saturation)
            }

            SeekBar(
                modifier = Modifier.weight(1f),
                endValue = 100f,
                currentValue = tempSaturation.floatValue,
                startValue = -100f,
                seekStarted = {},
                enabled = visualEffectsSettings.enabled,
                seekTo = {
                    tempSaturation.floatValue = it
                },
                seekEnded = {
                    onVisualEffectsConfigUpdate(
                        visualEffectsSettings.copy(saturation = tempSaturation.floatValue)
                    )
                },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp)
            )
        }


    }


}