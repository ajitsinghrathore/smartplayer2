@file:OptIn(ExperimentalPermissionsApi::class, ExperimentalPermissionsApi::class)

package com.asr.smartaplayer.presentation.screens.immersiveVideoPlayBack

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState


@Composable
fun CameraPermissionDialog(
    permissionState: PermissionState,
    showPermissionDialog: MutableState<Boolean>
){


    val message = "This feature keeps track of your face and play/pause  the video automatically for you when you are not looking at screen or you are drowsy . In  order to keep track of your face you need to give camera permission , please allow app to use your device default front camera "

    val impMessage = "It is totally safe and works without internet."

    AlertDialog(
        onDismissRequest = {
        },
        shape = RoundedCornerShape(15.dp),
        backgroundColor = MaterialTheme.colorScheme.surface,
        confirmButton = {
            TextButton(onClick = { permissionState.launchPermissionRequest() }) {
                Text(text = "Allow")
            }
        },
        text = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = message,
                    style = MaterialTheme.typography.bodySmall,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                    color = MaterialTheme.colorScheme.onSurface
                )
                Spacer(modifier = Modifier.height(20.dp))
                Text(
                    text = impMessage,
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.primary,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                )

            }
        },
        title = {
            Text(
                text = "Allow Camera Access",
                style = MaterialTheme.typography.titleMedium,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth(),
                color = MaterialTheme.colorScheme.onSurface

            )
        },
        properties = DialogProperties(),
        dismissButton = {
            TextButton(onClick = { showPermissionDialog.value = false }) {
                Text(text = "Not Now")
            }
        }

    )




}
