package com.asr.smartaplayer.presentation.drowsyDetection

import android.annotation.SuppressLint
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetector


class FaceAnalyzer(
    val isAwake: (Boolean) -> Unit,
    private val faceDetector: FaceDetector
): ImageAnalysis.Analyzer {



    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(image: ImageProxy) {
        Log.d("DROWSY", "invoke: got image for analysis")
        val mediaImage = image.image
        if (mediaImage != null) {
            val inputImage = InputImage.fromMediaImage(mediaImage, image.imageInfo.rotationDegrees)
            faceDetector.process(inputImage).addOnSuccessListener { faces ->
                Log.d("TAG***", "analyze: ")
                if(faces.isEmpty()){
                    isAwake(false)
                }
                else{
                    val face = faces[0]
                    val rotY = face.headEulerAngleY
                    val rotZ = face.headEulerAngleZ
                    val rotX = face.headEulerAngleX
                    Log.d("TAG****", "analyze: $rotX $rotY $rotZ  ${face.leftEyeOpenProbability}  ${face.rightEyeOpenProbability}")
                    val left = face.leftEyeOpenProbability?:0f
                    val right = face.rightEyeOpenProbability?:0f
                    isAwake((left > 0.5 || right > 0.5) && rotY < 30 && rotY> -30)
                }
                image.close()
            }

        }
    }


}