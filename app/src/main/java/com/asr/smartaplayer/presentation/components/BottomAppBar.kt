package com.asr.smartaplayer.presentation.components

import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.presentation.AppDestination
import com.exyte.animatednavbar.AnimatedNavigationBar
import com.exyte.animatednavbar.animation.balltrajectory.Parabolic
import com.exyte.animatednavbar.animation.indendshape.Height
import com.exyte.animatednavbar.items.dropletbutton.DropletButton
import com.exyte.animatednavbar.utils.noRippleClickable

@Composable
fun BottomBar(
    allDestinations: List<AppDestination>,
    navigateToScreen: (AppDestination) -> Unit,
    selectedIndex: Int
) {

    AnimatedNavigationBar(
        modifier = Modifier
            .height(70.dp)
            .background(Color.Transparent),
        selectedIndex = selectedIndex,
        ballAnimation = Parabolic(tween(300)),
        indentAnimation = Height(tween(300)),
        barColor = MaterialTheme.colorScheme.background,
        ballColor = MaterialTheme.colorScheme.onBackground,
    ) {
        allDestinations.forEachIndexed { _, item ->
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .noRippleClickable { navigateToScreen(item) },
                contentAlignment = Alignment.Center
            ) {
                DropletButton(
                    isSelected = item.index == selectedIndex,
                    onClick = { navigateToScreen(item) },
                    icon = item.iconResource!!,
                    iconColor = if (selectedIndex == item.index) MaterialTheme.colorScheme.onBackground else MaterialTheme.colorScheme.inverseOnSurface,
                    size = 30.dp,
                    dropletColor = MaterialTheme.colorScheme.onBackground
                )

            }
        }

    }

}
