@file:OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)

package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.net.Uri
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import coil.decode.VideoFrameDecoder
import coil.request.ImageRequest
import coil.request.videoFrameMillis
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.GenericListWithItems
import com.asr.smartaplayer.presentation.components.VideoLoadingItem
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.humanReadableDuration
import de.charlex.compose.RevealDirection
import de.charlex.compose.RevealSwipe


@Composable
fun VideoCommentWithThumbNail(
    modifier: Modifier,
    videoComment: VideoComment,
    videoItem: BaseVideoItem,
    onClick: () -> Unit,
    onDeleteRequest: () -> Unit
) {

    val painter = if (videoComment.thumbNail == null) {
        rememberAsyncImagePainter(
            model = ImageRequest.Builder(LocalContext.current)
                .data(videoItem.videoUrl)
                .fetcherFactory<Any> { data, options, imageLoader ->
                    imageLoader.components.newBuilder()
                        .add(VideoFrameDecoder.Factory())
                        .build()
                        .newFetcher(data, options, imageLoader)?.first
                }
                .videoFrameMillis(videoComment.playBackPosition) // specify the position of the video frame here
                .build(),
            imageLoader = ImageLoader.Builder(LocalContext.current)
                .components {
                    add(VideoFrameDecoder.Factory())
                }.crossfade(true)
                .build()
        )
    } else {
        rememberAsyncImagePainter(model = Uri.parse(videoComment.thumbNail))
    }

    RevealSwipe(
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 4.dp, end = 4.dp, top = 10.dp, bottom = 10.dp)
            .height(90.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(Color.Transparent),
        hiddenContentStart = {
            Image(
                modifier = Modifier
                    .padding(20.dp)
                    .size(25.dp),
                painter = painterResource(id = R.drawable.baseline_delete_forever_24),
                contentDescription = null,
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onSurface)
            )
        },
        onBackgroundStartClick = { onDeleteRequest() },
        closeOnBackgroundClick = true,
        backgroundCardStartColor = Color.Transparent,
        directions = setOf(RevealDirection.StartToEnd)
    ) {


        Row(
            modifier = Modifier
                .fillMaxSize()
                .clickable { onClick() },
            verticalAlignment = Alignment.CenterVertically
        ) {

            Image(
                painter = painter,
                contentScale = ContentScale.Crop,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxHeight()
                    .aspectRatio(1f)
                    .clip(RoundedCornerShape(5.dp))
            )

            Spacer(modifier = Modifier.width(10.dp))


            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(1f),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.SpaceBetween
            ) {

                Text(
                    text = videoComment.comment,
                    color = MaterialTheme.colorScheme.onBackground,
                    style = MaterialTheme.typography.bodySmall,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(5.dp),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 3,
                )

                Text(
                    text = humanReadableDuration(videoComment.playBackPosition),
                    color = MaterialTheme.colorScheme.onBackground,
                    style = MaterialTheme.typography.bodySmall,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.padding(5.dp)
                )


            }


        }

    }


}


@Composable
fun VideoComments(
    modifier: Modifier,
    onAddNewVideoComment: (String) -> Unit,
    videoComments: List<VideoComment>,
    videoItem: BaseVideoItem,
    onClick: (VideoComment) -> Unit,
    onDeleteRequest: (VideoComment) -> Unit,
) {


    var comment by remember {
        mutableStateOf("")
    }


    Column(modifier = modifier) {

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = comment,
            onValueChange = { comment = it },
            maxLines = 1,
            label = { Text("Add comment") },
            placeholder = { Text(text = "Add a comment to this timestamp in video ...") },
            colors = TextFieldDefaults.colors(
                focusedContainerColor = Color.Transparent,
                unfocusedContainerColor = Color.Transparent
            ),
            trailingIcon = {
                GeneralIconButton(
                    modifier = Modifier.size(50.dp),
                    onClick = { onAddNewVideoComment(comment) },
                    iconColor = MaterialTheme.colorScheme.onSurfaceVariant,
                    iconId = R.drawable.baseline_done_24,
                    contentDescription = "Add comment at this position of video",
                    paddingValues = PaddingValues(10.dp)
                )
            }
        )

        Spacer(modifier = Modifier.height(10.dp))

        GenericListWithItems(
            modifier = Modifier.fillMaxWidth(),
            listState = rememberLazyListState(),
            listOfItems = videoComments,
            loadMoreItems = {},
            isLoading = false,
            loadingItemIndicator = { VideoLoadingItem() },
            emptyStateComposable = {
                Text(
                    text = "No Bookmarked position or comments found for this video , please add one to check it out here.",
                    color = MaterialTheme.colorScheme.onBackground,
                    style = MaterialTheme.typography.bodySmall,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                )
            }
        ) { _: LazyItemScope, _: Int, videoComment: VideoComment ->
            VideoCommentWithThumbNail(
                modifier = Modifier,
                videoComment = videoComment,
                videoItem = videoItem,
                onClick = { onClick(videoComment) },
                onDeleteRequest = { onDeleteRequest(videoComment) }
            )
        }

    }


}