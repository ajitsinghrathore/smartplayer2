package com.asr.smartaplayer.presentation.screens.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.asr.smartaplayer.R
import com.asr.smartaplayer.data.humanReadableSize
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.OptionalPreservableVideoProperties
import com.asr.smartaplayer.domain.models.UserLeaveHintAction
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsCheckbox
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsGroup
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsList
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsListMultiSelect
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsMenuLink
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsSlider
import com.asr.smartaplayer.presentation.screens.settings.components.SubtitleTextSettings


@Composable
fun SettingScreen(
    paddingValues: PaddingValues,
    viewModel: SettingsViewModel,
) {

    val scrollState = rememberScrollState()
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(paddingValues)
            .verticalScroll(scrollState)
    ) {
        Divider(modifier = Modifier.height(3.dp))

        SettingsGroup(
            title = "Video Arrangement"
        ) {
            val orderVideosBy = OrderVideosBy.values().map { it.name.replace("_", " ") }
            val selectedOrderVideosBy =
                viewModel.videosSortingPreference.value?.orderVideosBy?.name?.replace("_", " ")
                    ?: ""
            val selectedIndex1 = orderVideosBy.indexOf(selectedOrderVideosBy)
            SettingsList(
                title = "Sort videos according to",
                icon = R.drawable.icons8_sort_by_price_24,
                items = orderVideosBy,
                currentSelectedIndex = if (selectedIndex1 != -1) selectedIndex1 else null,
                description = "The chosen selection will dictate the order in which videos are arranged when they are loaded from local or google drive",
                onItemSelected = { _, item ->
                    viewModel.updateOrderVideosBy(
                        orderVideosBy = OrderVideosBy.valueOf(
                            item.replace(" ", "_")
                        )
                    )
                }
            )


            val orderVideosIn = OrderVideosIn.values().map { it.name.replace("_", " ") }
            val selectedOrderVideosIn =
                viewModel.videosSortingPreference.value?.orderVideosIn?.name?.replace("_", " ")
                    ?: ""
            val selectedIndex2 = orderVideosIn.indexOf(selectedOrderVideosIn)
            SettingsList(
                title = "sort the videos in ",
                items = orderVideosIn,
                icon = R.drawable.icons8_sort_by_50,
                currentSelectedIndex = if (selectedIndex2 != -1) selectedIndex2 else null,
                description = "Decides whether the previously chosen criteria should be applied in ascending or descending order.",
                onItemSelected = { _, item ->
                    viewModel.updateOrderVideosIn(
                        orderVideosIn = OrderVideosIn.valueOf(
                            item.replace(" ", "_")
                        )
                    )
                }
            )

        }

        Divider(modifier = Modifier.height(3.dp))

        SettingsGroup(title = "Private Videos") {
            SettingsCheckbox(
                title = "Biometric Authentication",
                checked = viewModel.isOneBiometricAuthEnabled.value,
                onCheckedChange = {
                    viewModel.updateIsOneBiometricAuthEnabled(it)
                },
                icon = R.drawable.baseline_fingerprint_24,
                description = "Enabling this option unlocks all private videos with one biometric authentication at the top. Disabling it means you'll need to authenticate twice: once to hide/reveal all private videos and again to play each private video."
            )
        }


        Divider(modifier = Modifier.height(3.dp))

        SettingsGroup(title = "Video Preview") {
            SettingsCheckbox(
                title = "Swipe Up to dismiss",
                checked = viewModel.miniPlayerSwipeToDismiss.value,
                onCheckedChange = {
                    viewModel.updateSwipeToDismissPlayer(it)
                },
                icon = R.drawable.baseline_swipe_up_24,
                description = "Toggle this option to dismiss the top-of-screen video preview with a simple swipe upwards. Please be aware that when this option is enabled, all video control gestures, such as adjusting brightness, volume, and seeking, will be disabled for the video preview. "
            )
        }

        Divider(modifier = Modifier.height(3.dp))

        SettingsGroup(title = "Video Playback") {
            SettingsSlider(
                title = "Drive video Streaming Cache Size",
                description = "Adjust the slider to set the cache size for video streaming. The cache is a temporary storage area where videos are stored for smooth playback. A larger cache size can provide smoother streaming, especially for higher quality videos. However, it will also use more of your device’s storage space. Try to find a balance that works best for your streaming needs and storage capacity. \nReopen the app after changing the cache size",
                start = (100 * 1024 * 1024).toFloat(),
                end = (2000 * 1024 * 1024).toFloat(),
                currentValue = viewModel.driveVideoCacheSize.longValue.toFloat(),
                icon = R.drawable.baseline_cached_24,
                onValueChangeFinished = {
                    viewModel.updateDriveVideoCacheSize(value = it.toLong())
                },
                onValueChange = {
                    viewModel.driveVideoCacheSize.longValue = it.toLong()
                },
                currentDisplayValue = humanReadableSize(context, viewModel.driveVideoCacheSize.longValue)
            )


            val allPlaybackProperties = OptionalPreservableVideoProperties.values().map {
                it.name.replace("_", " ")
            }

            val currentlySelectedValues =
                viewModel.currentLySelectedPlaybackPropertiesForSync.value.map {
                    it.name.replace("_", " ")
                }

            SettingsListMultiSelect(
                title = "Synchronize Video Attributes",
                icon = R.drawable.baseline_restore_24,
                descriptionText = "Select the video attributes you want to synchronize across all videos. For instance, if you modify audio attributes, the changes will apply to all videos. Conversely, if you unselect a specific attribute, the alteration will only affect that individual video.",
                confirmButton = "Save",
                currentLySelectedValues = allPlaybackProperties.toSet() - currentlySelectedValues.toSet(),
                items = allPlaybackProperties,
                onItemsSelected = { properties ->

                    val restorableProperties = allPlaybackProperties.toSet() - properties
                    val updatedProperties = restorableProperties.map {
                        OptionalPreservableVideoProperties.valueOf(it.replace(" ", "_"))
                    }
                    viewModel.updateCurrentLySelectedPlaybackPropertiesForSync(newProperties = updatedProperties.toSet())
                },
                useSelectedValuesAsSubtitle = true
            )

            val userLeaveActions = UserLeaveHintAction.values().map { it.displayName}
            val selectedUserLeaveAction = viewModel.userLeaveHintAction.value
            val selectedIndex = userLeaveActions.indexOf(selectedUserLeaveAction?.displayName)
            val description: String = selectedUserLeaveAction?.description?:""

            SettingsList(
                title = "Background play setting",
                icon = R.drawable.baseline_video_settings_24,
                items = userLeaveActions,
                currentSelectedIndex = if (selectedIndex != -1) selectedIndex else null,
                description = description,
                onItemSelected = { _, item ->
                    viewModel.updateUserLeaveHintActionKey(
                        value = UserLeaveHintAction.values().find { it.displayName == item }!!
                    )
                }
            )

            var showSubtitleBottomSheet by remember{
                mutableStateOf(false)
            }


            SettingsMenuLink(
                icon = {
                       Icon(
                           painter = rememberAsyncImagePainter(model = R.drawable.icons8_subtitles_24),
                           contentDescription ="" ,
                           tint = MaterialTheme.colorScheme.onBackground
                       )
                },
                title = "Subtitle Settings",
                subtitle = "Customize the appearance of subtitles in videos, including options for font size, text color, and background color."
            ){
                showSubtitleBottomSheet = true
            }

            if(showSubtitleBottomSheet) {

                SubtitleTextSettings(
                    subtitlesDisplaySettings = viewModel.subtitlesDisplaySettings.value,
                    onSettingsChange = { viewModel.updateSubtitlesDisplaySettings(it) },
                    onDismiss = { showSubtitleBottomSheet = false }
                )
            }

        }

        Divider(modifier = Modifier.height(3.dp))


        SettingsGroup(title = "Drowsy State detection") {
            SettingsSlider(
                title = "Drowsy state detection sensitivity",
                description = "Customize the sensitivity level of the detector to your preference. A higher value will result in the video pausing even for slight instances of looking away from the screen or briefly closing your eyes.",
                start = 5f,
                end = 40f,
                currentValue = 45f - viewModel.currentDrowsinessSensitivity.floatValue,
                icon = R.drawable.baseline_face_24,
                onValueChangeFinished = {
                    val updatedValue = 45f - it
                    viewModel.updateDrowsinessSensitivity(value = updatedValue)
                },
                onValueChange = {
                    val updatedValue = 45f - it
                    viewModel.currentDrowsinessSensitivity.floatValue = updatedValue
                }
            )
        }


    }
}
