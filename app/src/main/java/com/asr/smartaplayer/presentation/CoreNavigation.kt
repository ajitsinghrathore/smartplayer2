@file:OptIn(
    ExperimentalAnimationApi::class, ExperimentalPermissionsApi::class,
    ExperimentalPermissionsApi::class, ExperimentalPermissionsApi::class,
    ExperimentalPermissionsApi::class
)

package com.asr.smartaplayer.presentation

import android.app.Activity
import android.app.PictureInPictureParams
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.core.view.WindowInsetsControllerCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.media3.common.util.UnstableApi
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.asr.smartaplayer.presentation.components.BottomBar
import com.asr.smartaplayer.presentation.components.TopBar
import com.asr.smartaplayer.presentation.components.authenticate
import com.asr.smartaplayer.presentation.components.videoPlayer.getActivity
import com.asr.smartaplayer.presentation.components.videoPlayer.updateSystemBrightness
import com.asr.smartaplayer.presentation.screens.driveScreen.DriveScreen
import com.asr.smartaplayer.presentation.screens.driveScreen.DriveScreenViewModel
import com.asr.smartaplayer.presentation.screens.homeScreen.HomeScreen
import com.asr.smartaplayer.presentation.screens.homeScreen.HomeScreenViewModel
import com.asr.smartaplayer.presentation.screens.immersiveVideoPlayBack.ImmersiveVideoScreen
import com.asr.smartaplayer.presentation.screens.settings.SettingScreen
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.systemuicontroller.rememberSystemUiController


fun NavGraphBuilder.defaultAnimatedDestination(
    destination: AppDestination,
    activity: Activity? = null,
    content: @Composable (NavBackStackEntry) -> Unit
) {

    composable(
        destination.route,
        arguments = destination.requiredNavArguments,
        popEnterTransition = {
            fadeIn(
                animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
            )

        },
        popExitTransition = {
            fadeOut(
                animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
            )
        },
        enterTransition = {
            fadeIn(animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing))
        },
        exitTransition = {
            fadeOut(animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing))
        },
    ) {
        if (destination.isFullScreenDestination) {
            val systemUiController = rememberSystemUiController()

            DisposableEffect(key1 = true) {
                // Hide system UI when this composable is displayed
                systemUiController.isStatusBarVisible = false
                systemUiController.isNavigationBarVisible = false

                systemUiController.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

                // Reset system UI visibility when this composable is disposed
                onDispose {
                    systemUiController.isStatusBarVisible = true
                    systemUiController.isNavigationBarVisible = true
                }
            }
        } else {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        }

        content(it)
    }


}

@UnstableApi
@Composable
fun CoreNavigation(
    destinations: List<AppDestination>,
) {
    CheckForNotificationPermission()

    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = AppDestination.getDestinationFromRoute(
        navBackStackEntry?.destination?.route
    )


    val activityViewModel = hiltViewModel<MainActivityViewModel>()
    val homeScreenViewModel = hiltViewModel<HomeScreenViewModel>()
    val driveScreenViewModel = hiltViewModel<DriveScreenViewModel>()

    LaunchedEffect(key1 = currentRoute){
        activityViewModel.currentDestination.value = currentRoute
        if (currentRoute.index != null){
            activityViewModel.currentNavIndicatorBottomIndex.intValue = currentRoute.index
        }
    }


    var showTopAppBar = !activityViewModel.isMiniPlayerOpen.value
    val configuration = LocalConfiguration.current
    var showBottomAppBar = if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        !activityViewModel.isMiniPlayerOpen.value
    } else {
        true
    }

    val context = LocalContext.current



    Scaffold(
        bottomBar = {
            AnimatedVisibility(
                visible = !currentRoute.isFullScreenDestination && showBottomAppBar,
                enter = slideInVertically(
                    initialOffsetY = { it },
                    animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
                ),
                exit = slideOutVertically(
                    targetOffsetY = { it },
                    animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
                ),
            ) {
                BottomBar(
                    allDestinations = destinations,
                    navigateToScreen = {
                        activityViewModel.checkAndCloseVideoIfPlaying()
                        if (it.route != currentRoute.route) {
                            activityViewModel.currentSearchText.value = null
                            navController.navigate(it.route) {
                                launchSingleTop = true
                                popUpTo(0) {
                                    inclusive = true
                                }
                            }
                        }
                    },
                    selectedIndex = activityViewModel.currentNavIndicatorBottomIndex.intValue
                )
            }
        },
        topBar = {
            AnimatedVisibility(
                visible = !currentRoute.isFullScreenDestination && showTopAppBar,
                enter = slideInVertically(
                    initialOffsetY = { -it },
                    animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
                ),
                exit = slideOutVertically(
                    targetOffsetY = { -it },
                    animationSpec = tween(durationMillis = 500, easing = FastOutSlowInEasing)
                ),
            ) {
                val hasPrivateVideosAccess = activityViewModel.hasAccessToSeePrivateVideos.value
                TopBar(
                    hasPrivateAccess = hasPrivateVideosAccess,
                    currentDestination = activityViewModel.currentDestination.value?:AppDestination.HomeScreen,
                    currentSearchText =activityViewModel.currentSearchText.value,
                    onSearchTextChange = {
                                         activityViewModel.currentSearchText.value = it
                    },
                ) {
                    if (!it) {
                        activityViewModel.updateAccessToPrivateVideos(false)
                    } else {
                        authenticate(
                            context = context,
                            onAuthenticationFailed = {},
                            onAuthenticationSucceeded = {
                                activityViewModel.updateAccessToPrivateVideos(true)
                            }
                        )
                    }
                }
            }
        }
    ) { padding ->


        NavHost(
            navController = navController,
            startDestination = destinations[0].route
        ) {

            defaultAnimatedDestination(
                destination = AppDestination.HomeScreen,
                activity = context.getActivity()
            ) {

                activityViewModel.exoPlayer?.setBrightness(context = LocalContext.current)
                HomeScreen(
                    contentPadding = padding,
                    viewModel = homeScreenViewModel,
                    hasAccessToSeePrivateVideos = activityViewModel.hasAccessToSeePrivateVideos.value,
                    isOneBiometricAuthEnabled = activityViewModel.isOneBiometricAuthEnabled.value,
                    smartExoPlayer = activityViewModel.exoPlayer,
                    onCloseVideoPreviewRequested = {
                        showTopAppBar = true
                        showBottomAppBar = true
                        activityViewModel.checkAndCloseVideoIfPlaying()
                    },
                    onVideoPreviewRequested = {
                        showTopAppBar = false
                        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            showBottomAppBar = false
                        }
                        activityViewModel.playVideo(videoItem = it, context = context)
                    },
                    openImmersiveVideoPlayback = {
                        navController.navigate(AppDestination.ImmersiveModeScreen.route) {
                            launchSingleTop = true
                        }
                    },
                    miniPlayerVisible = activityViewModel.isMiniPlayerOpen.value,
                    videoComments = activityViewModel.videoCommentsState.value,
                    addComment = { video, comment ->
                        activityViewModel.addCommentInVideo(video, comment, context)
                    },
                    searchText = activityViewModel.currentSearchText.value?:""
                )
            }

            defaultAnimatedDestination(
                destination = AppDestination.DriveScreen,
                activity = context.getActivity()
            ) {
                activityViewModel.exoPlayer?.setBrightness(context = LocalContext.current)
                DriveScreen(
                    paddingValues = padding,
                    viewModel = driveScreenViewModel,
                    miniPlayerVisible = activityViewModel.isMiniPlayerOpen.value,
                    smartExoPlayer = activityViewModel.exoPlayer,
                    onCloseVideoPreviewRequested = {
                        showTopAppBar = true
                        showBottomAppBar = true
                        activityViewModel.checkAndCloseVideoIfPlaying()
                    },
                    onVideoPreviewRequested = {
                        showTopAppBar = false
                        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            showBottomAppBar = false
                        }
                        activityViewModel.playVideo(videoItem = it, context = context)
                    },
                    openImmersiveVideoPlayBack = {
                        navController.navigate(AppDestination.ImmersiveModeScreen.route) {
                            launchSingleTop = true
                        }
                    },
                    videoComments = activityViewModel.videoCommentsState.value,
                    addComment = { video, comment ->
                        activityViewModel.addCommentInVideo(video, comment, context)
                    }
                )
            }

            defaultAnimatedDestination(
                destination = AppDestination.PlayListScreen,
                activity = context.getActivity()
            ) {

            }

            defaultAnimatedDestination(
                destination = AppDestination.SettingsScreen,
                activity = context.getActivity()
            ) {
                activityViewModel.exoPlayer?.setBrightness(context = LocalContext.current)
                SettingScreen(
                    paddingValues = padding,
                    viewModel = hiltViewModel(),
                )
            }

            defaultAnimatedDestination(destination = AppDestination.ImmersiveModeScreen) {
                val activity = context.getActivity()
                val videoOrientation = activityViewModel.exoPlayer?.getVideoAspectRatio()

                if (activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
                    Log.d("TAG****", "CoreNavigation: $videoOrientation")
                    videoOrientation?.let {
                        if (it.numerator > it.denominator) {
                            activity.requestedOrientation =
                                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        } else {
                            activity.requestedOrientation =
                                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                    }
                }

                activityViewModel.exoPlayer?.setBrightness(context = LocalContext.current)

                ImmersiveVideoScreen(
                    exitImmersiveMode = { navController.navigateUp() },
                    smartExoPlayer = activityViewModel.exoPlayer,
                    onPIPRequest = {
                        val aspectRatio = activityViewModel.exoPlayer?.getVideoAspectRatio()
                        val params =
                            PictureInPictureParams.Builder().setAspectRatio(aspectRatio).build()
                        context.getActivity()?.enterPictureInPictureMode(params)
                    },
                    viewModel = hiltViewModel(),
                    onCommentAddRequest = {video, comment ->
                        activityViewModel.addCommentInVideo(video, comment, context)
                    },
                    onCommentDeleteRequest = {video, comment ->
                        activityViewModel.removeCommentInVideo(videoItem = video, comment = comment)
                    },
                    videoComments = activityViewModel.videoCommentsState.value
                )
            }

        }


    }


}


@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun CheckForNotificationPermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        val notificationPermissionState =
            rememberPermissionState(permission = android.Manifest.permission.POST_NOTIFICATIONS)
        LaunchedEffect(key1 = notificationPermissionState.hasPermission) {
            if (!notificationPermissionState.hasPermission) {
                notificationPermissionState.launchPermissionRequest()
            }
        }
    }

}




