package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun CircularProgressBar(
    modifier: Modifier,
    currentValue: Float,
    minValue: Float,
    maxValue: Float,
    iconResource: Int
) {


    Box(
        modifier = modifier.background(
            Brush.radialGradient(
                listOf(MaterialTheme.colorScheme.background, Color.Transparent)
            )
        ),
        contentAlignment = Alignment.Center
    ) {
        val color = MaterialTheme.colorScheme.primary
        val percentage = ((currentValue - minValue) / (maxValue - minValue))
        Canvas(
            modifier = Modifier
                .sizeIn(minHeight = 50.dp, maxHeight = 100.dp)
                .fillMaxHeight(0.3f)
                .aspectRatio(1f)
        ) {
            drawArc(
                color = color,
                startAngle = -90f,
                sweepAngle = 360 * percentage,
                useCenter = false,
                style = Stroke(width = 5.dp.toPx(), cap = StrokeCap.Round)
            )
        }

        Image(
            painter = painterResource(id = iconResource),
            contentDescription = "volume icon",
            colorFilter = ColorFilter.tint(color),
            modifier = Modifier
                .sizeIn(minHeight = 50.dp, maxHeight = 100.dp)
                .fillMaxHeight(0.3f)
                .padding(15.dp)
        )

    }


}