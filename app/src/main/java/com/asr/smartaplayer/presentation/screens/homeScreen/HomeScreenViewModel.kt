package com.asr.smartaplayer.presentation.screens.homeScreen

import android.content.Context
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.useCases.AllUseCases
import com.asr.smartaplayer.domain.utils.PaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import com.asr.smartaplayer.presentation.showToast
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeScreenViewModel @Inject constructor(
    private val allUseCases: AllUseCases,
    @ApplicationContext private val context: Context
) : ViewModel() {


    val videosState: MutableState<List<LocalVideoItem>> = mutableStateOf(
        emptyList()
    )
    val loadingState = mutableStateOf(false)
    val swipeToDismissMiniPlayer = mutableStateOf(false)
    private var videosPaginationState = PaginationState()
    private var videosLoadingJob: Job? = null

    init {
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.SwipeToDismissVideoPreview)
                .collectLatest {
                    swipeToDismissMiniPlayer.value = it
                }
        }
    }


    fun loadMoreVideos(freshLoad: Boolean = false, hasAccessToSeePrivateVideos: Boolean, searchText: String) {

        if (freshLoad){
            videosLoadingJob?.cancel()
            videosLoadingJob = null
        }
       videosLoadingJob =  viewModelScope.launch(Dispatchers.IO) {
            if (videosPaginationState.isLoading || videosPaginationState.paginationCompleted) {
                if (!freshLoad) {
                    return@launch
                }

            }
            if (freshLoad) {
                videosPaginationState = PaginationState()
            }
            Log.d("TAG___", ": load more starting  $searchText $videosPaginationState")

            loadingState.value = true
            val result = allUseCases.videoUseCases.getAllLocalVideos(
                paginationState = videosPaginationState,
                hasAccessToSeePrivateVideos = hasAccessToSeePrivateVideos,
                searchText = searchText
            )
           Log.d("TAG___", "loadMoreVideos: loading ended ")
            if (result is UseCaseTaskResult.Success) {
                if (freshLoad) {
                    videosState.value = result.data
                } else {
                    videosState.value += result.data
                }
            } else {
                showToast(context, "something went wrong !!")
            }
            videosPaginationState.isLoading = false
            loadingState.value = false
        }
    }

    fun toggleVideoIsPrivate(
        videoItem: LocalVideoItem,
        isPrivate: Boolean,
        hasAccessToSeePrivateVideos: Boolean
    ): Flow<Float> = flow {
            Log.d("TAG__", "toggleVideoIsPrivate: $videoItem")
            allUseCases.videoUseCases.toggleIsVideoPrivate(
                videoItem = videoItem,
                isPrivate = isPrivate
            ).collect{
                when(it){
                    is UseCaseTaskResult.Failure -> {}
                    is UseCaseTaskResult.Progress -> emit(it.progress)
                    is UseCaseTaskResult.Success -> {
                        Log.d("DRIVE*", "toggleVideoIsPrivate: got success item ${it.data}")
                        val result = it.data
                        val updatedList: MutableList<LocalVideoItem> = mutableListOf()
                        for (item in videosState.value) {
                            if (item.videoUrl == videoItem.videoUrl) {
                                if (result.isPrivate && hasAccessToSeePrivateVideos)
                                    updatedList.add(result)
                                else if (!result.isPrivate)
                                    updatedList.add(result)
                            } else {
                                updatedList.add(item)
                            }
                        }
                        videosState.value = updatedList
                    }
                }
            }

        Log.d("DRIVE*", "toggleVideoIsPrivate:  Completing viewmodel flow body")
    }


}