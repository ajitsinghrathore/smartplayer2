package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim


import android.view.MotionEvent
import androidx.compose.ui.unit.IntSize
import kotlin.math.abs

abstract class ScrimGestureDetector {

    private var touchStartX = 0
    private var touchStartY = 0
    private var gesture: ScrimGesture = ScrimGesture.Unknown
    private var gestureLocked = false

    private var lastActionUpEvent: Long = 0


    fun handleTouchEvent(event: MotionEvent, touchAreaSize: IntSize) {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                handleGestureStart(event, touchAreaSize)
            }

            MotionEvent.ACTION_MOVE -> {
                handleGestureProgress(event, touchAreaSize)
            }

            MotionEvent.ACTION_UP -> {
                if (!gestureLocked && lastActionUpEvent != 0L && System.currentTimeMillis() - lastActionUpEvent < 300){
                    onGestureEnded(ScrimGesture.DoubleClick)
                }
                else if (!gestureLocked) {
                    onGestureEnded(ScrimGesture.Click)
                } else {
                    onGestureEnded(gesture)
                }
                gestureLocked = false
                gesture = ScrimGesture.Unknown
                lastActionUpEvent = if (!gestureLocked) {
                    System.currentTimeMillis()
                } else{
                    0
                }
            }
        }

    }


    private fun handleGestureStart(event: MotionEvent, touchAreaSize: IntSize) {
        touchStartX = event.x.toInt()
        touchStartY = event.y.toInt()
        gesture = if (touchStartX < (touchAreaSize.width / 2)) {
            ScrimGesture.LeftUpDown
        } else {
            ScrimGesture.RightUpDown
        }
    }

    private fun handleGestureProgress(event: MotionEvent, touchAreaSize: IntSize) {
        val currentX = event.x
        val currentY = event.y

        val deltaX = currentX - touchStartX
        val deltaY = touchStartY - currentY
        val swipedHorizontally: Boolean
        val swipedVertically: Boolean

        if (!gestureLocked) {
            swipedHorizontally = abs(deltaX) > 60
            swipedVertically = abs(deltaY) > 60
        } else {
            swipedHorizontally = true
            swipedVertically = true
        }


        if (!gestureLocked && (swipedHorizontally || swipedVertically)) {
            if (swipedHorizontally) {
                gestureLocked = true
                gesture = ScrimGesture.HorizontalSwipe
            } else {
                gestureLocked = true
            }
            touchStartX = event.x.toInt()
            touchStartY = event.y.toInt()
            onGestureStarted(gesture)
            return
        }

        if (gestureLocked) {
            when (gesture) {
                ScrimGesture.RightUpDown -> {
                    onGestureProgressListener(
                        gesture,
                        calculateRelativeChangeRequested(
                            deltaY, touchAreaSize
                        )
                    )
                }

                ScrimGesture.LeftUpDown -> {
                    onGestureProgressListener(
                        gesture,
                        calculateRelativeChangeRequested(
                            deltaY,
                            touchAreaSize
                        )
                    )
                }

                ScrimGesture.HorizontalSwipe -> {
                    onGestureProgressListener(
                        gesture,
                        calculateRelativeChangeRequestedHorizontally(deltaX, touchAreaSize)
                    )
                }

                ScrimGesture.Click -> {}
                else -> {}
            }
        }


    }

    private fun calculateRelativeChangeRequested(delta: Float, touchAreaSize: IntSize): Float {
        // it calculates the amount of swipe relative to available swipe area
        // ex-  0.5 means  he has swiped half the area of videoPlayer
        // and 1.5 means he has swiped 1.5 times the area of video player only possible in case of mini player
        val screenHeight = touchAreaSize.height
        return delta / screenHeight
    }

    private fun calculateRelativeChangeRequestedHorizontally(
        delta: Float,
        touchAreaSize: IntSize
    ): Float {
        val screenWidth = touchAreaSize.width
        return delta / screenWidth
    }


    abstract fun onGestureStarted(gesture: ScrimGesture)
    abstract fun onGestureEnded(gesture: ScrimGesture)
    abstract fun onGestureProgressListener(
        gesture: ScrimGesture,
        gestureLengthRelativeToScreen: Float
    )

}


enum class ScrimGesture {
    LeftUpDown,
    RightUpDown,
    HorizontalSwipe,
    Click,
    DoubleClick,
    Unknown
}



