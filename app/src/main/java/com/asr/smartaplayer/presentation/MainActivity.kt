package com.asr.smartaplayer.presentation

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.fragment.app.FragmentActivity
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.media3.common.util.UnstableApi
import com.asr.smartaplayer.R
import com.asr.smartaplayer.ui.theme.SmartPlayerAppTheme
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.services.drive.DriveScopes
import com.google.firebase.appdistribution.FirebaseAppDistribution
import com.google.firebase.appdistribution.FirebaseAppDistributionException
import com.google.firebase.appdistribution.InterruptionLevel
import com.google.firebase.appdistribution.ktx.appDistribution
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : FragmentActivity() {

//    override fun onResume() {
//        super.onResume()
//        val firebaseAppDistribution = FirebaseAppDistribution.getInstance()
//        firebaseAppDistribution.updateIfNewReleaseAvailable()
//            .addOnProgressListener { _ ->
//                // (Optional) Implement custom progress updates in addition to
//                // automatic NotificationManager updates.
//            }
//    }


    val viewModel: MainActivityViewModel by viewModels()


    @UnstableApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Firebase.appDistribution.showFeedbackNotification(
            // Text providing notice to your testers about collection and
            // processing of their feedback data
            R.string.feedback_text,
            // The level of interruption for the notification
            InterruptionLevel.HIGH
        )

        setContent {
            SmartPlayerAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CoreNavigation(destinations = AppDestination.getAllBottomNavDestinations())
                }
            }
        }


    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.d("TAG______________________", "onConfigurationChanged: 1 ${viewModel.exoPlayer}")
        viewModel.exoPlayer?.setBrightness(context = this as Context)
        Log.d("TAG______________________", "onConfigurationChanged: 2 ${viewModel.exoPlayer}")
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        viewModel.userLeaveHintReceived(this)
    }
}
