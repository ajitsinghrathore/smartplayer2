@file:OptIn(
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterialApi::class
)

package com.asr.smartaplayer.presentation.components

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import coil.decode.VideoFrameDecoder
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import de.charlex.compose.RevealSwipe


@Composable
fun VideoMetadata(videoItem: BaseVideoItem, modifier: Modifier) {


    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = modifier
    ) {
        Text(
            text = videoItem.videoDuration,
            style = MaterialTheme.typography.labelMedium,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Text(
            text = videoItem.videoSize,
            style = MaterialTheme.typography.labelMedium,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }


}


@Composable
fun LocalVideoItem(
    videoItem: LocalVideoItem,
    modifier: Modifier = Modifier,
    isOneBiometricAuthEnabled: Boolean,
    onVideoItemSelected: (BaseVideoItem) -> Unit,
    onToggleIsVideoPrivate: (Boolean) -> Unit,
) {

    val vectorResource = if (videoItem.isPrivate) {
        R.drawable.icons8_remove_fingerprint_50
    } else {
        R.drawable.icons8_biometrics_64
    }

    RevealSwipe(
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 4.dp, end = 4.dp, top = 10.dp, bottom = 10.dp)
            .height(90.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(Color.Transparent),
        hiddenContentStart = {
            Image(
                modifier = Modifier
                    .padding(20.dp)
                    .size(25.dp),
                painter = painterResource(id = vectorResource),
                contentDescription = null,
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onSurface)
            )
        },
        hiddenContentEnd = {
            Image(
                modifier = Modifier
                    .padding(20.dp)
                    .size(25.dp),
                painter = painterResource(id = vectorResource),
                contentDescription = null,
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onSurface)
            )
        },
        onBackgroundEndClick = { onToggleIsVideoPrivate(!videoItem.isPrivate) },
        onBackgroundStartClick = { onToggleIsVideoPrivate(!videoItem.isPrivate) },
        closeOnBackgroundClick = true,
        backgroundCardEndColor = MaterialTheme.colorScheme.surface,
        backgroundCardStartColor = MaterialTheme.colorScheme.surface,
    ) {

        Card(
            modifier = modifier,
            elevation = CardDefaults.cardElevation(5.dp),
            onClick = { onVideoItemSelected(videoItem) },
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.background
            ),
        ) {

            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(end = 5.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {

                VideoThumbnail(
                    videoItem = videoItem,
                    isOneBiometricAuthEnabled = isOneBiometricAuthEnabled
                )
                Spacer(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(20.dp)
                )
                Column(
                    verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.Start,
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                ) {
                    Text(
                        text = videoItem.videoName,
                        style = MaterialTheme.typography.bodyLarge,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    VideoMetadata(videoItem = videoItem, modifier = Modifier.fillMaxWidth())
                }

            }
        }


    }
}


@Composable
fun DriveVideoItem(
    videoItem: DriveVideoItem,
    modifier: Modifier = Modifier,
    onVideoItemSelected: (BaseVideoItem) -> Unit,
) {

    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 4.dp, end = 4.dp, top = 10.dp, bottom = 10.dp)
            .height(90.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(Color.Transparent),
        elevation = CardDefaults.cardElevation(5.dp),
        onClick = { onVideoItemSelected(videoItem) },
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.background
        ),
    ) {

        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(end = 5.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {

            VideoThumbnail(videoItem = videoItem)
            Spacer(
                modifier = Modifier
                    .fillMaxHeight()
                    .width(20.dp)
            )
            Column(
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.Start,
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(1f)
            ) {
                Text(
                    text = videoItem.videoName,
                    style = MaterialTheme.typography.bodyLarge,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                VideoMetadata(videoItem = videoItem, modifier = Modifier.fillMaxWidth())
            }

        }
    }


}


@Composable
fun VideoThumbnail(videoItem: DriveVideoItem) {

    val painter = rememberAsyncImagePainter(model = videoItem.videoThumbNailLink)

    Box {
        Image(
            painter = painter,
            contentDescription = "thumbnail",
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f),
            contentScale = ContentScale.Crop,
        )
    }


}


@Composable
fun VideoThumbnail(videoItem: LocalVideoItem, isOneBiometricAuthEnabled: Boolean) {

    val imageLoader = ImageLoader.Builder(LocalContext.current)
        .components {
            add(VideoFrameDecoder.Factory())
        }.crossfade(true)
        .build()
    val painter = rememberAsyncImagePainter(
        model = Uri.parse(videoItem.videoUrl),
        imageLoader = imageLoader
    )



    Box {
        Image(
            painter = painter,
            contentDescription = "thumbnail",
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f),
            contentScale = ContentScale.Crop,
        )

        if (videoItem.isPrivate) {
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .aspectRatio(1f)
                    .background(MaterialTheme.colorScheme.background.copy(alpha = 0.8f))
            ) {
                val vectorResource = if (!isOneBiometricAuthEnabled) {
                    R.drawable.icons8_lock_24
                } else {
                    R.drawable.icons8_open_lock_24
                }
                Image(
                    modifier = Modifier
                        .size(24.dp)
                        .align(Alignment.Center),
                    painter = painterResource(id = vectorResource),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )

            }
        }
    }


}


