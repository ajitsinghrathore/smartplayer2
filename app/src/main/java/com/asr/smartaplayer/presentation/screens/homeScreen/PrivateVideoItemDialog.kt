package com.asr.smartaplayer.presentation.screens.homeScreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.presentation.components.authenticate


@Composable
fun PrivateVideoItemDialog(
    onProceed: () -> Unit,
    onDiscarded: () -> Unit,
    progress: Float,
    makePrivate: Boolean
) {

    val message =
        if (makePrivate) {
            "You can hide videos in a secure folder, making them inaccessible from your gallery or other apps.\nWhen moving a video to this folder, you'll be prompted to delete the original for added privacy. To view these private videos, simply tap the visibility icon at the top of the app bar. \nAccessing them initially requires biometric authentication, ensuring your privacy. For added convenience, you can adjust settings to skip re-authentication when playing these videos. Your privacy, your control"
        }
    else{
           "Are you sure you want to make this video public?\nBy making this video public, it will be added to your gallery amd will be accessible to everyone and can be viewed by any App.\n\nPlease confirm your choice below"
        }


    val title = if (makePrivate){
        "Move Video to Private Folder"
    }else{
        "Remove Video From Private folder"
    }
    val context = LocalContext.current
    AlertDialog(
        onDismissRequest = {
        },
        shape = RoundedCornerShape(15.dp),
        backgroundColor = MaterialTheme.colorScheme.surface,
        confirmButton = {
            TextButton(
                enabled = progress == 0f,
                onClick = {
                    authenticate(
                        context = context,
                        onAuthenticationSucceeded = {
                            onProceed()
                        },
                        onAuthenticationFailed = {}
                    )
                }
            ) {
                Text(text = "Proceed")
            }
        },
        text = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = message,
                    style = MaterialTheme.typography.bodySmall,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                    color = MaterialTheme.colorScheme.onSurface
                )



                if (progress != 0f) {
                    Spacer(modifier = Modifier.height(10.dp))
                    LinearProgressIndicator(
                        progress = progress,
                        color = MaterialTheme.colorScheme.primary,
                        backgroundColor = MaterialTheme.colorScheme.onSurface
                    )
                }
            }
        },
        title = {
            Text(
                text = title,
                style = MaterialTheme.typography.titleMedium,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth(),
                color = MaterialTheme.colorScheme.onSurface
            )
        },
        properties = DialogProperties(),
        dismissButton = {
            TextButton(
                enabled = progress == 0f,
                onClick = { onDiscarded() }
            ) {
                Text(text = "Not interested")
            }
        }

    )

}