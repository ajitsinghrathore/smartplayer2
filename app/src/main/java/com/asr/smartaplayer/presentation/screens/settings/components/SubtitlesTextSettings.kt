@file:OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class,
    ExperimentalComposeUiApi::class
)

package com.asr.smartaplayer.presentation.screens.settings.components

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.toColorInt
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.SubtitlesDisplaySettings
import io.mhssn.colorpicker.ext.transparentBackground


@Composable
fun SubtitleTextSettings(
    subtitlesDisplaySettings: SubtitlesDisplaySettings?,
    onSettingsChange: (SubtitlesDisplaySettings) -> Unit,
    onDismiss: () -> Unit
) {


    Log.d("SUBTITLE", "SubtitleTextSettings: $subtitlesDisplaySettings")

    subtitlesDisplaySettings?.let {


        var tempTextSize by remember {
            mutableFloatStateOf(subtitlesDisplaySettings.textSize)
        }


        ModalBottomSheet(
            onDismissRequest = onDismiss, containerColor = MaterialTheme.colorScheme.surface
        ) {


            Column(
                modifier = Modifier.padding(20.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Sample subtitle text",
                    color = Color(subtitlesDisplaySettings.textColor.toColorInt()),
                    fontSize = tempTextSize.sp,
                    modifier = Modifier.background(
                        Color(subtitlesDisplaySettings.backgroundColor.toColorInt())
                    )
                )

                Spacer(modifier = Modifier.height(10.dp))

                SettingsSlider(
                    title = null,
                    description = null,
                    start = 12f,
                    end = 30f,
                    currentValue = tempTextSize,
                    icon = R.drawable.baseline_format_size_24,
                    currentDisplayedComposable = {},
                    onValueChange = {
                        tempTextSize = it
                    },
                    onValueChangeFinished = {
                        onSettingsChange(
                            SubtitlesDisplaySettings(
                                textSize = it,
                                textColor = subtitlesDisplaySettings.textColor,
                                backgroundColor = subtitlesDisplaySettings.backgroundColor
                            )
                        )
                    })
                Spacer(modifier = Modifier.height(10.dp))


                Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.Start) {
                    val backgroundColors = listOf(
                        "#000000", "#FFFFFF", "#00000000"
                    )

                    val selectedBackgroundIndex =
                        backgroundColors.indexOf(subtitlesDisplaySettings.backgroundColor)


                    Text(
                        text = "Background color", color = MaterialTheme.colorScheme.onSurface
                    )
                    Spacer(modifier = Modifier.height(10.dp))


                    PresetColorChips(selectedChipIndex = selectedBackgroundIndex,
                        colors = backgroundColors,
                        onColorSelected = {
                            onSettingsChange(
                                SubtitlesDisplaySettings(
                                    textSize = subtitlesDisplaySettings.textSize,
                                    textColor = subtitlesDisplaySettings.textColor,
                                    backgroundColor = it
                                )
                            )
                        }
                    )

                    Spacer(modifier = Modifier.height(10.dp))


                    val textColors = listOf(
                        "#000000", "#FFFFFF", "#FFFF00", "#0000FF", "#00FF00"
                    )

                    val selectedTextColorIndex =
                        textColors.indexOf(subtitlesDisplaySettings.textColor)

                    Text(
                        text = "Text color", color = MaterialTheme.colorScheme.onSurface
                    )
                    Spacer(modifier = Modifier.height(10.dp))


                    PresetColorChips(selectedChipIndex = selectedTextColorIndex,
                        colors = textColors,
                        onColorSelected = {
                            onSettingsChange(
                                SubtitlesDisplaySettings(
                                    textSize = subtitlesDisplaySettings.textSize,
                                    textColor = it,
                                    backgroundColor = subtitlesDisplaySettings.backgroundColor
                                )
                            )
                        }
                    )


                }


            }
        }


    }


}


@Composable
fun PresetColorChips(
    selectedChipIndex: Int, colors: List<String>, onColorSelected: (String) -> Unit
) {

    ScrollableTabRow(
        selectedTabIndex = selectedChipIndex,
        containerColor = Color.Transparent,
        indicator = {},
        divider = {},
        modifier = Modifier.fillMaxWidth(),
        edgePadding = 0.dp
    ) {

        for (colorIndex in colors.indices) {
            val color = colors[colorIndex]
            val containerColor =
                if (color == "#00000000") Color.Transparent else Color(color.toColorInt())
            val colorModifier = if (color == "#00000000") {
                Modifier
                    .height(30.dp)
                    .padding(horizontal = 10.dp)
                    .width(30.dp)
                    .clip(RoundedCornerShape(100.dp))
                    .transparentBackground(8)
                    .background(Color.Cyan.copy(alpha = 0.2f))

            } else {
                Modifier
                    .height(30.dp)
                    .padding(horizontal = 10.dp)
                    .width(30.dp)
            }
            TextButton(
                onClick = { onColorSelected(color) },
                colors = ButtonDefaults.textButtonColors(
                    containerColor = containerColor,
                ),
                modifier = colorModifier,
                contentPadding = PaddingValues(horizontal = 5.dp),
                shape = RoundedCornerShape(50.dp)
            ) {
                if (selectedChipIndex == colorIndex) {
                    androidx.compose.material.Icon(
                        imageVector = Icons.Default.Check,
                        contentDescription = "",
                        tint = getContrastColor(Color(color.toColorInt()))
                    )
                }
            }

        }
    }


}


fun getContrastColor(color: Color): Color {
    if (color == Color.Transparent){
        return Color.Black
    }
    val luminance = color.luminance()

    // Determine the threshold value (0.5 is a common threshold)
    val threshold = 0.5

    // Decide the contrasting color based on luminance
    return if (luminance > threshold) {
        Color.Black
    } else {
        Color.White
    }
}