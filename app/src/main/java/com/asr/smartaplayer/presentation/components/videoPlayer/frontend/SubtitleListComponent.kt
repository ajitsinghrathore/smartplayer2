@file:OptIn(ExperimentalPermissionsApi::class, ExperimentalComposeUiApi::class)

package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.Manifest
import android.os.Build
import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.CheckboxColors
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.presentation.components.GenericListWithItems
import com.asr.smartaplayer.presentation.components.VideoLoadingItem
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsCheckbox
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState


@Composable
fun SubtitlesList(
    modifier: Modifier,
    subtitles: List<Subtitle>,
    loadMoreSubtitle: (Boolean, String) -> Unit,
    isSubtitlesLoading: Boolean,
    selectedSubtitle: Subtitle?,
    isSubtitlesEnabled: Boolean,
    onSubtitleToggle: (Boolean) -> Unit,
    onSubtitleSelect: (Subtitle) -> Unit
) {

    val permissionState = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        rememberPermissionState(
            permission = Manifest.permission.READ_MEDIA_VIDEO
        )
    } else {
        rememberPermissionState(permission = Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    if (!permissionState.hasPermission) {

        Column(
            modifier = modifier,
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "SmartPlayer needs Storage access for searching subtitles locally",
                style = MaterialTheme.typography.bodySmall,
                modifier = Modifier.padding(horizontal = 10.dp),
                color = MaterialTheme.colorScheme.onBackground,
                textAlign = TextAlign.Center
            )
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp)
            )
            Button(onClick = { permissionState.launchPermissionRequest() }) {
                Text(
                    text = "Grant Permission",
                    style = MaterialTheme.typography.labelMedium,
                    color = MaterialTheme.colorScheme.onPrimary,
                )
            }
        }

        return
    }


    var searchText by remember {
        mutableStateOf("")
    }

    Column(modifier = modifier) {

        SettingsCheckbox(
            modifier = Modifier.fillMaxWidth(),
            title = "Subtitles",
            onCheckedChange = onSubtitleToggle,
            checked = isSubtitlesEnabled,
            icon = R.drawable.icons8_subtitles_24,
            description = "Turn on/off subtitles",
            enabled = selectedSubtitle != null
        )

        val keyboardController = LocalSoftwareKeyboardController.current
        val focusManager = LocalFocusManager.current
        TextField(
            modifier = Modifier
                .fillMaxWidth(),
            value = searchText,
            onValueChange = {
                searchText = it
            },
            maxLines = 1,
            placeholder = { Text(text = "Search subtitles locally") },
            colors = TextFieldDefaults.colors(
                focusedContainerColor = Color.Transparent,
                unfocusedContainerColor = Color.Transparent
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    loadMoreSubtitle(true, searchText)
                    focusManager.clearFocus(true)
                    keyboardController?.hide()
                }
            ),
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Search
            ),
            trailingIcon = {
                GeneralIconButton(
                    modifier = Modifier.size(50.dp),
                    onClick = {
                        loadMoreSubtitle(true, searchText)
                        focusManager.clearFocus(true)
                       keyboardController?.hide()
                    },
                    iconColor = MaterialTheme.colorScheme.onSurfaceVariant,
                    iconId = R.drawable.baseline_search_24,
                    contentDescription = "Add comment at this position of video",
                    paddingValues = PaddingValues(10.dp)
                )
            }
        )

        Spacer(modifier = Modifier.height(10.dp))

        GenericListWithItems(
            modifier = Modifier.fillMaxWidth(),
            listState = rememberLazyListState(),
            listOfItems = subtitles,
            loadMoreItems = { loadMoreSubtitle(it, searchText) },
            isLoading = isSubtitlesLoading,
            loadingItemIndicator = { VideoLoadingItem() },
        ) { _: LazyItemScope, _: Int, subtitle: Subtitle ->
            SubtitleItem(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp),
                isSelected = if (selectedSubtitle == null) false else selectedSubtitle.uri == subtitle.uri,
                subtitle = subtitle,
                contentColor = MaterialTheme.colorScheme.onBackground,
                onSelect = { onSubtitleSelect(subtitle) }
            )
        }
    }


}