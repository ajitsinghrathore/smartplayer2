package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.graphics.SurfaceTexture
import android.util.Log
import android.view.TextureView
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.util.UnstableApi
import androidx.media3.ui.PlayerView
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.ExoPlayerControlsUiState
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.components.videoPlayer.createVideoCommentItemForVideo
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.lowerSection.LowerSectionForPreview
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.upperSection.UpperSectionForVideoPreview
import com.asr.smartaplayer.presentation.components.videoPlayer.getDeviceMaxVolume
import com.asr.smartaplayer.presentation.components.videoPlayer.getDeviceMinVolume
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@Composable
@androidx.annotation.OptIn(UnstableApi::class)
fun SmartVideoPreviewUI(
    modifier: Modifier,
    smartExoPlayer: SmartExoPlayer?,
    onClosePreviewRequested: () -> Unit,
    onFullScreenModeRequested: () -> Unit,
    enableGestures: Boolean = false,
    videoComments: List<VideoComment>,
    addComment: (BaseVideoItem, VideoComment) -> Unit
) {

    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    var controlsState by remember {
        mutableStateOf(ExoPlayerControlsUiState.ShowAllControls)
    }




        Box(
            modifier = modifier.background(MaterialTheme.colorScheme.background),
            contentAlignment = Alignment.BottomStart
        ) {

            val coroutineScope = rememberCoroutineScope()
            val videoPlayerState = smartExoPlayer?.videoPlayerState
            val currentPlayingMedia = videoPlayerState?.currentPlayingMedia?.value

            if (smartExoPlayer != null) {

                AndroidView(modifier = modifier, factory = {
                    PlayerView(context).apply {
                        Log.d("PLAYERVIEW", "ExoPlayerUi: creating new player view")
                        smartExoPlayer.attachPlayerView(this, coroutineScope)
                        this.useController = false
                        this.keepScreenOn = true
                    }
                }, update = {
                    Log.d("PLAYERVIEW", "SmartVideoPreviewUI:  update player view")
                    it.useController = false
                    it.keepScreenOn = true
                })

            }

            if (currentPlayingMedia != null) {


                if (enableGestures) {

                    TouchScrim(
                        modifier = Modifier.fillMaxSize(),
                        currentBrightness = currentPlayingMedia.brightnessIntensity.value,
                        currentVolume = currentPlayingMedia.volumeLevel.value,
                        currentPlayBackPosition = currentPlayingMedia.lastPlayedPosition.value,
                        totalDuration = currentPlayingMedia.totalDuration.value,
                        deviceMaxVolume = getDeviceMaxVolume(context).toFloat(),
                        deviceMinVolume = getDeviceMinVolume(context = context).toFloat(),
                        onBrightnessChange = {
                            smartExoPlayer.setBrightness(brightnessLevel = it, context = context)
                        },
                        onVolumeUpdateChange = {
                            smartExoPlayer.setVolume(volumeLevel = it, context = context)
                        },
                        onPlayBackPositionChange = {
                            smartExoPlayer.seekTo(position = it)
                        },
                        onBrightnessChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowBrightnessChange else ExoPlayerControlsUiState.HideAllControls
                        },
                        onVolumeChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowVolumeChange else ExoPlayerControlsUiState.HideAllControls
                        },
                        onPlayBackPositionChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowSeekChange else ExoPlayerControlsUiState.HideAllControls
                            if (it) {
                                smartExoPlayer.softPause()
                            } else {
                                smartExoPlayer.softPlay()
                            }
                        },
                        onTapListener = {
                            controlsState = when (controlsState) {
                                ExoPlayerControlsUiState.HideAllControls -> {
                                    ExoPlayerControlsUiState.ShowAllControls
                                }

                                ExoPlayerControlsUiState.ShowPlayBackSpeedChangeControl -> {
                                    ExoPlayerControlsUiState.ShowAllControls
                                }

                                else -> {
                                    ExoPlayerControlsUiState.HideAllControls
                                }
                            }
                        },
                        onDoubleTapListener = {
                            if (videoPlayerState.isPlaying.value)
                                smartExoPlayer.hardPause()
                            else
                                smartExoPlayer.hardPlay()
                        },
                        showBrightnessChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowBrightnessChange,
                        showVolumeChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowVolumeChange,
                        showPlayBackSeekChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowSeekChange
                    )
                } else {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .clickable(interactionSource = remember { MutableInteractionSource() },
                                indication = null,
                                onClick = {
                                    controlsState =
                                        if (controlsState == ExoPlayerControlsUiState.HideAllControls) {
                                            ExoPlayerControlsUiState.ShowAllControls
                                        } else {
                                            ExoPlayerControlsUiState.HideAllControls
                                        }
                                })
                    ) {}
                }

                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.SpaceBetween
                ) {

                    AnimatedVisibility(
                        visible = controlsState == ExoPlayerControlsUiState.ShowAllControls,
                        enter = fadeIn(),
                        exit = fadeOut(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f, true)
                    ) {

                        UpperSectionForVideoPreview(
                            modifier = Modifier.fillMaxSize(),
                            onCloseMiniPlayerRequested = onClosePreviewRequested,
                            onFullScreenModeRequested = onFullScreenModeRequested,
                        )
                    }


                    AnimatedVisibility(
                        visible = controlsState == ExoPlayerControlsUiState.ShowAllControls,
                        enter = fadeIn(),
                        exit = fadeOut(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f, true)
                    ) {


                        LowerSectionForPreview(
                            modifier = Modifier.fillMaxSize(),
                            seekStarted = {
                                smartExoPlayer.softPause()
                            },
                            seekTo = {
                                currentPlayingMedia.lastPlayedPosition.value = it
                                smartExoPlayer.seekTo(it)
                            },
                            seekEnded = {
                                smartExoPlayer.softPlay()
                            },
                            totalDuration = currentPlayingMedia.totalDuration.value,
                            currentValue = currentPlayingMedia.lastPlayedPosition.value,
                            bufferedPosition = smartExoPlayer.videoPlayerState.bufferingPosition.value,
                            videoComments = videoComments,
                            addComment = { comment, position ->
                                scope.launch(Dispatchers.IO) {
                                    createVideoCommentItemForVideo(
                                        context = context,
                                        videoItem = currentPlayingMedia.videoItem.value,
                                        comment = comment,
                                        position = position,
                                        smartExoPlayer = smartExoPlayer
                                    ) {
                                        Log.d("TAG(((((((((((((", "SmartVideoPreviewUI: $it")
                                        addComment(
                                            currentPlayingMedia.videoItem.value,
                                            it
                                        )

                                    }
                                }
                            }
                        )

                    }

                }

                AnimatedVisibility(
                    visible = videoPlayerState.isBuffering.value,
                    enter = fadeIn(),
                    exit = fadeOut(),
                    modifier = Modifier
                        .fillMaxSize(0.5f)
                        .align(Alignment.Center)
                ) {
                    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loading))
                    LottieAnimation(
                        composition = composition,
                        iterations = LottieConstants.IterateForever,
                    )
                }


                AnimatedVisibility(
                    visible = controlsState == ExoPlayerControlsUiState.ShowAllControls,
                    enter = fadeIn(),
                    exit = fadeOut(),
                    modifier = Modifier
                        .align(Alignment.Center)
                ) {
                    PlayButton(
                        modifier = Modifier
                            .height(50.dp)
                            .aspectRatio(1f),
                        isPlaying = videoPlayerState.isPlaying.value,
                        togglePlaying = {
                            if (it) {
                                smartExoPlayer.hardPlay()
                            } else {
                                smartExoPlayer.hardPause()
                            }
                        },
                        paddingValues = PaddingValues(12.dp)
                    )
                }
            }

        }


}


