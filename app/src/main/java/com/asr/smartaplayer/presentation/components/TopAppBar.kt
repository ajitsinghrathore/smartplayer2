@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)

package com.asr.smartaplayer.presentation.components

import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.SemanticsProperties.ImeAction
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.AppDestination
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton

@Composable
fun TopBar(
    hasPrivateAccess: Boolean,
    currentDestination: AppDestination,
    currentSearchText: String?,
    onSearchTextChange: (String?)-> Unit,
    onPrivateAccessToggled: (Boolean) -> Unit,
) {



    AnimatedVisibility(
        visible = currentSearchText != null,
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp),
        enter = slideInHorizontally { it },
        exit = slideOutHorizontally { it }
    ){

        val focusRequester = remember { FocusRequester() }

        LaunchedEffect(currentSearchText != null) {
            focusRequester.requestFocus()
        }

        TextField(
            modifier = Modifier.fillMaxSize().focusRequester(focusRequester),
            value = currentSearchText?:"",
            onValueChange = {
                onSearchTextChange(it)
            },
            maxLines = 1,
            placeholder = { Text(text = "search videos..") },
            colors = TextFieldDefaults.colors(
                focusedContainerColor = MaterialTheme.colorScheme.background,
                unfocusedContainerColor = MaterialTheme.colorScheme.background
            ),
            trailingIcon = {
                GeneralIconButton(
                    modifier = Modifier.size(50.dp),
                    onClick = {
                              onSearchTextChange(null)
                    },
                    iconColor = MaterialTheme.colorScheme.onSurfaceVariant,
                    iconId = com.airbnb.lottie.R.drawable.abc_ic_clear_material,
                    contentDescription = "clear",
                    paddingValues = PaddingValues(10.dp)
                )
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = androidx.compose.ui.text.input.ImeAction.Search
            ),
        )




    }


    AnimatedVisibility(
        visible = currentSearchText == null,
        enter = slideInVertically(initialOffsetY = { -it}) ,
        exit = slideOutVertically{ -it}
    ){
        TopAppBar(
            title = {
                Text(
                    text = "Smart Player",
                    style = MaterialTheme.typography.bodyLarge
                )
            },
            actions = {
                if (currentDestination is AppDestination.HomeScreen){
                    IconButton(onClick = { onSearchTextChange("") }){
                        Image(
                            painter = painterResource(id = R.drawable.baseline_search_24),
                            contentDescription = "search videos",
                            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground),
                            modifier = Modifier.size(30.dp)
                        )
                    }
                }
                IconButton(onClick = { onPrivateAccessToggled(!hasPrivateAccess)}){
                    Image(
                        painter = painterResource(id = if (hasPrivateAccess) R.drawable.baseline_visibility_24 else R.drawable.baseline_visibility_off_24),
                        contentDescription = "",
                        colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.primary),
                        modifier = Modifier.size(30.dp)
                    )
                }
            },
            colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                containerColor = MaterialTheme.colorScheme.background,
                scrolledContainerColor = MaterialTheme.colorScheme.background,
                navigationIconContentColor = MaterialTheme.colorScheme.onBackground,
                titleContentColor = MaterialTheme.colorScheme.onBackground,
                actionIconContentColor = MaterialTheme.colorScheme.onBackground,
            )
        )
    }


}

