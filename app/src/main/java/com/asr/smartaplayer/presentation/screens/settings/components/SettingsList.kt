@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class
)

package com.asr.smartaplayer.presentation.screens.settings.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.RadioButton
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun SettingsList(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    title: String,
    items: List<String>,
    icon: Int? = null,
    currentSelectedIndex: Int?,
    description: String,
    onItemSelected: (Int, String) -> Unit
) {


    var showBottomSheet by remember { mutableStateOf(false) }

    val sheetState = rememberModalBottomSheetState()

    val safeSubtitle =
        if (currentSelectedIndex != null) items[currentSelectedIndex] else description

    SettingsMenuLink(
        modifier = modifier,
        enabled = enabled,
        icon = {
            if (icon != null) {
                Image(
                    modifier = Modifier
                        .size(20.dp),
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }
        },
        title = title,
        subtitle = safeSubtitle,
        onClick = { showBottomSheet = true },
    )
    val coroutineScope = rememberCoroutineScope()

    if (!showBottomSheet) return

    val scrollState = rememberScrollState()
    val onSelected: () -> Unit = {
        coroutineScope.launch {
            showBottomSheet = false
        }
    }

    ModalBottomSheet(
        sheetState = sheetState,
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = { showBottomSheet = false },
    ) {
        Column(modifier = Modifier.padding(20.dp)) {
            Text(
                text = title,
                color = MaterialTheme.colorScheme.onBackground,
                style = MaterialTheme.typography.bodyLarge
            )
            if (description.isNotBlank()) {
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    text = description,
                    color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.8f),
                    style = MaterialTheme.typography.bodySmall
                )
            }
            Column(
                modifier = Modifier.verticalScroll(scrollState)
            ) {
                items.forEachIndexed { index, item ->
                    Row(
                        Modifier.padding(horizontal = 5.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        RadioButton(
                            selected = (index == currentSelectedIndex),
                            onClick = {
                                onSelected()
                                onItemSelected(index, item)
                            }
                        )
                        androidx.compose.material3.Text(
                            text = item,
                            style = MaterialTheme.typography.labelMedium,
                            fontWeight = FontWeight.Bold,
                            color = MaterialTheme.colorScheme.onSurfaceVariant
                        )
                    }
                }
            }

        }

    }


}