package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components

import android.content.res.Configuration
import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import com.exyte.animatednavbar.utils.noRippleClickable


@Composable
fun BoxScope.SideSheetForControls(
    openFromLeft: Boolean = false,
    isVisible: Boolean,
    onSheetCloseRequested: () -> Unit,
    content: @Composable (Modifier) -> Unit
){

    val configuration = LocalConfiguration.current

    if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        val enterAnim = if (openFromLeft) {
            slideInHorizontally(initialOffsetX = {-it}) + fadeIn()
        }else{
            slideInHorizontally(initialOffsetX = {it}) + fadeIn()
        }
        val exitAnim = if(openFromLeft){
            slideOutHorizontally(targetOffsetX = { -it}) + fadeOut()
        }else{
            slideOutHorizontally(targetOffsetX = { it }) + fadeOut()
        }

        AnimatedVisibility(
            visible = isVisible,
            enter = enterAnim,
            exit = exitAnim,
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(0.7f)
                .noRippleClickable {
                    onSheetCloseRequested()
                }
                .align(Alignment.CenterEnd)
        ) {
            val modifierForContent = Modifier
                .fillMaxSize()
                .background(
                    Brush.horizontalGradient(
                        listOf(
                            Color.Transparent,
                            MaterialTheme.colorScheme.background.copy(alpha = 0.9f),
                            MaterialTheme.colorScheme.background.copy(alpha = 0.9f),
                        )
                    )
                )
                .padding(start = 200.dp, end = 10.dp, top = 10.dp, bottom = 10.dp)
            Box (
                modifier = modifierForContent.noRippleClickable {}
            ){
                content(Modifier.fillMaxSize())
            }
        }

    }
    else {
        AnimatedVisibility(
            visible = isVisible,
            enter = slideInVertically(initialOffsetY = { it }) + fadeIn(),
            exit = slideOutVertically(targetOffsetY = { it }) + fadeOut(),
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .noRippleClickable {
                    onSheetCloseRequested()
                }
                .align(Alignment.BottomCenter)
        ) {
            val modifierForContent = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(
                    Brush.verticalGradient(
                        listOf(
                            Color.Transparent,
                            MaterialTheme.colorScheme.background.copy(alpha = 0.9f),
                            MaterialTheme.colorScheme.background.copy(alpha = 0.9f)
                        )
                    )
                )
                .padding(start = 10.dp, end = 10.dp, top = 300.dp, bottom = 25.dp)
            Box (
                modifier = modifierForContent.noRippleClickable {}
            ){
                content(Modifier.fillMaxWidth())
            }

        }
    }





}