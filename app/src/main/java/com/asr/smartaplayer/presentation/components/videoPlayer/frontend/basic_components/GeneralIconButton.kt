package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun GeneralIconButton(
    modifier: Modifier,
    onClick: () -> Unit,
    iconColor: Color,
    iconId: Int,
    contentDescription: String,
    backGround: Color? = null,
    paddingValues: PaddingValues = PaddingValues(0.dp),
    enabled: Boolean = true
){
    IconButton(
        modifier = modifier,
        onClick = { onClick() },
        colors = IconButtonDefaults.iconButtonColors(
            containerColor = backGround?: Color.Transparent,
            disabledContainerColor = backGround?.copy(alpha = 0.5f)?: Color.Transparent
        ),
        enabled = enabled
    ) {
        Image(
            painter = painterResource(id = iconId),
            contentDescription = contentDescription,
            colorFilter = if (enabled) ColorFilter.tint(iconColor) else ColorFilter.tint(iconColor.copy(alpha = 0.5f)),
            modifier = Modifier.fillMaxSize().padding(paddingValues).clip(CircleShape)
        )
    }

}