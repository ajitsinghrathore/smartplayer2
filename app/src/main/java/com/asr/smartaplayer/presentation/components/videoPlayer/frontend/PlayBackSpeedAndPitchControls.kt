package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SeekBar
import java.util.Locale


@Composable
fun PlaybackSpeedAndPitchControls(
    modifier: Modifier,
    currentPlayBackSpeed: Float,
    onPlaybackSpeedChange: (Float) -> Unit,
    currentPitchSpeed: Float,
    onPlaybackPitchChange: (Float) -> Unit
) {

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround
    ) {

        val categories = listOf("speed", "pitch")

        categories.forEach { category ->
            if (category == "speed"){
                Image(
                    modifier = Modifier.size(30.dp),
                    painter = rememberAsyncImagePainter(model = R.drawable.baseline_speed_24),
                    contentDescription = "adjust video speed",
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }

            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                val currentValue = if (category == "speed") currentPlayBackSpeed else currentPitchSpeed
                Text(
                    text = "%.2fX".format(currentValue),
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.height(5.dp))

                SeekBar(
                    modifier = Modifier,
                    endValue = 4f,
                    currentValue = currentValue,
                    startValue = 0.2f,
                    seekStarted = { },
                    seekTo = { value ->
                        if (category == "speed") {
                            onPlaybackSpeedChange(value)
                        } else {
                            onPlaybackPitchChange(value)
                        }
                    },
                    seekEnded = { },
                    thumbSize = DpSize(13.dp, 13.dp),
                    thumbPadding = PaddingValues(top = 3.dp),
                    vertical = true,
                    verticalHeight = 150.dp
                )
                Spacer(modifier = Modifier.height(5.dp))

                GeneralIconButton(
                    modifier = Modifier.size(50.dp),
                    onClick = {
                        if (category == "speed") {
                            onPlaybackSpeedChange(1f)
                        } else {
                            onPlaybackPitchChange(1f)
                        }
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = R.drawable.baseline_1x_mobiledata_24,
                    backGround = MaterialTheme.colorScheme.surface,
                    contentDescription = "reset speed or pitch to 1x",
                    paddingValues = PaddingValues(14.dp)
                )
                Spacer(modifier = Modifier.height(5.dp))

                Text(
                    text = category.replaceFirstChar {
                        if (it.isLowerCase()) it.titlecase(
                            Locale.getDefault()
                        ) else it.toString()
                    }, style = MaterialTheme.typography.bodySmall
                )

            }

            if (category == "pitch"){
                Image(
                    modifier = Modifier.size(30.dp),
                    painter = rememberAsyncImagePainter(model = R.drawable.icons8_tuning_fork_50),
                    contentDescription = "adjust video pitch",
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }
        }


    }


}

