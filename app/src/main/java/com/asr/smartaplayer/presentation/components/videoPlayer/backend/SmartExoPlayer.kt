package com.asr.smartaplayer.presentation.components.videoPlayer.backend

import android.content.Context
import android.media.audiofx.BassBoost
import android.media.audiofx.Equalizer
import android.media.audiofx.LoudnessEnhancer
import android.media.audiofx.PresetReverb
import android.net.Uri
import android.provider.MediaStore.Video.Media
import android.util.Log
import android.util.Rational
import android.util.TypedValue
import androidx.annotation.OptIn
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.core.graphics.toColorInt
import androidx.media3.common.AudioAttributes
import androidx.media3.common.AuxEffectInfo
import androidx.media3.common.C
import androidx.media3.common.Effect
import androidx.media3.common.Format
import androidx.media3.common.MediaItem
import androidx.media3.common.MediaItem.SubtitleConfiguration
import androidx.media3.common.MimeTypes
import androidx.media3.common.PlaybackParameters
import androidx.media3.common.Player
import androidx.media3.common.Tracks
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DataSource
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.datasource.DefaultDataSourceFactory
import androidx.media3.datasource.cache.CacheDataSource
import androidx.media3.datasource.cache.SimpleCache
import androidx.media3.effect.Contrast
import androidx.media3.effect.FrameDropEffect
import androidx.media3.effect.HslAdjustment
import androidx.media3.effect.RgbFilter
import androidx.media3.effect.ScaleAndRotateTransformation
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.dash.DashMediaSource
import androidx.media3.exoplayer.hls.HlsMediaSource
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import androidx.media3.exoplayer.source.MediaSource
import androidx.media3.exoplayer.source.MergingMediaSource
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import androidx.media3.exoplayer.source.SilenceMediaSource
import androidx.media3.exoplayer.source.SingleSampleMediaSource
import androidx.media3.exoplayer.util.EventLogger
import androidx.media3.ui.CaptionStyleCompat
import androidx.media3.ui.PlayerView
import com.asr.smartaplayer.domain.models.AudioEqualizerSettings
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.SubtitlesDisplaySettings
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.presentation.components.videoPlayer.getCurrentVolume
import com.asr.smartaplayer.presentation.components.videoPlayer.updateSystemBrightness
import com.asr.smartaplayer.presentation.components.videoPlayer.updateSystemVolume
import com.google.common.collect.ImmutableList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


// GRAY SCALE FILTER RgbFilter.createGrayscaleFilter()
// Inverted filter (negative)  RgbFilter.createInvertedFilter()
// rotate (optionally scale the video) ScaleAndRotateTransformation.Builder().setRotationDegrees(90f).build()
// contrast Contrast(float)
// frame drop effect FrameDropEffect.createDefaultFrameDropEffect(100f)
// Ligntness adjustment HslAdjustment.Builder().adjustLightness(80f).build()
// Hue  HslAdjustment.Builder().adjustHue(360f).build()
// saturation  HslAdjustment.Builder().adjustSaturation(50f).build()
// RgbAdjustment RgbAdjustment.Builder().setBlueScale(0f).setGreenScale(1f).setRedScale(1f).build()


@OptIn(UnstableApi::class)
class SmartExoPlayer(
    private val context: Context,
    private val onVideoPlayStateChangedCallback: (SmartPlayerVideoStateEvent) -> Unit,
    private val cache: SimpleCache,
    private val subtitlesDisplaySettings: SubtitlesDisplaySettings
) : Player.Listener {


    private var exoPlayer: ExoPlayer = ExoPlayer.Builder(context).build().apply {
        repeatMode = Player.REPEAT_MODE_ONE
        setAudioAttributes(
            AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.AUDIO_CONTENT_TYPE_MOVIE)
                .build(),
            true
        )
        setHandleAudioBecomingNoisy(true)
        setWakeMode(C.WAKE_MODE_LOCAL)
        playbackParameters = PlaybackParameters(1f)
        addAnalyticsListener(EventLogger("DRIVE"))
    }

    var audioEqualizer: Equalizer

    private var presetReverb: PresetReverb

    private var bassBoost: BassBoost

    private var loudnessEnhancer: LoudnessEnhancer

    private val driveDataSourceFactory = AuthHttpDataSourceFactory()

    private val cachedDriveDataSourceFactory = CacheDataSource.Factory()
        .setCache(cache)
        .setUpstreamDataSourceFactory(driveDataSourceFactory)
        .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

    var playerView: PlayerView? = null

    private var currentPositionUpdatingJob: Job? = null

    val videoPlayerState = VideoPlayerState(
        currentPlayingMedia = mutableStateOf(null),
        isPlaying = mutableStateOf(false),
        isBuffering = mutableStateOf(false),
        playWhenReady = mutableStateOf(true),
        bufferingPosition = mutableLongStateOf(0)
    )


    override fun onTracksChanged(tracks: Tracks) {
        super.onTracksChanged(tracks)

        for (trackGroup in tracks.groups) {
            // Group level information.
            val trackType = trackGroup.type
            val trackInGroupIsSelected = trackGroup.isSelected
            val trackInGroupIsSupported = trackGroup.isSupported
            for (i in 0 until trackGroup.length) {
                // Individual track information.
                val isSupported = trackGroup.isTrackSupported(i)
                val isSelected = trackGroup.isTrackSelected(i)
                val trackFormat = trackGroup.getTrackFormat(i)

                Log.d("SUBTITLE", "onTracksChanged: $trackFormat  ")
            }
        }
    }

    init {
        Log.d("EQUALIZER", ": init ------------------ ${exoPlayer.audioSessionId}")
        exoPlayer.addListener(this)
        audioEqualizer = Equalizer(0, exoPlayer.audioSessionId)
        presetReverb = PresetReverb(0, exoPlayer.audioSessionId)
        bassBoost = BassBoost(0, exoPlayer.audioSessionId)
        loudnessEnhancer = LoudnessEnhancer(exoPlayer.audioSessionId)
        audioEqualizer.enabled = true
    }

    private fun startUpdatingCurrentPosition() = flow {
        while (true) {
            delay(500)
            emit(exoPlayer.currentPosition)
        }
    }

    private fun setUpCurrentPositionUpdateJob(coroutineScope: CoroutineScope) {
        stopCurrentPositionUpdatingJob()
        currentPositionUpdatingJob = startUpdatingCurrentPosition().onEach {
            videoPlayerState.currentPlayingMedia.value?.lastPlayedPosition?.value = it
            videoPlayerState.bufferingPosition.value = exoPlayer.bufferedPosition
        }.launchIn(coroutineScope)
    }

    private fun stopCurrentPositionUpdatingJob() {
        currentPositionUpdatingJob?.cancel()
        currentPositionUpdatingJob = null
    }

    override fun onPlaybackStateChanged(playbackState: Int) {
        super.onPlaybackStateChanged(playbackState)
        Log.d("DRIVE", "onPlaybackStateChanged: $playbackState")

        when (playbackState) {

            Player.STATE_BUFFERING -> {
                videoPlayerState.isBuffering.value = true
            }

            Player.STATE_READY -> {
                videoPlayerState.isBuffering.value = false
                videoPlayerState.currentPlayingMedia.value?.totalDuration?.value =
                    exoPlayer.duration
                Log.d("TAG***", "ready state:  ${exoPlayer.videoSize.width} ${exoPlayer.videoSize.height}")
            }

            Player.STATE_ENDED -> {

            }

            Player.STATE_IDLE -> {

            }
        }
    }


    // methods for public use
    fun attachPlayerView(
        playerView: PlayerView,
        coroutineScope: CoroutineScope
    ) {
        playerView.player = exoPlayer
        this.playerView = playerView
        setUpCurrentPositionUpdateJob(coroutineScope = coroutineScope)
        playerView.subtitleView.let {
            Log.d("SUBTITLE", "attachPlayerView: $subtitlesDisplaySettings")
            it?.setFixedTextSize(TypedValue.COMPLEX_UNIT_SP, subtitlesDisplaySettings.textSize)
            it?.setStyle(
                CaptionStyleCompat(
                    subtitlesDisplaySettings.textColor.toColorInt(), // Text color
                    subtitlesDisplaySettings.backgroundColor.toColorInt(), // Background color (transparent)
                    Color.Transparent.toArgb(), // Window color (transparent)
                    CaptionStyleCompat.EDGE_TYPE_NONE, // Edge type
                    Color.Transparent.toArgb(), // Edge color (transparent)
                    null // Typeface (null for default)
                )
            )
        }
    }


    fun closeVideoPlayer() {
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_STOPPED_REQUEST)
        stopCurrentPositionUpdatingJob()
        videoPlayerState.reset()
        exoPlayer.clearAuxEffectInfo()
        exoPlayer.release()
        bassBoost.release()
        audioEqualizer.release()
        presetReverb.release()
        loudnessEnhancer.release()
    }


    private fun createSubTitleMediaSource(
        isSubtitlesEnabled: Boolean,
        selectedSubtitle: Subtitle?
    ): MediaSource? {
        val subtitleSource: MediaSource? = null
        if (isSubtitlesEnabled && selectedSubtitle != null) {
            val subTitleConfiguration = SubtitleConfiguration
                .Builder(selectedSubtitle.uri)
                .setMimeType(MimeTypes.APPLICATION_SUBRIP)
                .setSelectionFlags(C.SELECTION_FLAG_DEFAULT)
                .build()

            return SingleSampleMediaSource.Factory(DefaultDataSource.Factory(context)).createMediaSource(subTitleConfiguration, 0)

        }
        return subtitleSource
    }

    private fun sourceAgnosticPreparePlayer(
        videoItem: BaseVideoItem,
        videoMetaData: VideoMetaData,
        setMediaSource: (MediaSource?) -> Unit
    ) {
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_START_REQUEST)
        setMediaSource(
            createSubTitleMediaSource(
                videoMetaData.isSubtitlesEnabled, videoMetaData.selectedSubtitle
            )
        )
        exoPlayer.apply {
            this.seekTo(videoMetaData.lastPlayedPosition)
            this.playbackParameters =
                PlaybackParameters(videoMetaData.playBackSpeed, videoMetaData.playBackPitch)
            this.playWhenReady = true
        }
        videoPlayerState.currentPlayingMedia.value = CurrentPlayingMedia(
            videoItem = mutableStateOf(videoItem),
            lastPlayedPosition = mutableLongStateOf(videoMetaData.lastPlayedPosition),
            brightnessIntensity = mutableFloatStateOf(videoMetaData.brightnessIntensity),
            volumeLevel = mutableFloatStateOf(getCurrentVolume(context).toFloat()),
            playBackSpeed = mutableFloatStateOf(videoMetaData.playBackSpeed),
            playBackPitch = mutableFloatStateOf(videoMetaData.playBackPitch),
            totalDuration = mutableLongStateOf(videoMetaData.totalDuration),
            isSubtitleEnabled = mutableStateOf(videoMetaData.isSubtitlesEnabled),
            selectedSubtitle = mutableStateOf(videoMetaData.selectedSubtitle),
            visualEffect = mutableStateOf(videoMetaData.visualEffectsSettings),
            audioEqualizerSettings = mutableStateOf(videoMetaData.audioEqualizerSettings)
        )
        setVisualEffectsAndPrepare()
        setVolume(videoPlayerState.currentPlayingMedia.value?.volumeLevel!!.value, context)
        setBrightness(
            videoPlayerState.currentPlayingMedia.value?.brightnessIntensity!!.value,
            context
        )

        setPresetAudioMode(videoMetaData.audioEqualizerSettings.audioPresetMode)
        setBandValues(videoMetaData.audioEqualizerSettings.customAudioBandValues)
        setBassBoost(videoMetaData.audioEqualizerSettings.audioBassBoost)
        setReverbMode(videoMetaData.audioEqualizerSettings.audioReverbMode)
        setLoudnessBoost(videoMetaData.audioEqualizerSettings.audioLoudnessBoost)


        videoPlayerState.isPlaying.value = true
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_STARTED)
    }


    fun playVideo(
        videoItem: LocalVideoItem,
        videoMetaData: VideoMetaData
    ) = sourceAgnosticPreparePlayer(videoItem = videoItem, videoMetaData = videoMetaData) { subTitleMediaSource ->
        setMediaSourceForLocalVideos(videoItem, subTitleMediaSource)
    }

    private fun setMediaSourceForLocalVideos(videoItem: LocalVideoItem, subTitleMediaSource: MediaSource?){
        val videoMediaSource = DefaultMediaSourceFactory(context).createMediaSource(MediaItem.fromUri(videoItem.videoUrl))
        val finalMediaSource = if (subTitleMediaSource == null ) MergingMediaSource(videoMediaSource) else MergingMediaSource(videoMediaSource, subTitleMediaSource)
        exoPlayer.setMediaSource(finalMediaSource)
    }


    @UnstableApi
    fun playVideo(
        videoItem: DriveVideoItem,
        videoMetaData: VideoMetaData,
        authToken: String,
    ) = sourceAgnosticPreparePlayer(videoItem = videoItem, videoMetaData = videoMetaData) {subTitleMediaSource ->
        setMediaSourceForDriveVideos(videoItem, subTitleMediaSource, authToken)
    }


    private fun setMediaSourceForDriveVideos(videoItem: DriveVideoItem, subTitleMediaSource: MediaSource?,authToken: String? = null){
        authToken?.let {
            driveDataSourceFactory.setAuthToken(authToken = authToken)
        }
        val videoMediaSource = ProgressiveMediaSource.Factory(cachedDriveDataSourceFactory)
            .createMediaSource(MediaItem.fromUri(videoItem.videoUrl))
        val finalMediaSource = if (subTitleMediaSource == null ) MergingMediaSource(videoMediaSource) else MergingMediaSource(videoMediaSource, subTitleMediaSource)
        exoPlayer.setMediaSource(finalMediaSource)
    }

    fun hardPause() {
        exoPlayer.pause()
        videoPlayerState.isPlaying.value = false
        videoPlayerState.playWhenReady.value = false
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_PAUSED)
    }

    fun softPause() {
        exoPlayer.pause()
        videoPlayerState.isPlaying.value = false
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_PAUSED)
    }


    fun softPlay() {
        if (videoPlayerState.playWhenReady.value) {
            exoPlayer.play()
            videoPlayerState.isPlaying.value = true
            onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_RESUMED)
        }
    }

    fun hardPlay() {
        exoPlayer.play()
        videoPlayerState.isPlaying.value = true
        videoPlayerState.playWhenReady.value = true
        onVideoPlayStateChangedCallback(SmartPlayerVideoStateEvent.PLAYBACK_RESUMED)
    }

    fun seekTo(position: Long) {
        exoPlayer.seekTo(position)
        videoPlayerState.currentPlayingMedia.value?.lastPlayedPosition?.value = position
    }

    fun getVideoAspectRatio(): Rational {
        Log.d("TAG***", "getVideoAspectRatio:  ${exoPlayer.videoSize.width} ${exoPlayer.videoSize.height}")
        return Rational(exoPlayer.videoSize.width, exoPlayer.videoSize.height)
    }

    fun setPlayBackSpeed(speed: Float) {
        exoPlayer.apply {
            val currentPlayingMedia = videoPlayerState.currentPlayingMedia.value
            if (currentPlayingMedia != null) {
                this.playbackParameters = PlaybackParameters(
                    speed,
                    currentPlayingMedia.playBackPitch.value
                )
                currentPlayingMedia.playBackSpeed.value = speed
            }
        }
    }

    fun setPlayBackPitch(pitch: Float) {
        exoPlayer.apply {
            val currentPlayingMedia = videoPlayerState.currentPlayingMedia.value
            if (currentPlayingMedia != null) {
                this.playbackParameters = PlaybackParameters(
                    currentPlayingMedia.playBackSpeed.value,
                    pitch
                )
                currentPlayingMedia.playBackPitch.value = pitch
            }
        }
    }

    fun setBrightness(brightnessLevel: Float? = null, context: Context) {
        if (brightnessLevel != null) {
            updateSystemBrightness(context, brightnessLevel)
            videoPlayerState.currentPlayingMedia.value?.brightnessIntensity?.value = brightnessLevel
        } else {
            val brightnessState =
                videoPlayerState.currentPlayingMedia.value?.brightnessIntensity?.value
            if (brightnessState != null) {
                updateSystemBrightness(context, brightnessState)
            }
        }
    }

    fun setVolume(volumeLevel: Float, context: Context) {
        updateSystemVolume(context, volumeLevel)
        videoPlayerState.currentPlayingMedia.value?.volumeLevel?.value = volumeLevel
    }

    private fun withCurrentAudioEqualizerSettingsState(handler: (MutableState<AudioEqualizerSettings>) -> Unit) {
        val currentAudioEqualizerSettings =
            videoPlayerState.currentPlayingMedia.value?.audioEqualizerSettings
        currentAudioEqualizerSettings?.let {
            handler(it)
        }
    }

    @UnstableApi
    fun setReverbMode(mode: Int, ) = withCurrentAudioEqualizerSettingsState {
        if (mode == 0) {
            exoPlayer.clearAuxEffectInfo()
        } else {
            presetReverb.preset = mode.toShort()
            presetReverb.enabled = true
            exoPlayer.setAuxEffectInfo(AuxEffectInfo(presetReverb.id, 1f))
        }
        it.value = it.value.copy(audioReverbMode = mode)
    }

    fun setBassBoost(strength: Int, ) = withCurrentAudioEqualizerSettingsState {
        bassBoost.enabled = strength != 0
        bassBoost.setStrength(strength.toShort())
        it.value = it.value.copy(audioBassBoost = strength)
    }

    fun setLoudnessBoost(audioLoudnessBoost: Int, ) = withCurrentAudioEqualizerSettingsState {
        loudnessEnhancer.setTargetGain(audioLoudnessBoost)
        loudnessEnhancer.enabled = audioLoudnessBoost != 0
        it.value = it.value.copy(audioLoudnessBoost = audioLoudnessBoost)
    }

    fun setPresetAudioMode(mode: Int, )  = withCurrentAudioEqualizerSettingsState{
        if (mode >= 0) {
            audioEqualizer.usePreset(mode.toShort())
            val updatedBandValues = mutableMapOf<String, Short>()
            val bands = audioEqualizer.numberOfBands
            for(bandIndex in 0  until bands){
                updatedBandValues[bandIndex.toString()] = audioEqualizer.getBandLevel(bandIndex.toShort())
            }
            it.value = it.value.copy(customAudioBandValues = updatedBandValues)
        }
        it.value  = it.value.copy(audioPresetMode = mode)
    }

    fun setBandValues(mapOfValues: Map<String, Short>, )  = withCurrentAudioEqualizerSettingsState{
        val updatedBandValues = it.value.customAudioBandValues + mapOfValues
        updatedBandValues.forEach{(index, value) ->
            audioEqualizer.setBandLevel(index.toShort(), value)
        }
        it.value = it.value.copy(customAudioBandValues = updatedBandValues)
    }

    fun enableSubtitle(enable: Boolean) {
        val currentPlayingMedia = videoPlayerState.currentPlayingMedia.value
        currentPlayingMedia?.let {
            currentPlayingMedia.isSubtitleEnabled.value = enable
            if (!enable) {
                currentPlayingMedia.selectedSubtitle.value = null
            }
            val updatedSubTitleMediaSource = createSubTitleMediaSource(
                enable,
                selectedSubtitle = currentPlayingMedia.selectedSubtitle.value
            )
            when (val videoItem = currentPlayingMedia.videoItem.value) {
                is LocalVideoItem -> {
                    setMediaSourceForLocalVideos(videoItem, updatedSubTitleMediaSource)
                }

                is DriveVideoItem -> {
                    setMediaSourceForDriveVideos(videoItem, updatedSubTitleMediaSource)
                }
            }

            exoPlayer.apply {
                this.seekTo(currentPlayingMedia.lastPlayedPosition.value)
                this.playbackParameters = PlaybackParameters(
                    currentPlayingMedia.playBackSpeed.value, currentPlayingMedia.playBackPitch.value
                )
            }
        }
    }

    fun selectSubtitle(subtitle: Subtitle) {
        videoPlayerState.currentPlayingMedia.value?.selectedSubtitle?.value = subtitle
        enableSubtitle(true)
    }

    fun setVisualEffectsAndPrepare() {
        val currentPlayingMedia = videoPlayerState.currentPlayingMedia.value
        val listOfEffects = mutableListOf<Effect>()
        currentPlayingMedia?.let {
            val currentVisualEffect = currentPlayingMedia.visualEffect.value
            if (currentVisualEffect.enabled) {
                if (currentVisualEffect.grayScaleEnabled) {
                    listOfEffects.add(RgbFilter.createGrayscaleFilter())
                }
                if (currentVisualEffect.invertedColorsEnabled) {
                    listOfEffects.add(RgbFilter.createInvertedFilter())
                }
                listOfEffects.add(
                    ScaleAndRotateTransformation
                        .Builder()
                        .setRotationDegrees(currentVisualEffect.videoTotalRotation)
                        .build()
                )
                listOfEffects.add(
                    Contrast(currentVisualEffect.contrast)
                )
                listOfEffects.add(
                    HslAdjustment.Builder().adjustHue(currentVisualEffect.hue).build()
                )
                listOfEffects.add(
                    HslAdjustment.Builder().adjustSaturation(currentVisualEffect.saturation).build()
                )
            }
            Log.d("DRIVE", "setVisualEffects: $listOfEffects")
        }
//        exoPlayer.setVideoEffects(listOfEffects)
        exoPlayer.prepare()
    }
}


