package com.asr.smartaplayer.presentation.drowsyDetection

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import kotlin.math.max

class CameraLifeCycleOwner(
    private val smartExoPlayer: SmartExoPlayer?,
    private val sensitivity: Int
) : LifecycleOwner {

    private var drowsyScore = 0
    private val lifecycleRegistry: LifecycleRegistry = LifecycleRegistry(this)

    override val lifecycle: Lifecycle = lifecycleRegistry


    fun updateDrowsyState(isAwake: Boolean) {
        if (smartExoPlayer!= null) {
            drowsyScore = if (!isAwake) {
                Integer.min(drowsyScore + 1, sensitivity + sensitivity/2)
            } else {
                max(drowsyScore - 1, 0)
            }

            if (drowsyScore > sensitivity) {
                smartExoPlayer.softPause()
            } else {
                smartExoPlayer.softPlay()
            }

        }


    }


    fun startLifeCycle() {
        Log.d("DROWSY", "startLifeCycle: ")
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    fun stopLifeCycle() {
        Log.d("DROWSY", "stopLifeCycle: ")
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }


}

