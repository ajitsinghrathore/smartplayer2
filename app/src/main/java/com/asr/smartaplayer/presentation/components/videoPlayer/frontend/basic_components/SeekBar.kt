@file:OptIn(ExperimentalMaterial3Api::class)

package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components

import android.util.Log
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.layout
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.VideoComment
import dev.vivvvek.seeker.Seeker
import dev.vivvvek.seeker.SeekerDefaults
import dev.vivvvek.seeker.Segment
import dev.vivvvek.seeker.rememberSeekerState


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SeekBar(
    modifier: Modifier,
    endValue: Float,
    currentValue: Float,
    seekStarted: () -> Unit,
    seekTo: (Float) -> Unit,
    seekEnded: () -> Unit,
    thumbSize: DpSize,
    thumbPadding: PaddingValues,
    startValue: Float = 0f,
    vertical: Boolean = false,
    verticalHeight: Dp = 50.dp,
    enabled: Boolean = true
) {

    var isSeeking by remember {
        mutableStateOf(false)
    }

    val interactionSource = remember { MutableInteractionSource() }
    val colors = SliderDefaults.colors(
        thumbColor = MaterialTheme.colorScheme.primary,
        activeTrackColor = MaterialTheme.colorScheme.onBackground,
        inactiveTrackColor = MaterialTheme.colorScheme.surface,
        disabledThumbColor = MaterialTheme.colorScheme.primary.copy(alpha = 0.6f),
        disabledActiveTrackColor = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.6f)
    )

    Log.d("SUBTITLE", "SeekBar: $enabled")

    var finalModifier = modifier

    if (vertical){
        finalModifier = modifier.graphicsLayer {
            rotationZ = 270f
            transformOrigin = TransformOrigin(0f, 0f)
        }.layout { measurable, constraints ->
                val placeable = measurable.measure(
                    Constraints(
                        minWidth = constraints.minHeight,
                        maxWidth = constraints.maxHeight,
                        minHeight = constraints.minWidth,
                        maxHeight = constraints.maxHeight,
                    )
                )
                layout(placeable.height, placeable.width) {
                    placeable.place(-placeable.width, 0)
                }
            }.width(verticalHeight)
    }


    Slider(
        modifier = finalModifier,
        value = currentValue,
        enabled = enabled,
        onValueChange = {
            if (!isSeeking) {
                isSeeking = true
                seekStarted()
            } else {
                seekTo(it)
            }

        },
        valueRange = startValue..endValue,
        onValueChangeFinished = {
            isSeeking = false
            seekEnded()
        },
        colors = colors,
        interactionSource = interactionSource,
        thumb = {
            SliderDefaults.Thumb(
                modifier = Modifier.padding(thumbPadding),
                interactionSource = interactionSource,
                colors = colors,
                enabled = enabled,
                thumbSize = thumbSize
            )
        },
    )


}



data class PlayBackSegmentsData(
    val segments: List<Segment>,
    val minSegmentStart: Long,
    val maxSegmentStart: Long
)


@Composable
fun PlayBackSeekBar(
    modifier: Modifier,
    endValue: Float,
    currentValue: Float,
    seekStarted: () -> Unit,
    seekTo: (Float) -> Unit,
    seekEnded: () -> Unit,
    startValue: Float = 0f,
    bufferedPosition: Float,
    thumbRadius: Dp,
    trackHeight: Dp,
    allSegments: List<Segment>,
) {

    var isSeeking by remember {
        mutableStateOf(false)
    }

    val interactionSource = remember { MutableInteractionSource() }


    Seeker(
        modifier = modifier,
        value = currentValue,
        onValueChange = {

            Log.d("SEEKING", "SeekBar:  still seeking")
            if (!isSeeking) {
                isSeeking = true
                seekStarted()
            } else {
                seekTo(it)
            }
        },
        range = startValue..endValue,
        onValueChangeFinished = {
            isSeeking = false
            seekEnded()
        },
        colors = SeekerDefaults.seekerColors(
            progressColor = MaterialTheme.colorScheme.onBackground,
            readAheadColor = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.6f),
            thumbColor = MaterialTheme.colorScheme.primary,
            trackColor = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.4f),
        ),
        interactionSource = interactionSource,
        readAheadValue = bufferedPosition,
        dimensions = SeekerDefaults.seekerDimensions(
            thumbRadius = thumbRadius,
            progressHeight = trackHeight,
            trackHeight = trackHeight,
            gap = 5.dp
        ),
        segments =allSegments
    )


}