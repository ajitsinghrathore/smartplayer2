package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.media.audiofx.Equalizer
import android.media.audiofx.PresetReverb
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SeekBar
import com.asr.smartaplayer.presentation.components.videoPlayer.readableDb
import com.asr.smartaplayer.presentation.components.videoPlayer.readableHertz
import com.exyte.animatednavbar.utils.noRippleClickable


@Composable
fun AudioEqualizerComponent(
    modifier: Modifier,
    equalizer: Equalizer,
    currentPresetMode: Int,
    onPresetChange: (Int) -> Unit,
    currentReverbMode: Int,
    onReverbSelected: (Int) -> Unit,
    currentBassBoost: Int,
    onBassBoostUpdate: (Int) -> Unit,
    currentLoudnessBoost: Int,
    onLoudnessBoostChange: (Int) -> Unit,
    currentBandValues: Map<String, Short>,
    onBandValuesUpdate: (String, Short) -> Unit
) {


    val lowerEqualizerBandLevelMilliBel: Short = equalizer.bandLevelRange[0]
    val upperEqualizerBandLevelMilliBel: Short = equalizer.bandLevelRange[1]

    Column(
        modifier = modifier
            .noRippleClickable { }
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val allPresets = remember {
            listOf("Custom") + List(equalizer.numberOfPresets.toInt()) {
                equalizer.getPresetName(
                    it.toShort()
                )
            }
        }

        HorizontalListOfChips(
            modifier = Modifier.fillMaxWidth(),
            selectedChipIndex = currentPresetMode + 1,
            chips = allPresets,
            onChipSelected = {
                onPresetChange(it - 1)
            },
            disabledChips = if (currentPresetMode == 0) emptySet() else setOf("Custom")
        )

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            val bands = remember {
                val bandCount = equalizer.numberOfBands
                val bandIndices = (0 until bandCount).toList().map { it.toShort() }
                val bandFrequencies =
                    bandIndices.map { readableHertz(equalizer.getCenterFreq(it)) }
                bandIndices.zip(bandFrequencies)
            }

            bands.forEach { (bandIndex, bandFrequency) ->


                Column(horizontalAlignment = Alignment.CenterHorizontally) {

                    Text(
                        text = readableDb(equalizer.getBandLevel(bandIndex)),
                        style = MaterialTheme.typography.bodySmall
                    )
                    Spacer(modifier = Modifier.height(5.dp))

                    SeekBar(
                        modifier = Modifier,
                        endValue = upperEqualizerBandLevelMilliBel.toFloat(),
                        currentValue = currentBandValues[bandIndex.toString()]!!.toFloat(),
                        startValue = lowerEqualizerBandLevelMilliBel.toFloat(),
                        seekStarted = { },
                        seekTo = { value ->
                            onPresetChange(-1)
                            onBandValuesUpdate(bandIndex.toString(), value.toInt().toShort())
                        },
                        seekEnded = { },
                        thumbSize = DpSize(13.dp, 13.dp),
                        thumbPadding = PaddingValues(top = 3.dp),
                        vertical = true,
                        verticalHeight = 150.dp
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(text = bandFrequency, style = MaterialTheme.typography.bodySmall)
                }


            }


        }


        Spacer(modifier = Modifier.height(10.dp))
        val allReverbAvailable = mapOf(
            PresetReverb.PRESET_NONE to "None",
            PresetReverb.PRESET_SMALLROOM to "Small room",
            PresetReverb.PRESET_MEDIUMROOM to "Medium room",
            PresetReverb.PRESET_LARGEROOM to "Large room",
            PresetReverb.PRESET_MEDIUMHALL to "Medium hall",
            PresetReverb.PRESET_LARGEHALL to "Large hall",
            PresetReverb.PRESET_PLATE to "Plate"
        )

        Text(text = "Reverb modes", style = MaterialTheme.typography.bodySmall)

        Spacer(modifier = Modifier.height(10.dp))

        HorizontalListOfChips(
            modifier = Modifier.fillMaxWidth(),
            selectedChipIndex = currentReverbMode,
            chips = allReverbAvailable.values.toList(),
            onChipSelected = {
                onReverbSelected(it)
            },
            disabledChips = emptySet()
        )


        Spacer(modifier = Modifier.height(10.dp))

        Text(text = "Bass Boost", style = MaterialTheme.typography.bodySmall)

        Spacer(modifier = Modifier.height(8.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            SeekBar(
                modifier = Modifier.weight(1f),
                endValue = 1000f,
                currentValue = currentBassBoost.toFloat(),
                seekStarted = { },
                seekTo = {
                    onBassBoostUpdate(it.toInt())
                },
                seekEnded = { },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp),
            )
            Spacer(modifier = Modifier.width(5.dp))
            Text(
                text = "${(currentBassBoost) / 10}%",
                style = MaterialTheme.typography.bodySmall
            )
        }

        Spacer(modifier = Modifier.height(8.dp))

        Text(text = "Loudness Boost", style = MaterialTheme.typography.bodySmall)

        Spacer(modifier = Modifier.height(8.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            SeekBar(
                modifier = Modifier.weight(1f),
                endValue = 1000f,
                currentValue = currentLoudnessBoost.toFloat(),
                seekStarted = { },
                seekTo = {
                    onLoudnessBoostChange(it.toInt())
                },
                seekEnded = { },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp),
            )
            Spacer(modifier = Modifier.width(5.dp))
            Text(
                text = "${(currentLoudnessBoost) / 10}%",
                style = MaterialTheme.typography.bodySmall
            )
        }


    }

}


@Composable
fun HorizontalListOfChips(
    modifier: Modifier,
    selectedChipIndex: Int,
    chips: List<String>,
    onChipSelected: (Int) -> Unit,
    disabledChips: Set<String>
) {

    ScrollableTabRow(
        selectedTabIndex = selectedChipIndex,
        containerColor = Color.Transparent,
        indicator = {},
        divider = {},
        modifier = modifier,
        edgePadding = 0.dp
    ) {

        for (presetIndex in chips.indices) {
            val preset = chips[presetIndex]
            TextButton(
                enabled = preset !in disabledChips,
                onClick = { onChipSelected(presetIndex) },
                colors = ButtonDefaults.textButtonColors(
                    containerColor = MaterialTheme.colorScheme.surface,
                    disabledContainerColor = MaterialTheme.colorScheme.surface.copy(alpha = 0.68f)
                ),
                modifier = Modifier
                    .padding(5.dp)
                    .height(30.dp),
                contentPadding = PaddingValues(horizontal = 5.dp),
                shape = RoundedCornerShape(50.dp)
            ) {
                Text(
                    text = preset,
                    color = if (selectedChipIndex == presetIndex) MaterialTheme.colorScheme.onSurfaceVariant else MaterialTheme.colorScheme.onSurface,
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(0.dp)
                )
            }

        }

    }

}













