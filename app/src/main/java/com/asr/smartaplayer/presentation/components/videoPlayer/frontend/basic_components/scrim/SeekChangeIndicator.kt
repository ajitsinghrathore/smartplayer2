package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import java.util.*
import kotlin.math.abs

@Composable
fun SeekChangeIndicator(
    modifier: Modifier,
    startPosition: Long,
    incrementDiff: Long
) {


    Box(
        modifier = modifier.background(
            Brush.radialGradient(listOf(MaterialTheme.colorScheme.background, Color.Transparent))
        ),
        contentAlignment = Alignment.Center
    ) {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {

            Text(
                text = getIncrementedDurationText(incrementDiff.toInt()),
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.primary,
            )

            Spacer(modifier = Modifier.height(10.dp))

            Text(
                text = humanReadableDuration(startPosition + incrementDiff),
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.primary,
            )
        }


    }


}


fun getIncrementedDurationText(seekTimeInMillis: Int): String {

    return if (seekTimeInMillis > 0) {
        "+(${humanReadableDuration(seekTimeInMillis.toLong())})"
    } else {
        "-(${humanReadableDuration(abs(seekTimeInMillis).toLong())})"
    }

}


fun humanReadableDuration(duration: Long): String =
    String.format("%tT", duration - TimeZone.getDefault().rawOffset)














