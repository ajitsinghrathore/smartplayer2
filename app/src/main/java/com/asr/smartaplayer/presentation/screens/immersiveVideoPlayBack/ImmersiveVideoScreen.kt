@file:OptIn(ExperimentalPermissionsApi::class, ExperimentalPermissionsApi::class,
    ExperimentalPermissionsApi::class, ExperimentalPermissionsApi::class
)

package com.asr.smartaplayer.presentation.screens.immersiveVideoPlayBack

import android.Manifest
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.ImmersiveVideoPlayer
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState


@Composable
fun ImmersiveVideoScreen(
    exitImmersiveMode: () -> Unit,
    smartExoPlayer: SmartExoPlayer?,
    onPIPRequest: () -> Unit,
    videoComments: List<VideoComment>,
    onCommentAddRequest: (BaseVideoItem, VideoComment) -> Unit,
    onCommentDeleteRequest: (BaseVideoItem, VideoComment) -> Unit,
    viewModel: ImmersiveVideoViewModel
) {


    Box(modifier = Modifier.fillMaxSize()) {
        val context = LocalContext.current
        DisposableEffect(smartExoPlayer) {

            if (smartExoPlayer != null) {
                viewModel.initializeCameraLifeCycleOwner(
                    context = context,
                    exoPlayer = smartExoPlayer
                )
                if (viewModel.isDrowsyEnabledRequested.value) {
                    viewModel.toggleDrowsyDetection(true)
                }
            }

            onDispose {
                viewModel.toggleDrowsyDetection(false)
            }
        }

        val permissionState =
            rememberPermissionState(permission = Manifest.permission.CAMERA)

        val showPermissionDialog = remember {
            mutableStateOf(permissionState.hasPermission)
        }



        ImmersiveVideoPlayer(
            modifier = Modifier.fillMaxSize(),
            smartExoPlayer = smartExoPlayer,
            onExitImmersiveMode = exitImmersiveMode,
            onDrowsyStateClick = {
                if (permissionState.hasPermission) {
                    viewModel.toggleDrowsyDetection(it)
                    viewModel.isDrowsyEnabledRequested.value = it
                } else {
                    showPermissionDialog.value = true
                }
            },
            isDrowsyEnabled = viewModel.isDrowsyEnabled.value,
            videoComments = videoComments,
            onPIPRequest = {
                onPIPRequest()
            },
            onCommentAddRequest = onCommentAddRequest,
            onCommentDeleteRequest = { commentItem, videoItem ->
                onCommentDeleteRequest(videoItem, commentItem)
            },
            loadMoreSubtitles = { paginationState, searchText ->
                viewModel.loadSubtitles(paginationState, searchText)
            }
        )

        if (!permissionState.hasPermission && showPermissionDialog.value) {

            CameraPermissionDialog(
                permissionState = permissionState,
                showPermissionDialog = showPermissionDialog
            )

        }


    }


}