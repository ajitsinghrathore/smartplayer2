package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim

import android.util.Log
import kotlin.math.abs
import kotlin.math.roundToLong

abstract class GestureDetector : ScrimGestureDetector() {


    abstract fun getCurrentBrightness(): Float
    abstract fun getCurrentVolume(): Float
    abstract fun getCurrentPlayBackPosition(): Long
    abstract fun getTotalDuration(): Long

    abstract fun brightnessChangeDetected(started: Boolean)
    abstract fun volumeChangeDetected(started: Boolean)
    abstract fun playBackPositionChangeDetected(started: Boolean)

    abstract val deviceMaxVolume: Float

    abstract val deviceMinVolume: Float

    abstract fun postUpdatedBrightness(value: Float)
    abstract fun postUpdatedVolume(value: Float)
    abstract fun postUpdatedPlayBackPosition(value: Long)

    abstract fun onTapListener()

    abstract fun onDoubleTapListener()

    private var gestureStartBrightness: Float? = null
    private var gestureStartVolume: Float? = null
    private var gestureStartPlayBackPosition: Long? = null


    override fun onGestureStarted(gesture: ScrimGesture) {
        Log.d("GESTURES", "onGestureStarted: $gesture")
        when (gesture) {
            ScrimGesture.LeftUpDown -> {
                gestureStartBrightness = getCurrentBrightness()
                brightnessChangeDetected(true)
            }

            ScrimGesture.RightUpDown -> {
                gestureStartVolume = getCurrentVolume()
                volumeChangeDetected(true)
            }

            ScrimGesture.HorizontalSwipe -> {
                gestureStartPlayBackPosition = getCurrentPlayBackPosition()
                playBackPositionChangeDetected(true)
            }

            ScrimGesture.Click -> {}
            ScrimGesture.Unknown -> {}
            ScrimGesture.DoubleClick ->  {}
        }
    }

    override fun onGestureEnded(gesture: ScrimGesture) {
        Log.d("GESTURES", "onGestureEnded: $gesture")
        when (gesture) {
            ScrimGesture.LeftUpDown -> {
                gestureStartBrightness = null
                brightnessChangeDetected(false)
            }
            ScrimGesture.RightUpDown -> {
                gestureStartVolume = null
                volumeChangeDetected(false)
            }
            ScrimGesture.HorizontalSwipe -> {
                gestureStartPlayBackPosition = null
                playBackPositionChangeDetected(false)
            }
            ScrimGesture.Click -> {
                onTapListener()
            }
            ScrimGesture.Unknown -> {}
            ScrimGesture.DoubleClick -> onDoubleTapListener()
        }
    }

    override fun onGestureProgressListener(
        gesture: ScrimGesture,
        gestureLengthRelativeToScreen: Float
    ) {
        Log.d("GESTURES", "onGestureProgressListener: $gesture $gestureLengthRelativeToScreen")
        when (gesture) {
            ScrimGesture.LeftUpDown -> postUpdatedBrightness(
                handleBrightnessChange(
                    gestureLengthRelativeToScreen,
                    gestureStartBrightness!!
                )
            )

            ScrimGesture.RightUpDown -> postUpdatedVolume(
                handleVolumeChange(
                    gestureStartVolume!!,
                    gestureLengthRelativeToScreen
                )
            )

            ScrimGesture.HorizontalSwipe -> postUpdatedPlayBackPosition(
                handlePlayBackPositionChange(
                    gestureStartPlayBackPosition!!,
                    gestureLengthRelativeToScreen,
                    getTotalDuration()
                )
            )

            ScrimGesture.Click -> {}
            ScrimGesture.Unknown -> {}
            ScrimGesture.DoubleClick -> {}
        }

    }

    private fun handlePlayBackPositionChange(
        gestureStartPlayBackPosition: Long,
        gestureLengthRelativeToScreen: Float,
        videoDuration: Long
    ): Long {
        val seekAmount = abs(120000 * gestureLengthRelativeToScreen).roundToLong()
        return if (gestureLengthRelativeToScreen > 0) {
            kotlin.math.min(gestureStartPlayBackPosition + seekAmount, videoDuration)
        } else {
            kotlin.math.max(gestureStartPlayBackPosition - seekAmount, 0)
        }
    }

    private fun handleBrightnessChange(relativeChange: Float, startBrightness: Float): Float {
        return if (relativeChange > 0) java.lang.Float.min(
            relativeChange + startBrightness,
            1f
        ) else java.lang.Float.max(
            0f,
            startBrightness - abs(relativeChange)
        )
    }


    private fun handleVolumeChange(
        startVolume: Float,
        relativeChange: Float,
    ): Float {

        Log.d("GESTURES", "handleVolumeChange: $startVolume $relativeChange")

        // mapping change to its volume range
        val rangeSize = (deviceMaxVolume - deviceMinVolume)
        val portionSize = rangeSize * abs(relativeChange)
        return if (relativeChange > 0) {
            java.lang.Float.min(startVolume + (deviceMinVolume + portionSize), deviceMaxVolume)
        } else {
            java.lang.Float.max(startVolume - (deviceMinVolume + portionSize), deviceMinVolume)
        }

    }

}