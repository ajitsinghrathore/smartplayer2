@file:OptIn(ExperimentalMaterialApi::class)

package com.asr.smartaplayer.presentation.components

import android.content.res.Configuration
import android.util.Log
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material.rememberSwipeableState
import androidx.compose.material.swipeable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.SmartVideoPreviewUI
import kotlin.math.roundToInt


@Composable
fun MiniPlayerViewWithList(
    contentPadding: PaddingValues,
    swipeToDismissMiniPlayer: Boolean,
    miniPlayerVisible: Boolean,
    videos: List<BaseVideoItem>,
    loadMoreVideos: (Boolean) -> Unit,
    smartExoPlayer: SmartExoPlayer?,
    isLoading: Boolean,
    openImmersiveVideoPlayback: () -> Unit,
    onCloseVideoPreviewRequested: () -> Unit,
    onVideoPreviewRequested: (BaseVideoItem) -> Unit,
    onVideoPrivateRequest: (BaseVideoItem, Boolean) -> Unit,
    isOneBiometricAuthEnabled: Boolean,
    videoComments: List<VideoComment>,
    addComment: (BaseVideoItem, VideoComment) -> Unit
) {

    val configuration = LocalConfiguration.current
    val swipeableState = rememberSwipeableState(initialValue = miniPlayerVisible)

    when (configuration.orientation) {

        Configuration.ORIENTATION_LANDSCAPE -> {
            BoxWithConstraints(
                modifier = Modifier
                    .fillMaxSize()
                    .animateContentSize()
            )
            {

                val width = (this.maxWidth).times(0.4f)
                val widthPx = with(LocalDensity.current) { width.toPx() }
                val anchors = mapOf(0f to true, widthPx to false)

                if (swipeToDismissMiniPlayer)
                    LaunchedEffect(swipeableState.currentValue) {
                        if (!swipeableState.currentValue) {
                            onCloseVideoPreviewRequested()
                        }
                    }

                LaunchedEffect(miniPlayerVisible) {
                    if (!miniPlayerVisible) {
                        swipeableState.animateTo(false, tween(durationMillis = 500))
                    } else {
                        swipeableState.animateTo(true, tween(durationMillis = 500))
                    }
                }

                val heightForHeader by animateDpAsState(
                    targetValue = contentPadding.calculateTopPadding(),
                    animationSpec = tween(durationMillis = 200), label = ""
                )


                GenericListWithItems(
                    modifier = Modifier.fillMaxSize(),
                    paddingValues = PaddingValues(bottom = contentPadding.calculateBottomPadding()),
                    listState = rememberLazyListState(),
                    listOfItems = videos,
                    loadMoreItems = loadMoreVideos,
                    isLoading = isLoading,
                    headerSpace = heightForHeader,
                    loadingItemIndicator = { VideoLoadingItem() }) { _: LazyItemScope, _: Int, baseVideoItem: BaseVideoItem ->
                    when (baseVideoItem) {
                        is DriveVideoItem -> {
                            DriveVideoItem(
                                videoItem = baseVideoItem,
                                onVideoItemSelected = onVideoPreviewRequested
                            )
                        }

                        is LocalVideoItem -> {
                            LocalVideoItem(
                                videoItem = baseVideoItem,
                                isOneBiometricAuthEnabled = isOneBiometricAuthEnabled,
                                onVideoItemSelected = onVideoPreviewRequested,
                                onToggleIsVideoPrivate = {
                                    onVideoPrivateRequest(baseVideoItem, it)
                                }
                            )
                        }
                    }
                }



                if (miniPlayerVisible) {
                    var boxModifier = Modifier
                        .fillMaxHeight()
                        .align(Alignment.CenterEnd)
                        .fillMaxWidth(0.4f)
                    boxModifier = if (swipeToDismissMiniPlayer) {
                        boxModifier
                            .swipeable(
                                state = swipeableState,
                                anchors = anchors,
                                orientation = Orientation.Horizontal,
                                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                                resistance = null
                            )
                            .offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
                    } else {
                        boxModifier.offset {
                            IntOffset(
                                swipeableState.offset.value.roundToInt(),
                                0
                            )
                        }
                    }
                    Box(
                        modifier = boxModifier
                    ) {
                        SmartVideoPreviewUI(
                            modifier = Modifier
                                .fillMaxSize(),
                            smartExoPlayer = smartExoPlayer,
                            onFullScreenModeRequested = {
                                openImmersiveVideoPlayback()
                            },
                            onClosePreviewRequested = {
                                onCloseVideoPreviewRequested()
                            },
                            enableGestures = !swipeToDismissMiniPlayer,
                            videoComments = videoComments,
                            addComment = addComment
                        )
                    }

                }


            }
        }

        else -> {
            BoxWithConstraints(
                modifier = Modifier
                    .fillMaxSize()
                    .animateContentSize()
            )
            {

                val height = (this.maxHeight).times(0.3f)
                val heightPx = with(LocalDensity.current) { height.toPx() }
                val anchors = mapOf(0f to true, -heightPx to false)

                if (swipeToDismissMiniPlayer)
                    LaunchedEffect(swipeableState.currentValue) {
                        if (!swipeableState.currentValue) {
                            onCloseVideoPreviewRequested()
                        }
                    }

                LaunchedEffect(miniPlayerVisible) {
                    if (!miniPlayerVisible) {
                        swipeableState.animateTo(false, tween(durationMillis = 500))
                    } else {
                        swipeableState.animateTo(true, tween(durationMillis = 500))
                    }
                }


                val heightForHeader by animateDpAsState(
                    targetValue = if (miniPlayerVisible) (this.maxHeight).times(0.3f) + with(
                        LocalDensity.current
                    ) {
                        swipeableState.offset.value.roundToInt().toDp()
                    } else contentPadding.calculateTopPadding(),
                    animationSpec = tween(durationMillis = 200), label = ""
                )

                GenericListWithItems(
                    modifier = Modifier.fillMaxSize(),
                    paddingValues = PaddingValues(bottom = contentPadding.calculateBottomPadding()),
                    listState = rememberLazyListState(),
                    listOfItems = videos,
                    loadMoreItems = loadMoreVideos,
                    isLoading = isLoading,
                    headerSpace = heightForHeader,
                    loadingItemIndicator = { VideoLoadingItem() }) { _: LazyItemScope, _: Int, baseVideoItem: BaseVideoItem ->
                    when (baseVideoItem) {
                        is DriveVideoItem -> {
                            DriveVideoItem(
                                videoItem = baseVideoItem,
                                onVideoItemSelected = onVideoPreviewRequested
                            )
                        }

                        is LocalVideoItem -> {
                            LocalVideoItem(
                                videoItem = baseVideoItem,
                                isOneBiometricAuthEnabled = isOneBiometricAuthEnabled,
                                onVideoItemSelected = onVideoPreviewRequested,
                                onToggleIsVideoPrivate = {
                                    onVideoPrivateRequest(baseVideoItem, it)
                                }
                            )
                        }
                    }
                }


                if (miniPlayerVisible) {
                    var boxModifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.TopCenter)

                    boxModifier = if (swipeToDismissMiniPlayer) {
                        boxModifier
                            .fillMaxHeight(0.3f)
                            .swipeable(
                                state = swipeableState,
                                anchors = anchors,
                                orientation = Orientation.Vertical,
                                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                                resistance = null
                            )
                            .offset { IntOffset(0, swipeableState.offset.value.roundToInt()) }
                    } else {
                        boxModifier
                            .fillMaxHeight(0.3f)
                            .offset {
                                IntOffset(
                                    0,
                                    swipeableState.offset.value.roundToInt()
                                )
                            }
                    }
                    Box(
                        modifier = boxModifier
                    ) {
                        SmartVideoPreviewUI(
                            modifier = Modifier
                                .fillMaxSize(),
                            smartExoPlayer = smartExoPlayer,
                            onFullScreenModeRequested = {
                                openImmersiveVideoPlayback()
                            },
                            onClosePreviewRequested = {
                                onCloseVideoPreviewRequested()
                            },
                            enableGestures = !swipeToDismissMiniPlayer,
                            videoComments = videoComments,
                            addComment = addComment
                        )
                    }

                }

            }
        }

    }


}