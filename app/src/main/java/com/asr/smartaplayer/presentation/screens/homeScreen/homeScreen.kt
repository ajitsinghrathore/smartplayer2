@file:OptIn(
    ExperimentalPermissionsApi::class
)

package com.asr.smartaplayer.presentation.screens.homeScreen

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.VideoPlayerState
import com.asr.smartaplayer.presentation.components.MiniPlayerViewWithList
import com.asr.smartaplayer.presentation.components.authenticate
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.rememberPermissionState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun HomeScreen(
    contentPadding: PaddingValues,
    viewModel: HomeScreenViewModel,
    searchText: String,
    hasAccessToSeePrivateVideos: Boolean,
    isOneBiometricAuthEnabled: Boolean,
    videoComments: List<VideoComment>,
    addComment: (BaseVideoItem, VideoComment) -> Unit,
    openImmersiveVideoPlayback: () -> Unit,
    smartExoPlayer: SmartExoPlayer?,
    onVideoPreviewRequested: (BaseVideoItem) -> Unit,
    onCloseVideoPreviewRequested: () -> Unit,
    miniPlayerVisible: Boolean
) {


    val context = LocalContext.current
    val permissionState = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        rememberPermissionState(
            permission = Manifest.permission.READ_MEDIA_VIDEO
        )
    } else {
        rememberPermissionState(permission = Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    if (checkPermission(permissionState)) {
        LaunchedEffect(key1 = hasAccessToSeePrivateVideos , key2 = searchText) {
            Log.d("DRIVE*", "HomeScreen: private pulled")
            viewModel.loadMoreVideos(true, hasAccessToSeePrivateVideos, searchText)
        }

        var currentPrivateVideoRequested by remember {
            mutableStateOf<LocalVideoItem?>(null)
        }
        MiniPlayerViewWithList(
            contentPadding = contentPadding,
            swipeToDismissMiniPlayer = viewModel.swipeToDismissMiniPlayer.value,
            miniPlayerVisible = miniPlayerVisible,
            videos = viewModel.videosState.value,
            loadMoreVideos = {
                viewModel.loadMoreVideos(
                    freshLoad = it, hasAccessToSeePrivateVideos = hasAccessToSeePrivateVideos, searchText= searchText
                )
            },
            smartExoPlayer = smartExoPlayer,
            isLoading = viewModel.loadingState.value,
            openImmersiveVideoPlayback = openImmersiveVideoPlayback,
            onCloseVideoPreviewRequested = onCloseVideoPreviewRequested,
            onVideoPreviewRequested = {
                val videoItem: LocalVideoItem = it as LocalVideoItem
                if (videoItem.isPrivate && !isOneBiometricAuthEnabled) {
                    authenticate(
                        context = context,
                        onAuthenticationFailed = {},
                        onAuthenticationSucceeded = {
                            onVideoPreviewRequested(it)
                        }
                    )
                } else {
                    onVideoPreviewRequested(it)
                }
            },
            onVideoPrivateRequest = { item, _ ->
                currentPrivateVideoRequested = item as LocalVideoItem
            },
            isOneBiometricAuthEnabled = isOneBiometricAuthEnabled,
            videoComments = videoComments,
            addComment = addComment
        )

        val scope = rememberCoroutineScope()

        currentPrivateVideoRequested?.let {

            var progress by remember {
                mutableFloatStateOf(0f)
            }

            PrivateVideoItemDialog(
                onProceed = {
                    scope.launch {
                        viewModel.toggleVideoIsPrivate(
                            it,
                            !it.isPrivate,
                            hasAccessToSeePrivateVideos = hasAccessToSeePrivateVideos
                        ).collect {pro ->
                            progress = pro
                        }
                        Log.d("DRIVE*", "HomeScreen: Completing flow collection in dialog box")
                        currentPrivateVideoRequested = null
                    }
                },
                onDiscarded = {
                    currentPrivateVideoRequested = null
                },
                progress = progress,
                makePrivate = !it.isPrivate
            )
        }

    } else {
        AskPermission {
            permissionState.launchPermissionRequest()
        }
    }


}


fun checkPermission(permissionState: PermissionState): Boolean {

    return permissionState.hasPermission

}