package com.asr.smartaplayer.presentation.screens.settings

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.OptionalPreservableVideoProperties
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.models.SubtitlesDisplaySettings
import com.asr.smartaplayer.domain.models.UserLeaveHintAction
import com.asr.smartaplayer.domain.models.VideosSortPreference
import com.asr.smartaplayer.domain.useCases.preferences.PreferencesUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val allUseCases: PreferencesUseCases
) : ViewModel() {


    val videosSortingPreference: MutableState<VideosSortPreference?> = mutableStateOf(null)
    val isOneBiometricAuthEnabled = mutableStateOf(false)
    val currentLySelectedPlaybackPropertiesForSync =
        mutableStateOf<Set<OptionalPreservableVideoProperties>>(emptySet())
    val miniPlayerSwipeToDismiss = mutableStateOf(false)
    val currentDrowsinessSensitivity = mutableFloatStateOf(5F)
    val driveVideoCacheSize = mutableLongStateOf(100*1024*1024)
    val userLeaveHintAction:  MutableState<UserLeaveHintAction?> = mutableStateOf(null)
    val subtitlesDisplaySettings: MutableState<SubtitlesDisplaySettings?> = mutableStateOf(null)


    private var videosSortingPreferenceJob: Job? = null

    init {
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.VideosSortingPreference)
                .collectLatest {
                    videosSortingPreference.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.SwipeToDismissVideoPreview)
                .collectLatest {
                    miniPlayerSwipeToDismiss.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.VideosOptionalPreservableProperties)
                .collectLatest {
                    currentLySelectedPlaybackPropertiesForSync.value = it.toSet()
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.DrowsinessSensitivity)
                .collectLatest {
                    currentDrowsinessSensitivity.floatValue = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.IsOneBiometricAuthEnabled)
                .collectLatest {
                    isOneBiometricAuthEnabled.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.DriveVideoCacheSize)
                .collectLatest {
                    driveVideoCacheSize.longValue = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.UserLeaveHintActionKey)
                .collectLatest {
                    userLeaveHintAction.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.listenForUpdatedSettingsForKey(SettingsKey.GlobalSubtitlesDisplaySettings)
                .collectLatest {
                    subtitlesDisplaySettings.value = it
                }
        }
    }

    fun updateSubtitlesDisplaySettings(value: SubtitlesDisplaySettings){
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.GlobalSubtitlesDisplaySettings, value)
        }
    }


    fun updateDrowsinessSensitivity(value: Float) {
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.DrowsinessSensitivity, value)
        }
    }

    fun updateUserLeaveHintActionKey(value: UserLeaveHintAction){
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.UserLeaveHintActionKey, value)
        }
    }


    fun updateDriveVideoCacheSize(value: Long) {
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.DriveVideoCacheSize, value)
        }
    }

    fun updateCurrentLySelectedPlaybackPropertiesForSync(newProperties: Set<OptionalPreservableVideoProperties>) {
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(
                SettingsKey.VideosOptionalPreservableProperties,
                newProperties.toList()
            )
        }
    }


    fun updateSwipeToDismissPlayer(value: Boolean) {
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.SwipeToDismissVideoPreview, value)
        }
    }

    fun updateIsOneBiometricAuthEnabled(value: Boolean){
        viewModelScope.launch {
            allUseCases.updateAppSettingsForKey(SettingsKey.IsOneBiometricAuthEnabled, value)
        }
    }


    fun updateOrderVideosIn(orderVideosIn: OrderVideosIn) {
        videosSortingPreference.value =
            videosSortingPreference.value?.copy(orderVideosIn = orderVideosIn)
        videosSortingPreferenceJob?.let {
            if (it.isActive) {
                it.cancel()
            }
        }
        videosSortingPreferenceJob = viewModelScope.launch {
            videosSortingPreference.value?.let {
                allUseCases.updateAppSettingsForKey(SettingsKey.VideosSortingPreference, it)
            }

        }
    }

    fun updateOrderVideosBy(orderVideosBy: OrderVideosBy) {
        videosSortingPreference.value =
            videosSortingPreference.value?.copy(orderVideosBy = orderVideosBy)
        videosSortingPreferenceJob?.let {
            if (it.isActive) {
                it.cancel()
            }
        }
        videosSortingPreferenceJob = viewModelScope.launch {
            videosSortingPreference.value?.let {
                allUseCases.updateAppSettingsForKey(SettingsKey.VideosSortingPreference, it)
            }
        }
    }


}