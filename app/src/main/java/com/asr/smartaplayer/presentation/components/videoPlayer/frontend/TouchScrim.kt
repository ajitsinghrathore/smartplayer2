@file:OptIn(ExperimentalComposeUiApi::class)

package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.IntSize
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.CircularProgressBar
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.GestureDetector
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.SeekChangeIndicator
import com.asr.smartaplayer.presentation.components.videoPlayer.getCurrentVolume


@Composable
fun TouchScrim(
    modifier: Modifier,
    currentBrightness: Float,
    currentVolume: Float,
    currentPlayBackPosition: Long,
    totalDuration: Long,
    deviceMaxVolume: Float,
    deviceMinVolume: Float,
    onBrightnessChange: (Float) -> Unit,
    onVolumeUpdateChange: (Float) -> Unit,
    onPlayBackPositionChange: (Long) -> Unit,
    onBrightnessChangeStarted: (Boolean) -> Unit,
    onVolumeChangeStarted: (Boolean) -> Unit,
    onPlayBackPositionChangeStarted: (Boolean) -> Unit,
    onTapListener: () -> Unit,
    onDoubleTapListener: () -> Unit,
    showBrightnessChangeIndicator: Boolean,
    showVolumeChangeIndicator: Boolean,
    showPlayBackSeekChangeIndicator: Boolean,
) {
    var startingPositionForSeeking by remember {
        mutableLongStateOf(0L)
    }

    val context = LocalContext.current

    val currentBrightnessUpdated by rememberUpdatedState(newValue =  currentBrightness)
    val currentPlayBackPositionUpdated by rememberUpdatedState(newValue =  currentPlayBackPosition)

    val gestureDetector = remember {
        object : GestureDetector() {
            override fun getCurrentBrightness(): Float = currentBrightnessUpdated

            override fun getCurrentVolume(): Float {
                return getCurrentVolume(context).toFloat()
            }

            override fun getCurrentPlayBackPosition(): Long = currentPlayBackPositionUpdated

            override fun getTotalDuration(): Long = totalDuration

            override fun brightnessChangeDetected(started: Boolean) =
                onBrightnessChangeStarted(started)

            override fun volumeChangeDetected(started: Boolean) = onVolumeChangeStarted(started)

            override fun playBackPositionChangeDetected(started: Boolean) {
                onPlayBackPositionChangeStarted(started)
                startingPositionForSeeking = currentPlayBackPosition
            }
            override val deviceMaxVolume: Float
                get() = deviceMaxVolume
            override val deviceMinVolume: Float
                get() = deviceMinVolume

            override fun postUpdatedBrightness(value: Float) = onBrightnessChange(value)

            override fun postUpdatedVolume(value: Float) = onVolumeUpdateChange(value)

            override fun postUpdatedPlayBackPosition(value: Long) = onPlayBackPositionChange(value)

            override fun onTapListener()  = onTapListener()
            override fun onDoubleTapListener()  = onDoubleTapListener()

        }
    }


    var scrimSize = remember {
        IntSize(0, 0)
    }


    Box(
        modifier = modifier
            .fillMaxSize()
            .onGloballyPositioned { scrimSize = it.size }
            .pointerInteropFilter {
                gestureDetector.handleTouchEvent(it, scrimSize)
                true
            }
    )
    {
        if (showVolumeChangeIndicator)
            CircularProgressBar(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(0.5f)
                    .align(Alignment.CenterStart),
                currentValue = currentVolume,
                minValue = deviceMinVolume,
                maxValue = deviceMaxVolume,
                iconResource = R.drawable.baseline_volume_up_24
            )
        if (showBrightnessChangeIndicator)
            CircularProgressBar(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(0.5f)
                    .align(Alignment.CenterEnd),
                currentValue = currentBrightness,
                minValue = 0f,
                maxValue = 1f,
                iconResource = R.drawable.baseline_brightness_4_24
            )


        if (showPlayBackSeekChangeIndicator)
            SeekChangeIndicator(
                modifier = Modifier.fillMaxSize(),
                startPosition = startingPositionForSeeking,
                incrementDiff = currentPlayBackPosition - startingPositionForSeeking
            )

    }

}



