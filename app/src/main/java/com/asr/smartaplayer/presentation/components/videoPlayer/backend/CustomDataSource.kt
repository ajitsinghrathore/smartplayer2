package com.asr.smartaplayer.presentation.components.videoPlayer.backend

import android.util.Log
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DataSpec
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.datasource.HttpDataSource
@androidx.annotation.OptIn(UnstableApi::class)
class AuthHttpDataSourceFactory : HttpDataSource.Factory {
    private val httpDataSourceFactory = DefaultHttpDataSource.Factory().setAllowCrossProtocolRedirects(true)

    private var defaultRequestProperties: Map<String, String> = mapOf()

    private var authToken : String = ""

    fun setAuthToken(authToken: String) {
        this.authToken = authToken
    }

    override fun createDataSource(): HttpDataSource {
        val dataSource = httpDataSourceFactory.createDataSource()
        Log.d("DRIVE", "createDataSource: updated auth token with $authToken")
        dataSource.setRequestProperty("Authorization", "Bearer $authToken")

        for ((key, value) in defaultRequestProperties) {
            dataSource.setRequestProperty(key, value)
        }

        return dataSource
    }

    override fun setDefaultRequestProperties(defaultRequestProperties: Map<String, String>): HttpDataSource.Factory {
        this.defaultRequestProperties = defaultRequestProperties
        return this
    }
}