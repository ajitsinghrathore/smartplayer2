package com.asr.smartaplayer.presentation.components.videoPlayer.backend

import androidx.compose.runtime.MutableState
import com.asr.smartaplayer.domain.models.AudioEqualizerSettings
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.models.VisualEffectsSettings

data class VideoPlayerState(
    val currentPlayingMedia: MutableState<CurrentPlayingMedia?>,
    val isPlaying: MutableState<Boolean>,
    val isBuffering: MutableState<Boolean>,
    val playWhenReady: MutableState<Boolean>,
    val bufferingPosition: MutableState<Long>
){
    fun reset(){
        currentPlayingMedia.value = null
        isPlaying.value = false
        isBuffering.value = false
        playWhenReady.value = true
        bufferingPosition.value = 0
    }
}



data class CurrentPlayingMedia(
    var videoItem: MutableState<BaseVideoItem>,
    var lastPlayedPosition: MutableState<Long>,
    var brightnessIntensity: MutableState<Float>,
    var volumeLevel: MutableState<Float>,
    var playBackSpeed: MutableState<Float>,
    var playBackPitch: MutableState<Float>,
    var totalDuration: MutableState<Long>,
    var isSubtitleEnabled: MutableState<Boolean>,
    var selectedSubtitle: MutableState<Subtitle?>,
    var audioEqualizerSettings: MutableState<AudioEqualizerSettings>,
    var visualEffect: MutableState<VisualEffectsSettings>,
)
