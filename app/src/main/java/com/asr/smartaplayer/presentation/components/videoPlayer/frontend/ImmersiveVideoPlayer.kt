package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.util.UnstableApi
import androidx.media3.ui.PlayerView
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState
import com.asr.smartaplayer.presentation.components.videoPlayer.ExoPlayerControlsUiState
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.components.videoPlayer.createVideoCommentItemForVideo
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SideSheetForControls
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.lowerSection.LowerSection
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.upperSection.UpperSection
import com.asr.smartaplayer.presentation.components.videoPlayer.getDeviceMaxVolume
import com.asr.smartaplayer.presentation.components.videoPlayer.getDeviceMinVolume
import com.asr.smartaplayer.presentation.showToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@androidx.annotation.OptIn(UnstableApi::class)
@Composable
fun ImmersiveVideoPlayer(
    modifier: Modifier,
    smartExoPlayer: SmartExoPlayer?,
    onDrowsyStateClick: (Boolean) -> Unit,
    onExitImmersiveMode: () -> Unit,
    isDrowsyEnabled: Boolean,
    videoComments: List<VideoComment>,
    onPIPRequest: () -> Unit,
    loadMoreSubtitles: suspend (GenericOffsetBasedPaginationState, String) -> List<Subtitle>?,
    onCommentAddRequest: (BaseVideoItem, VideoComment) -> Unit,
    onCommentDeleteRequest: (VideoComment, BaseVideoItem) -> Unit
) {


    // TODO  optimise scrim background check for current position to only query for time when needed
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()


    var controlsState by remember {
        mutableStateOf(ExoPlayerControlsUiState.HideAllControls)
    }

    var isControlsLocked by rememberSaveable {
        mutableStateOf(false)
    }

    var hideUnLockButton by remember {
        mutableStateOf(false)
    }


    Box(
        modifier = modifier.background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.BottomStart
    ) {

        val currentPlayingMedia = smartExoPlayer?.videoPlayerState?.currentPlayingMedia?.value

        if (smartExoPlayer != null) {
            AndroidView(modifier = modifier, factory = {
                PlayerView(context).apply {
                    smartExoPlayer.attachPlayerView(this, coroutineScope)
                    this.useController = false
                    this.keepScreenOn = true
                }
            }, update = {
                it.useController = false
                it.keepScreenOn = true
            })


            if (currentPlayingMedia != null) {


                // Touch SCRIM
                if (!isControlsLocked) {
                    TouchScrim(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(20.dp),
                        currentBrightness = currentPlayingMedia.brightnessIntensity.value,
                        currentVolume = currentPlayingMedia.volumeLevel.value,
                        currentPlayBackPosition = currentPlayingMedia.lastPlayedPosition.value,
                        totalDuration = currentPlayingMedia.totalDuration.value,
                        deviceMaxVolume = getDeviceMaxVolume(context).toFloat(),
                        deviceMinVolume = getDeviceMinVolume(context = context).toFloat(),
                        onBrightnessChange = {
                            smartExoPlayer.setBrightness(brightnessLevel = it, context = context)
                        },
                        onVolumeUpdateChange = {
                            smartExoPlayer.setVolume(volumeLevel = it, context = context)
                        },
                        onPlayBackPositionChange = {
                            smartExoPlayer.seekTo(it)
                        },
                        onBrightnessChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowBrightnessChange else ExoPlayerControlsUiState.HideAllControls
                        },
                        onVolumeChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowVolumeChange else ExoPlayerControlsUiState.HideAllControls
                        },
                        onPlayBackPositionChangeStarted = {
                            controlsState =
                                if (it) ExoPlayerControlsUiState.ShowSeekChange else ExoPlayerControlsUiState.HideAllControls
                            if (it) {
                                smartExoPlayer.softPause()
                            } else {
                                smartExoPlayer.softPlay()
                            }
                        },
                        onTapListener = {
                            controlsState = when (controlsState) {
                                ExoPlayerControlsUiState.HideAllControls -> {
                                    ExoPlayerControlsUiState.ShowAllControls
                                }

                                ExoPlayerControlsUiState.ShowVideoComments -> {
                                    smartExoPlayer.softPlay()
                                    ExoPlayerControlsUiState.HideAllControls
                                }

                                ExoPlayerControlsUiState.ShowSubtitleSettings -> {
                                    smartExoPlayer.softPlay()
                                    ExoPlayerControlsUiState.HideAllControls
                                }

                                else -> {
                                    ExoPlayerControlsUiState.HideAllControls
                                }
                            }
                        },
                        onDoubleTapListener = {
                            if (smartExoPlayer.videoPlayerState.isPlaying.value)
                                smartExoPlayer.hardPause()
                            else
                                smartExoPlayer.hardPlay()
                        },
                        showBrightnessChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowBrightnessChange,
                        showVolumeChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowVolumeChange,
                        showPlayBackSeekChangeIndicator = controlsState == ExoPlayerControlsUiState.ShowSeekChange
                    )
                }


                // Upper section
                AnimatedVisibility(
                    visible = controlsState == ExoPlayerControlsUiState.ShowAllControls && !isControlsLocked,
                    enter = slideInVertically(initialOffsetY = { -50 }) + fadeIn(),
                    exit = slideOutVertically(targetOffsetY = { -50 }) + fadeOut(),
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.5f)
                        .align(Alignment.TopCenter)
                ) {

                    UpperSection(
                        modifier = Modifier.fillMaxSize(),
                        onPIPRequest = { onPIPRequest() },
                        onDrowsyStateDetectionClicked = onDrowsyStateClick,
                        onExitImmersiveModeRequested = onExitImmersiveMode,
                        onCommentShowClicked = {
                            controlsState = ExoPlayerControlsUiState.ShowVideoComments
                            smartExoPlayer.softPause()
                        },
                        isCommentsEnabled = controlsState == ExoPlayerControlsUiState.ShowVideoComments,
                        isDrowsyEnabled = isDrowsyEnabled,
                        showSubtitleSettings = {
                            controlsState = ExoPlayerControlsUiState.ShowSubtitleSettings
                            smartExoPlayer.softPause()
                        },
                        showVideoEffectsSettings = {
                            controlsState = ExoPlayerControlsUiState.ShowVisualEffectsSettings
                        }
                    )
                }


                // Lower section
                AnimatedVisibility(
                    visible = controlsState == ExoPlayerControlsUiState.ShowAllControls && !isControlsLocked,
                    enter = slideInVertically(initialOffsetY = { 50 }) + fadeIn(),
                    exit = slideOutVertically(targetOffsetY = { 50 }) + fadeOut(),
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.5f)
                        .align(Alignment.BottomCenter)
                ) {
                    LowerSection(
                        modifier = Modifier.fillMaxSize(),
                        seekStarted = {
                            smartExoPlayer.softPause()
                        },
                        seekTo = {
                            currentPlayingMedia.lastPlayedPosition.value = it
                            smartExoPlayer.seekTo(it)
                        },
                        seekEnded = {
                            smartExoPlayer.softPlay()
                        },
                        lockControlsRequested = { isControlsLocked = true },
                        currentValue = currentPlayingMedia.lastPlayedPosition.value,
                        totalDuration = currentPlayingMedia.totalDuration.value,
                        videoComments = videoComments,
                        addComment = { comment, position ->
                            coroutineScope.launch(Dispatchers.IO) {
                                createVideoCommentItemForVideo(
                                    context = context,
                                    videoItem = currentPlayingMedia.videoItem.value,
                                    comment = comment,
                                    position = position,
                                    smartExoPlayer = smartExoPlayer,
                                ) {
                                    onCommentAddRequest(
                                        currentPlayingMedia.videoItem.value,
                                        it
                                    )
                                }
                            }

                        },
                        bufferedPosition = smartExoPlayer.videoPlayerState.bufferingPosition.value
                    )

                }

                // left section
                AnimatedVisibility(
                    visible = controlsState == ExoPlayerControlsUiState.ShowAllControls && !isControlsLocked,
                    enter = slideInHorizontally(initialOffsetX = { -50 }) + fadeIn(),
                    exit = slideOutHorizontally(targetOffsetX = { -50 }) + fadeOut(),
                    modifier = Modifier
                        .padding(start = 5.dp)
                        .wrapContentWidth()
                        .wrapContentHeight()
                        .align(Alignment.CenterStart)
                ) {

                    LeftSection(
                        modifier = Modifier
                            .wrapContentHeight()
                            .wrapContentWidth(),
                        onEqualizerSettingsClicked = {
                            controlsState = ExoPlayerControlsUiState.ShowEqualizerSettings
                        },
                        onPlayBackSpeedAndPitchSettingsClicked = {
                            controlsState = ExoPlayerControlsUiState.ShowPlayBackSpeedChangeControl
                        }
                    )

                }


                // Video comments overlay

                SideSheetForControls(
                    isVisible = controlsState == ExoPlayerControlsUiState.ShowVideoComments && !isControlsLocked,
                    onSheetCloseRequested = {
                        controlsState = ExoPlayerControlsUiState.HideAllControls
                        smartExoPlayer.softPlay()
                    }
                ) { modifier ->
                    VideoComments(
                        modifier = modifier,
                        onAddNewVideoComment = {
                            coroutineScope.launch(Dispatchers.IO) {
                                createVideoCommentItemForVideo(
                                    context = context,
                                    videoItem = currentPlayingMedia.videoItem.value,
                                    comment = it,
                                    position = currentPlayingMedia.lastPlayedPosition.value,
                                    smartExoPlayer = smartExoPlayer
                                ) {
                                    onCommentAddRequest(
                                        currentPlayingMedia.videoItem.value,
                                        it
                                    )
                                    showToast(context, "Comment added successfully")
                                    smartExoPlayer.softPlay()
                                    controlsState = ExoPlayerControlsUiState.HideAllControls
                                }
                            }
                        },
                        videoComments = videoComments,
                        videoItem = currentPlayingMedia.videoItem.value,
                        onClick = {
                            smartExoPlayer.seekTo(it.playBackPosition)
                            smartExoPlayer.softPlay()
                            controlsState = ExoPlayerControlsUiState.HideAllControls

                        },
                        onDeleteRequest = {
                            onCommentDeleteRequest(
                                it,
                                currentPlayingMedia.videoItem.value
                            )
                        }
                    )
                }


                // EqualizerSettings

                SideSheetForControls(
                    isVisible = controlsState == ExoPlayerControlsUiState.ShowEqualizerSettings,
                    onSheetCloseRequested = {
                        controlsState = ExoPlayerControlsUiState.HideAllControls
                    }
                ) { modifier ->
                    AudioEqualizerComponent(
                        modifier = modifier,
                        equalizer = smartExoPlayer.audioEqualizer,
                        currentReverbMode = currentPlayingMedia.audioEqualizerSettings.value.audioReverbMode,
                        onReverbSelected = {
                            smartExoPlayer.setReverbMode(it)
                        },
                        currentBassBoost = currentPlayingMedia.audioEqualizerSettings.value.audioBassBoost,
                        onBassBoostUpdate = {
                            smartExoPlayer.setBassBoost(it)
                        },
                        currentPresetMode = currentPlayingMedia.audioEqualizerSettings.value.audioPresetMode,
                        onPresetChange = {
                            smartExoPlayer.setPresetAudioMode(it)
                        },
                        currentBandValues = currentPlayingMedia.audioEqualizerSettings.value.customAudioBandValues,
                        onBandValuesUpdate = { index, value ->
                            smartExoPlayer.setBandValues(mapOf(index to value))
                        },
                        currentLoudnessBoost = currentPlayingMedia.audioEqualizerSettings.value.audioLoudnessBoost,
                        onLoudnessBoostChange = {
                            smartExoPlayer.setLoudnessBoost(it)
                        }
                    )
                }

                // playback speed and pitch settings
                SideSheetForControls(
                    isVisible = controlsState == ExoPlayerControlsUiState.ShowPlayBackSpeedChangeControl,
                    onSheetCloseRequested = {
                        controlsState = ExoPlayerControlsUiState.HideAllControls
                    }
                ) { modifier ->
                    PlaybackSpeedAndPitchControls(
                        modifier = modifier,
                        currentPlayBackSpeed = currentPlayingMedia.playBackSpeed.value,
                        onPlaybackSpeedChange = { smartExoPlayer.setPlayBackSpeed(it) },
                        currentPitchSpeed = currentPlayingMedia.playBackPitch.value,
                        onPlaybackPitchChange = { smartExoPlayer.setPlayBackPitch(it) }
                    )
                }

                val scope = rememberCoroutineScope()

                // subtitle settings
                SideSheetForControls(
                    isVisible = controlsState == ExoPlayerControlsUiState.ShowSubtitleSettings,
                    onSheetCloseRequested = {
                        controlsState = ExoPlayerControlsUiState.HideAllControls
                        smartExoPlayer.softPlay()
                    }
                ) { modifier ->
                    val subtitlePaginationState = remember {
                        mutableStateOf(ObservablePaginationState(GenericOffsetBasedPaginationState()))
                    }
                    val subtitles = remember {
                        mutableStateOf(emptyList<Subtitle>())
                    }
                    LaunchedEffect(Unit) {
                        subtitles.value =
                            loadMoreSubtitles(subtitlePaginationState.value.paginationState, "") ?: emptyList()
                    }
                    val loadingJob = remember {
                        mutableStateOf<Job?>(null)
                    }
                    SubtitlesList(
                        modifier = modifier,
                        subtitles = subtitles.value,
                        loadMoreSubtitle = { freshLoad, searchText ->
                            if (subtitlePaginationState.value.paginationState.isLoading || subtitlePaginationState.value.paginationState.paginationCompleted) {
                                if (freshLoad) {
                                    loadingJob.value?.cancel()
                                } else {
                                    return@SubtitlesList
                                }
                            }
                            if (freshLoad) {
                                subtitlePaginationState.value = subtitlePaginationState.value.copy(paginationState = GenericOffsetBasedPaginationState())
                            }
                            loadingJob.value = scope.launch(Dispatchers.IO) {
                                val updatedSubtitles =
                                    loadMoreSubtitles(
                                        subtitlePaginationState.value.paginationState,
                                        searchText
                                    )
                                        ?: return@launch
                                withContext(Dispatchers.Main) {
                                    subtitlePaginationState.value = subtitlePaginationState.value.copyWithNewRefreshIndicator()
                                    if (freshLoad) {
                                        subtitles.value = updatedSubtitles
                                    } else {
                                        subtitles.value = subtitles.value + updatedSubtitles
                                    }
                                }
                            }
                        },
                        isSubtitlesLoading = subtitlePaginationState.value.paginationState.isLoading,
                        selectedSubtitle = currentPlayingMedia.selectedSubtitle.value,
                        isSubtitlesEnabled = currentPlayingMedia.isSubtitleEnabled.value,
                        onSubtitleToggle = {
                            smartExoPlayer.enableSubtitle(it)
                        },
                        onSubtitleSelect = {
                            smartExoPlayer.selectSubtitle(it)
                        }
                    )
                }

                // visual effects settings
                SideSheetForControls(
                    isVisible = controlsState == ExoPlayerControlsUiState.ShowVisualEffectsSettings,
                    onSheetCloseRequested = {
                        controlsState = ExoPlayerControlsUiState.HideAllControls
                    }
                ) { modifier ->
                    VideoEffectsSettings(
                        modifier = modifier,
                        visualEffectsSettings = currentPlayingMedia.visualEffect.value,
                        onVisualEffectsConfigUpdate = {
                            currentPlayingMedia.visualEffect.value = it
                            smartExoPlayer.setVisualEffectsAndPrepare()
                        }
                    )
                }


                // Buffering indicator at center
                AnimatedVisibility(
                    visible = smartExoPlayer.videoPlayerState.isBuffering.value,
                    enter = fadeIn(),
                    exit = fadeOut(),
                    modifier = Modifier
                        .fillMaxSize(0.5f)
                        .align(Alignment.Center)
                ) {
                    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loading))
                    LottieAnimation(
                        composition = composition,
                        iterations = LottieConstants.IterateForever,
                    )
                }

                // Play button at center
                AnimatedVisibility(
                    visible = controlsState == ExoPlayerControlsUiState.ShowAllControls && !isControlsLocked,
                    enter = fadeIn(),
                    exit = fadeOut(),
                    modifier = Modifier
                        .align(Alignment.Center)
                ) {
                    PlayButton(
                        modifier = Modifier.size(50.dp),
                        isPlaying = smartExoPlayer.videoPlayerState.isPlaying.value,
                        togglePlaying = {
                            if (it) {
                                smartExoPlayer.hardPlay()
                            } else {
                                smartExoPlayer.hardPause()
                            }
                        },
                    )
                }

                // Placeholder for intercepting touch in case of locked controls
                if (isControlsLocked) {
                    val interactionSource = remember { MutableInteractionSource() }
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .clickable(interactionSource = interactionSource, indication = null) {
                                hideUnLockButton = !hideUnLockButton
                            }, contentAlignment = Alignment.BottomStart
                    ) {

                    }
                }

                // unlock button at left bottom
                AnimatedVisibility(
                    visible = isControlsLocked && !hideUnLockButton,
                    enter = slideInVertically(initialOffsetY = { 50 }) + fadeIn(),
                    exit = slideOutVertically(targetOffsetY = { 50 }) + fadeOut(),
                    modifier = Modifier
                        .height(50.dp)
                        .aspectRatio(1f)
                        .aspectRatio(1f)
                ) {
                    GeneralIconButton(
                        modifier = Modifier
                            .height(50.dp)
                            .aspectRatio(1f),
                        onClick = { isControlsLocked = false },
                        iconColor = MaterialTheme.colorScheme.onBackground,
                        iconId = R.drawable.baseline_lock_open_24,
                        contentDescription = " Unlock the controls ",
                        paddingValues = PaddingValues(14.dp)
                    )
                }
            }
        }
    }
}






















data class ObservablePaginationState(
    val paginationState: GenericOffsetBasedPaginationState,
    val uniqueRefreshIndicator : Long = System.currentTimeMillis()
){
    fun copyWithNewRefreshIndicator(): ObservablePaginationState{
        return this.copy(uniqueRefreshIndicator = System.currentTimeMillis())
    }
}