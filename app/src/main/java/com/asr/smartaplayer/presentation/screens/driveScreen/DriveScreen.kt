@file:OptIn(
    ExperimentalFoundationApi::class,
    ExperimentalFoundationApi::class, ExperimentalFoundationApi::class
)

package com.asr.smartaplayer.presentation.screens.driveScreen

import android.accounts.AccountManager
import android.app.Activity
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerDefaults
import androidx.compose.foundation.pager.PagerSnapDistance
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.Tab
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.VideoPlayerState
import com.asr.smartaplayer.presentation.components.MiniPlayerViewWithList
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.google.android.gms.auth.GoogleAuthUtil
import kotlinx.coroutines.launch


@Composable
fun DriveScreen(
    viewModel: DriveScreenViewModel,
    paddingValues: PaddingValues,
    miniPlayerVisible: Boolean,
    smartExoPlayer: SmartExoPlayer?,
    videoComments: List<VideoComment>,
    addComment: (BaseVideoItem, VideoComment) -> Unit,
    onCloseVideoPreviewRequested: () -> Unit,
    onVideoPreviewRequested: (BaseVideoItem) -> Unit,
    openImmersiveVideoPlayBack: () -> Unit
) {
    var showAccountSelector by remember {
        mutableStateOf(false)
    }

    if (viewModel.userEmails.value.isEmpty()) {
        AskDrivePermission(
            paddingValues = paddingValues,
            askPermission = { showAccountSelector = true }
        )
    }

    var lastChecked by remember {
        mutableLongStateOf(System.currentTimeMillis())
    }

    if (showAccountSelector) {
        ShowGoogleAccountSelection(
            lastChecked = lastChecked,
            onAccountNameSelected = {
                showAccountSelector = false
                viewModel.addUserEmail(email = it)
            },
            updateLastChecked = { lastChecked = System.currentTimeMillis() },
            onDiscarded = { showAccountSelector = false }
        )
    }


    if (viewModel.userEmails.value.isNotEmpty()) {


        val pagerState = rememberPagerState(initialPage = 0) {
            viewModel.userEmails.value.size
        }
        val scope = rememberCoroutineScope()

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.TopCenter
        ) {

            LaunchedEffect(key1 = pagerState.currentPage) {
                Log.d("DRIVE*", "DriveScreen: loading videos for ${pagerState.currentPage}")
                viewModel.loadVideoForEmailIfNotLoaded(userEmail = viewModel.userEmails.value[pagerState.currentPage])

            }

            HorizontalPager(
                state = pagerState,
            ) {


                MiniPlayerViewWithList(
                    contentPadding = PaddingValues(
                        top = paddingValues.calculateTopPadding() + 50.dp,
                        bottom = paddingValues.calculateBottomPadding()
                    ),
                    swipeToDismissMiniPlayer = viewModel.swipeToDismissMiniPlayer.value,
                    miniPlayerVisible = miniPlayerVisible,
                    videos = viewModel.getVideosForUserEmail(userEmail = viewModel.userEmails.value[it]).value,
                    loadMoreVideos = { freshLoad ->
                        viewModel.loadMoreVideos(
                            freshLoad = freshLoad,
                            userEmail = viewModel.userEmails.value[it]
                        )
                    },
                    smartExoPlayer = smartExoPlayer,
                    isLoading = viewModel.getLoadingStatusStateForEmail(userEmail = viewModel.userEmails.value[it]).value,
                    openImmersiveVideoPlayback = openImmersiveVideoPlayBack,
                    onCloseVideoPreviewRequested = onCloseVideoPreviewRequested,
                    onVideoPreviewRequested = onVideoPreviewRequested,
                    onVideoPrivateRequest = { _, _ -> },
                    isOneBiometricAuthEnabled = false,
                    videoComments = videoComments,
                    addComment = addComment,
                )
            }

            AnimatedVisibility(
                visible = !miniPlayerVisible,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = paddingValues.calculateTopPadding())
                    .height(50.dp)
                    .background(MaterialTheme.colorScheme.background)
                    .padding(horizontal = 10.dp)
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        modifier = Modifier.clickable { showAccountSelector = true },
                        imageVector = Icons.Default.Add,
                        contentDescription = "",
                        colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                    )
                    Spacer(modifier = Modifier.width(10.dp))
                    ScrollableTabRow(
                        selectedTabIndex = pagerState.currentPage,
                        containerColor = MaterialTheme.colorScheme.background,
                        indicator = {},
                        divider = {},
                        modifier = Modifier.weight(1f),
                        edgePadding = 0.dp
                    ) {
                        viewModel.userEmails.value.forEachIndexed { index, email ->
                            Tab(
                                selected = pagerState.currentPage == index,
                                enabled = false,
                                onClick = {
                                    scope.launch {
                                        pagerState.scrollToPage(index)
                                    }
                                },
                                text = {
                                    TextButton(
                                        onClick = {
                                            scope.launch {
                                                pagerState.scrollToPage(index)
                                            }
                                        },
                                        colors = ButtonDefaults.textButtonColors(containerColor = MaterialTheme.colorScheme.surface),
                                        modifier = Modifier
                                            .padding(10.dp),
                                        shape = RoundedCornerShape(50.dp)
                                    ) {
                                        Text(
                                            text = email,
                                            color = if (pagerState.currentPage == index) MaterialTheme.colorScheme.onSurfaceVariant else MaterialTheme.colorScheme.onSurface,
                                            style = MaterialTheme.typography.bodySmall
                                        )
                                    }

                                }
                            )
                        }
                    }

                }
            }

        }


    }


}


@Composable
fun AskDrivePermission(
    paddingValues: PaddingValues,
    askPermission: () -> Unit
) {

    val scrollState = rememberScrollState()

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(paddingValues = paddingValues)
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)

            .verticalScroll(state = scrollState)
    ) {

        val painter = painterResource(id = R.drawable.connect_drive)
        Image(
            painter = painter,
            contentDescription = "Connect Google Drive",
            modifier = Modifier
                .height(250.dp)
                .width(250.dp)
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(25.dp)
        )


        Text(
            text = """
                    Explore Your Videos from Google Drive
                    -------------------------------------
            
                    Ready to view your videos from Google Drive? Let's get started:
            
                    🚀 Simply tap "Connect Google Drive" below.
                    📲 Sign in with your Google account and grant permissions.
                    🎉 Your videos will appear right here for instant access.
            
                    Connecting now ensures you can enjoy your videos without any hassle.
                """.trimIndent(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.padding(horizontal = 20.dp),
            color = MaterialTheme.colorScheme.onBackground,
        )

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(35.dp)
        )

        Button(onClick = { askPermission() }) {
            Text(
                text = "Connect Google Drive",
                style = MaterialTheme.typography.labelLarge,
                color = MaterialTheme.colorScheme.onPrimary,
            )
        }

    }

}


@Composable
fun ShowGoogleAccountSelection(
    lastChecked: Long,
    onAccountNameSelected: (String) -> Unit,
    updateLastChecked: () -> Unit,
    onDiscarded: () -> Unit
) {

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult()
    ) { result ->
        updateLastChecked()
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val name = result.data?.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
            onAccountNameSelected(name ?: "")
        } else {
            onDiscarded()
        }
    }
    val intent = AccountManager.newChooseAccountIntent(
        null,
        null,
        arrayOf(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE),
        null,
        null,
        null,
        null
    )
    LaunchedEffect(key1 = lastChecked) {
        launcher.launch(intent)
    }
}