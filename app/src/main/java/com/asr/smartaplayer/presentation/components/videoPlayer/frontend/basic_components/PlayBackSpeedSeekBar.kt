package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.asr.smartaplayer.R


@Composable
fun PlayBackSpeedSeekBar(
    onPlayBackSpeedChange : (Float) -> Unit,
    onPlayBackSpeedChangeFinished: () -> Unit,
    currentPlayBackSpeed: Float
){

    Row(
        verticalAlignment = Alignment.CenterVertically
    ){
        IconButton(onClick = { onPlayBackSpeedChange(1f)}){
            Image(
                painter = painterResource(id = R.drawable.baseline_1x_mobiledata_24),
                contentDescription = "reset speed",
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground),
                modifier = Modifier
                    .height(30.dp)
                    .width(30.dp)
            )
        }

        Text(
            text = "%.2fX".format(currentPlayBackSpeed),
            color = MaterialTheme.colorScheme.onBackground,
            fontSize = 16.sp,
            modifier = Modifier.width(50.dp)
        )

        Spacer(modifier = Modifier.width(10.dp))


        SeekBar(
            modifier = Modifier.weight(1f),
            endValue = 4f,
            startValue = 0.2f,
            currentValue = currentPlayBackSpeed,
            seekStarted = { },
            seekEnded = { onPlayBackSpeedChangeFinished() },
            seekTo = { onPlayBackSpeedChange(it)},
            thumbPadding = PaddingValues(top = 3.dp),
            thumbSize = DpSize(13.dp, 13.dp)
        )
    }





}