package com.asr.smartaplayer.presentation

import android.app.PictureInPictureParams
import android.content.Context
import android.util.Log
import androidx.annotation.OptIn
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.util.UnstableApi
import androidx.media3.database.StandaloneDatabaseProvider
import androidx.media3.datasource.cache.LeastRecentlyUsedCacheEvictor
import androidx.media3.datasource.cache.SimpleCache
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.GlobalVideoMetadata
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.models.SubtitlesDisplaySettings
import com.asr.smartaplayer.domain.models.UserLeaveHintAction
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.useCases.AllUseCases
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartPlayerVideoStateEvent
import com.asr.smartaplayer.presentation.components.videoPlayer.getCurrentBrightness
import com.asr.smartaplayer.presentation.components.videoPlayer.getCurrentVolume
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject


@HiltViewModel
class MainActivityViewModel @OptIn(UnstableApi::class) @Inject constructor(
    private val allUseCases: AllUseCases,
    @ApplicationContext private val applicationContext: Context
) : ViewModel(), DefaultLifecycleObserver {

    val hasAccessToSeePrivateVideos: MutableState<Boolean> = mutableStateOf(false)
    val isOneBiometricAuthEnabled: MutableState<Boolean> = mutableStateOf(false)
    private val userLeaveHintAction: MutableState<UserLeaveHintAction?> = mutableStateOf(null)
    var currentDestination: MutableState<AppDestination?>  = mutableStateOf(null)
    var exoPlayer: SmartExoPlayer? = null
    var isMiniPlayerOpen: MutableState<Boolean> = mutableStateOf(false)
    var videoCommentsState: MutableState<List<VideoComment>> = mutableStateOf(emptyList())
    private val subtitlesDisplaySettings: MutableState<SubtitlesDisplaySettings?>  = mutableStateOf(null)

    val currentNavIndicatorBottomIndex = mutableIntStateOf(0)
    val currentSearchText = mutableStateOf<String?>(null)
    private var cache: SimpleCache? = null


    private fun ensureExoPlayer(context: Context, action: (SmartExoPlayer) -> Unit) {
        if (exoPlayer == null) {
            exoPlayer = SmartExoPlayer(
                context = context,
                onVideoPlayStateChangedCallback = { this.onVideoStateChangedCallback(it) },
                cache = cache!!,
                subtitlesDisplaySettings = subtitlesDisplaySettings.value!!
            )
        }
        action(exoPlayer!!)
    }


    init {
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.IsOneBiometricAuthEnabled)
                .collectLatest {
                    isOneBiometricAuthEnabled.value = it
                }
        }
        viewModelScope.launch {
            val cacheSize = allUseCases.preferencesUseCases.getAppSettingsForKey(SettingsKey.DriveVideoCacheSize)
            cache = SimpleCache(
                File(applicationContext.cacheDir, "media"),
                LeastRecentlyUsedCacheEvictor(cacheSize),
                StandaloneDatabaseProvider(applicationContext)
            )
        }
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.UserLeaveHintActionKey)
                .collectLatest {
                    userLeaveHintAction.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.GlobalSubtitlesDisplaySettings)
                .collectLatest {
                    subtitlesDisplaySettings.value = it
                }
        }
    }

    private fun onVideoStateChangedCallback(event: SmartPlayerVideoStateEvent) {
        when (event) {
            SmartPlayerVideoStateEvent.PLAYBACK_STARTED -> {
                isMiniPlayerOpen.value = true
                viewModelScope.launch {
                    val videoItem =
                        exoPlayer?.videoPlayerState?.currentPlayingMedia?.value?.videoItem?.value
                    if (videoItem != null) {
                        val result = allUseCases.videoUseCases.getAllVideoComments(
                            videoItem = videoItem
                        )
                        if (result is UseCaseTaskResult.Success){
                            videoCommentsState.value = result.data
                        }else{
                            showToast(
                                applicationContext,
                                "Something went wrong in loading comments for video"
                            )
                        }
                    }
                }
            }

            SmartPlayerVideoStateEvent.PLAYBACK_PAUSED -> {
                saveCurrentVideoState()
            }

            SmartPlayerVideoStateEvent.PLAYBACK_RESUMED -> {}
            SmartPlayerVideoStateEvent.PLAYBACK_STOPPED_REQUEST -> {
                saveCurrentVideoState()
                isMiniPlayerOpen.value = false
                videoCommentsState.value = emptyList()
            }

            SmartPlayerVideoStateEvent.PLAYBACK_START_REQUEST -> {
                saveCurrentVideoState()
                videoCommentsState.value = emptyList()
            }
        }
    }

    fun addCommentInVideo(video: BaseVideoItem, videoComment: VideoComment, context: Context) {
        viewModelScope.launch {
            val success = allUseCases.videoUseCases.addVideoComment(
                videoItem = video,
                videoComment = videoComment
            )
            if (success){
                var comments = videoCommentsState.value + listOf(videoComment)
                comments = comments.sortedBy { it.playBackPosition }
                videoCommentsState.value = comments
                showToast(applicationContext, "Bookmarked position successfully ✔️")
            }else{
                showToast(applicationContext, "something went wrong !!")
            }

        }
    }

    fun removeCommentInVideo(videoItem: BaseVideoItem, comment: VideoComment) {
        viewModelScope.launch {
            val success = allUseCases.videoUseCases.deleteCommentInVideo(
                videoItem = videoItem,
                comment = comment
            )
            if (success){
                val updatedComments = videoCommentsState.value.filter { it.playBackPosition != comment.playBackPosition }
                videoCommentsState.value = updatedComments
            }
            else{
                showToast(applicationContext, "something went wrong !!")
            }
        }
    }

    private fun saveCurrentVideoState() {
        val currentPlayingMedia = exoPlayer?.videoPlayerState?.currentPlayingMedia?.value
        if (currentPlayingMedia != null) {

            val metaData = VideoMetaData(
                lastPlayedPosition = currentPlayingMedia.lastPlayedPosition.value,
                brightnessIntensity = currentPlayingMedia.brightnessIntensity.value,
                playBackSpeed = currentPlayingMedia.playBackSpeed.value,
                totalDuration = currentPlayingMedia.totalDuration.value,
                audioEqualizerSettings = currentPlayingMedia.audioEqualizerSettings.value,
                playBackPitch = currentPlayingMedia.playBackPitch.value,
                isSubtitlesEnabled = currentPlayingMedia.isSubtitleEnabled.value,
                selectedSubtitle = currentPlayingMedia.selectedSubtitle.value,
                visualEffectsSettings = currentPlayingMedia.visualEffect.value
            )

            val globalVideoMetadata = GlobalVideoMetadata(
                playBackSpeed = metaData.playBackSpeed,
                playBackPitch = metaData.playBackPitch,
                audioEqualizerSettings = metaData.audioEqualizerSettings,
                visualEffectsSettings = metaData.visualEffectsSettings
            )

            viewModelScope.launch {
                allUseCases.videoUseCases.updateLocalVideoMetaData(
                    metaData,
                    exoPlayer?.videoPlayerState?.currentPlayingMedia?.value!!.videoItem.value,
                )
            }
            viewModelScope.launch {
                allUseCases.preferencesUseCases.updateAppSettingsForKey(
                    SettingsKey.GlobalOptionalPreservableVideoMetaData, globalVideoMetadata
                )
            }
        }
    }

    @UnstableApi
    fun playVideo(videoItem: BaseVideoItem, context: Context) = ensureExoPlayer(context) {
        viewModelScope.launch {
            val currentGlobalVideoMetadata = allUseCases.preferencesUseCases.getAppSettingsForKey(SettingsKey.GlobalOptionalPreservableVideoMetaData)
            val defaultGlobalData = SettingsKey.GlobalOptionalPreservableVideoMetaData.defaultValue

            val videoMetaData = allUseCases.videoUseCases.getVideoMetaData(videoItem) ?: VideoMetaData(
                lastPlayedPosition = 0,
                brightnessIntensity = getCurrentBrightness(context),
                playBackSpeed =defaultGlobalData.playBackSpeed,
                totalDuration = 0,
                audioEqualizerSettings = defaultGlobalData.audioEqualizerSettings,
                playBackPitch = defaultGlobalData.playBackPitch,
                isSubtitlesEnabled = false,
                selectedSubtitle = null,
                visualEffectsSettings = defaultGlobalData.visualEffectsSettings
            )
            val preservedProperties =
                allUseCases.preferencesUseCases.getAppSettingsForKey(
                    SettingsKey.VideosOptionalPreservableProperties
                ).toSet()

            val updatedMetaData = videoMetaData.applyGlobalConfigOnNonPreservedProperties(
                preservedProperties = preservedProperties,
                globalPreservedVideoMetaData = currentGlobalVideoMetadata
            )

            when (videoItem) {
                is LocalVideoItem -> exoPlayer?.playVideo(
                    videoItem = videoItem,
                    videoMetaData = updatedMetaData
                )

                is DriveVideoItem -> {
                    val authToken =
                        allUseCases.videoUseCases.getVideoAccessToken(
                            videoItem = videoItem
                        ) ?: return@launch
                    exoPlayer?.playVideo(videoItem, videoMetaData, authToken)
                }
            }

        }
    }

    fun checkAndCloseVideoIfPlaying() {
        exoPlayer?.closeVideoPlayer()
        exoPlayer = null
    }

    fun updateAccessToPrivateVideos(access: Boolean) {
        hasAccessToSeePrivateVideos.value = access
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        if (currentDestination.value == null || currentDestination.value !is AppDestination.ImmersiveModeScreen){
            exoPlayer?.softPlay()
            return
        }
        when(userLeaveHintAction.value){
            UserLeaveHintAction.PAUSE -> exoPlayer?.softPlay()
            else -> {}
        }
    }

    override fun onCleared() {
        super.onCleared()
        ProcessLifecycleOwner.get().lifecycle.removeObserver(this)
    }

    fun userLeaveHintReceived(activity: MainActivity){
        Log.d("BACKGROUND", "userLeaveHintReceived: ")
        if (currentDestination.value == null || currentDestination.value !is AppDestination.ImmersiveModeScreen){
            exoPlayer?.softPause()
            return
        }
        when(userLeaveHintAction.value){
            UserLeaveHintAction.PAUSE -> exoPlayer?.softPause()
            UserLeaveHintAction.PIP -> {
                val aspectRatio = exoPlayer?.getVideoAspectRatio()
                val params =
                    PictureInPictureParams.Builder().setAspectRatio(aspectRatio).build()
                activity.enterPictureInPictureMode(params)
            }
            UserLeaveHintAction.AUDIO_ONLY -> {}
            else -> {}
        }

    }

}