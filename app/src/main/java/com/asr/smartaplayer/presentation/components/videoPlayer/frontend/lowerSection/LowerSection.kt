package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.lowerSection

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayBackSeekBar
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayBackSegmentsData
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.PlayButton
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SeekBar
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.scrim.humanReadableDuration
import com.asr.smartaplayer.presentation.components.videoPlayer.getActivity
import dev.vivvvek.seeker.Segment


@Composable
fun LowerSection(
    modifier: Modifier,
    totalDuration: Long,
    currentValue: Long,
    bufferedPosition: Long,
    seekStarted: () -> Unit,
    seekTo: (Long) -> Unit,
    seekEnded: () -> Unit,
    lockControlsRequested: () -> Unit,
    addComment: (String, Long)-> Unit,
    videoComments: List<VideoComment>
) {

    val context = LocalContext.current
    Column(
        modifier = modifier
            .background(
                Brush.verticalGradient(
                    listOf(Color.Transparent, MaterialTheme.colorScheme.background)
                )
            )
            .padding(PaddingValues(start = 20.dp, end = 20.dp, bottom = 10.dp))
            .fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {


        val segmentsData = remember(key1 = videoComments) {
            val allSegments = mutableListOf<Segment>()
            var minComment = Long.MAX_VALUE
            var maxComment = Long.MIN_VALUE
            for (comment in videoComments) {
                allSegments.add(
                    Segment(
                        name = comment.comment,
                        start = comment.playBackPosition.toFloat()
                    )
                )
                minComment = minOf(comment.playBackPosition, minComment)
                maxComment = maxOf(comment.playBackPosition, maxComment)
            }
            allSegments.add(0, Segment(start = 0f, name = "start"))
            PlayBackSegmentsData(
                segments = allSegments,
                minSegmentStart = minComment,
                maxSegmentStart = maxComment
            )
        }


        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.Bottom
        ) {
            Text(
                text = humanReadableDuration(currentValue),
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 12.sp
            )
            PlayBackSeekBar(
                modifier = Modifier
                    .weight(1f)
                    .height(15.dp),
                seekStarted = seekStarted,
                seekEnded = seekEnded,
                seekTo = { seekTo(it.toLong()) },
                endValue = totalDuration.toFloat(),
                currentValue = currentValue.toFloat(),
                allSegments = segmentsData.segments,
                trackHeight = 5.dp,
                thumbRadius = 8.dp,
                bufferedPosition = bufferedPosition.toFloat(),
            )
            Text(
                text = humanReadableDuration(totalDuration),
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 12.sp
            )
        }


        Spacer(modifier = Modifier.height(15.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {

            GeneralIconButton(
                modifier = Modifier
                    .width(50.dp)
                    .aspectRatio(1f),
                onClick = lockControlsRequested,
                iconColor = MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.baseline_lock_24,
                paddingValues = PaddingValues(14.dp),
                contentDescription = "Lock the video player controls"
            )


            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.wrapContentWidth()
            ) {

                GeneralIconButton(
                    modifier = Modifier
                        .width(50.dp)
                        .aspectRatio(1f),
                    onClick = {
                        val previousComment =
                            segmentsData.segments.lastOrNull { it.start < currentValue - 1000 }
                        if (previousComment != null)
                            seekTo(previousComment.start.toLong())
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = androidx.media3.ui.R.drawable.exo_ic_chevron_left,
                    contentDescription = "go to previous bookmarked position",
                    enabled = currentValue > segmentsData.minSegmentStart,
                    paddingValues = PaddingValues(14.dp)
                )
                Spacer(modifier = Modifier.width(5.dp))
                GeneralIconButton(
                    modifier = Modifier
                        .width(50.dp)
                        .aspectRatio(1f),
                    onClick = { addComment("", currentValue)
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = R.drawable.baseline_bookmark_add_24,
                    contentDescription = "bookmark this position",
                    paddingValues = PaddingValues(14.dp)
                )
                Spacer(modifier = Modifier.width(5.dp))
                GeneralIconButton(
                    modifier = Modifier
                        .width(50.dp)
                        .aspectRatio(1f),
                    onClick = {
                        val nextComment =
                            segmentsData.segments.firstOrNull { it.start > currentValue }
                        if (nextComment != null)
                            seekTo(nextComment.start.toLong())
                    },
                    iconColor = MaterialTheme.colorScheme.onBackground,
                    iconId = androidx.media3.ui.R.drawable.exo_ic_chevron_right,
                    contentDescription = "go to next bookmarked position",
                    enabled = currentValue < segmentsData.maxSegmentStart,
                    paddingValues = PaddingValues(14.dp)
                )
            }

            GeneralIconButton(
                modifier = Modifier
                    .width(50.dp)
                    .aspectRatio(1f),
                onClick = { rotateScreen(context) },
                iconColor = MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.baseline_screen_rotation_24,
                contentDescription = "Rotate the  screen",
                paddingValues = PaddingValues(14.dp)
            )

        }

    }


}


fun rotateScreen(context: Context) {

    val activity = context.getActivity()

    activity?.let {

        val orientation: Int = it.resources.configuration.orientation
        it.requestedOrientation =
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) ActivityInfo.SCREEN_ORIENTATION_PORTRAIT else ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

    }


}
