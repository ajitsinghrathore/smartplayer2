package com.asr.smartaplayer.presentation.screens.settings.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.SeekBar
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsTileIcon
import com.asr.smartaplayer.presentation.screens.settings.components.SettingsTileTexts


@Composable
fun SettingsSlider(
    modifier: Modifier = Modifier,
    title: String?,
    description: String?,
    onValueChange: (Float) -> Unit = {},
    start: Float,
    end: Float,
    currentValue: Float,
    icon: Int?,
    onValueChangeFinished: (Float) -> Unit,
    currentDisplayValue: String? = null,
    currentDisplayedComposable: (@Composable () -> Unit)? = null
) {


    Row(
        modifier = modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        SettingsTileIcon(icon = {
            if (icon != null) {
                Image(
                    modifier = Modifier
                        .size(20.dp),
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }

        })


        Column(
            modifier = Modifier.weight(1f)
        ) {
            SettingsTileTexts(
                modifier = Modifier,
                title = {
                    title?.let {
                        Text(
                            text = it,
                            color = MaterialTheme.colorScheme.onBackground,
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                },
                subtitle = {
                    description?.let {
                        Text(
                            text = description,
                            color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.8f),
                            style = MaterialTheme.typography.bodySmall
                        )
                    }
                }
            )
            if (currentDisplayValue != null) {
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = currentDisplayValue,
                    color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.8f),
                    style = MaterialTheme.typography.bodyMedium,
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
            }
            if (currentDisplayedComposable != null) {
                Spacer(modifier = Modifier.height(5.dp))
                currentDisplayedComposable()
            }
            Spacer(modifier = Modifier.height(5.dp))

            SeekBar(
                modifier = Modifier.fillMaxWidth(),
                endValue = end,
                startValue = start,
                currentValue = currentValue,
                seekStarted = { },
                seekTo = {
                    onValueChange(it)
                },
                seekEnded = {
                    onValueChangeFinished(currentValue)
                },
                thumbSize = DpSize(13.dp, 13.dp),
                thumbPadding = PaddingValues(top = 3.dp)
            )


        }
    }


}