package com.asr.smartaplayer.presentation.screens.immersiveVideoPlayBack

import android.content.Context
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.useCases.AllUseCases
import com.asr.smartaplayer.domain.useCases.preferences.PreferencesUseCases
import com.asr.smartaplayer.domain.useCases.videosUseCases.VideoUseCases
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import com.asr.smartaplayer.presentation.drowsyDetection.CameraLifeCycleOwner
import com.asr.smartaplayer.presentation.drowsyDetection.DrowsyDetector
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImmersiveVideoViewModel @Inject constructor(
    private val allUseCases: AllUseCases,
) : ViewModel() {


    val isDrowsyEnabled: MutableState<Boolean> = mutableStateOf(false)
    val isDrowsyEnabledRequested: MutableState<Boolean> = mutableStateOf(false)

    private var cameraLifeCycleOwner: CameraLifeCycleOwner? = null

    private var drowsyDetector: DrowsyDetector? = null


    fun initializeCameraLifeCycleOwner(context: Context, exoPlayer: SmartExoPlayer){
        viewModelScope.launch {
            cameraLifeCycleOwner?.stopLifeCycle()
            val sensitivity = allUseCases.preferencesUseCases.getAppSettingsForKey(SettingsKey.DrowsinessSensitivity)
            cameraLifeCycleOwner = CameraLifeCycleOwner(exoPlayer, sensitivity = sensitivity.toInt())
            drowsyDetector = DrowsyDetector()
            drowsyDetector?.registerFaceAnalysis(context, cameraLifeCycleOwner!!)
        }
    }


    fun toggleDrowsyDetection(enable: Boolean){
        if(enable){
            cameraLifeCycleOwner?.let {
                it.startLifeCycle()
                isDrowsyEnabled.value = true
            }
        }else{
            cameraLifeCycleOwner?.let {
                it.stopLifeCycle()
                isDrowsyEnabled.value = false
            }
        }
    }


    suspend fun loadSubtitles(paginationState: GenericOffsetBasedPaginationState, searchText: String): List<Subtitle>?{
        return allUseCases.videoUseCases.getAllSubtitles(paginationState, searchText)
    }



}