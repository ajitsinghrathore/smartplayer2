@file:OptIn(ExperimentalFoundationApi::class)

package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.RadioButton
import androidx.compose.material.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.domain.models.Subtitle


@Composable
fun SubtitleItem(
    modifier: Modifier,
    isSelected: Boolean,
    subtitle: Subtitle,
    contentColor: Color,
    onSelect: () -> Unit
) {

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = isSelected,
            onClick = onSelect,
            colors = RadioButtonDefaults.colors(
                selectedColor = MaterialTheme.colorScheme.primary,
                unselectedColor = MaterialTheme.colorScheme.primary
            )
        )

        Column(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f)
                .padding(10.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                modifier = Modifier.basicMarquee(),
                text = subtitle.displayName,
                style = MaterialTheme.typography.bodyLarge,
                maxLines = 1,
                color = contentColor,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = subtitle.mimeType,
                style = MaterialTheme.typography.bodySmall,
                maxLines = 1,
                color = contentColor,
                overflow = TextOverflow.Ellipsis
            )
        }


    }


}