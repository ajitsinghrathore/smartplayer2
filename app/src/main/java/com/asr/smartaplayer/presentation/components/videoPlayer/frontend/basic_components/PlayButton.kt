package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R

@Composable
fun PlayButton(
    modifier: Modifier,
    isPlaying: Boolean,
    paddingValues: PaddingValues = PaddingValues(0.dp),
    togglePlaying : (Boolean) -> Unit,
){

    GeneralIconButton(
        modifier = modifier,
        onClick = {
                  togglePlaying(!isPlaying)
        },
        iconColor = MaterialTheme.colorScheme.onBackground,
        iconId = if (isPlaying) R.drawable.baseline_pause_24 else R.drawable.baseline_play_arrow_24,
        contentDescription = " play or pause the video ",
        paddingValues = paddingValues
    )

}