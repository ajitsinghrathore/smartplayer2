package com.asr.smartaplayer.presentation.screens.driveScreen

import android.content.Context
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.useCases.AllUseCases
import com.asr.smartaplayer.domain.utils.KeyBasedPaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import com.asr.smartaplayer.presentation.getSateValueFromMapAndCreateIfNotThere
import com.asr.smartaplayer.presentation.getValueFromMapAndCreateIfNotThere
import com.asr.smartaplayer.presentation.showToast
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.log


@HiltViewModel
class DriveScreenViewModel @Inject constructor(
    private val allUseCases: AllUseCases,
    @ApplicationContext private val context: Context
) : ViewModel() {

    val swipeToDismissMiniPlayer: MutableState<Boolean> = mutableStateOf(false)
    val userEmails: MutableState<List<String>> = mutableStateOf(emptyList())


    private var paginationStateForEmails: MutableMap<String, KeyBasedPaginationState> =
        mutableMapOf()
    private var videosForEmails: MutableMap<String, MutableState<List<DriveVideoItem>>> =
        mutableMapOf()
    private var loadingStatusStateForEmails: MutableMap<String, MutableState<Boolean>> =
        mutableMapOf()


    init {
        Log.d("DRIVE", ":  CREATED VIEWMODEL")
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.AllUserEmails)
                .collect {
                    userEmails.value = it
                }
        }
        viewModelScope.launch {
            allUseCases.preferencesUseCases.listenForUpdatedSettingsForKey(SettingsKey.SwipeToDismissVideoPreview)
                .collect {
                    swipeToDismissMiniPlayer.value = it
                }
        }
    }


    fun getLoadingStatusStateForEmail(userEmail: String): MutableState<Boolean> {
        return getSateValueFromMapAndCreateIfNotThere(
            key = userEmail,
            map = loadingStatusStateForEmails,
            defaultValueForState = false
        )
    }

    fun getVideosForUserEmail(userEmail: String): MutableState<List<DriveVideoItem>> {
        return getSateValueFromMapAndCreateIfNotThere(
            key = userEmail,
            map = videosForEmails,
            defaultValueForState = emptyList()
        )
    }

    fun addUserEmail(email: String) {
        viewModelScope.launch {
            if (email in userEmails.value) {
                return@launch
            }
            val updatedEmails = userEmails.value + listOf(email)
            allUseCases.preferencesUseCases.updateAppSettingsForKey(
                SettingsKey.AllUserEmails,
                updatedEmails
            )
        }

    }

    fun loadMoreVideos(freshLoad: Boolean = false, userEmail: String) {
        viewModelScope.launch {
            Log.d("DRIVE", "loadMoreVideos: $freshLoad  $userEmail")

            var paginationState = getValueFromMapAndCreateIfNotThere(
                key = userEmail,
                map = paginationStateForEmails,
                defaultValue = KeyBasedPaginationState()
            )

            if (paginationState.isLoading || paginationState.paginationCompleted) {
                if (!freshLoad) {
                    return@launch
                }
            }
            if (freshLoad) {
                paginationState = KeyBasedPaginationState()
                paginationStateForEmails[userEmail] = paginationState
            }
            Log.d("TAG___", ": load more starting $paginationState")

            val currentUiLoadingState = getSateValueFromMapAndCreateIfNotThere(
                key = userEmail,
                map = loadingStatusStateForEmails,
                defaultValueForState = false
            )
            currentUiLoadingState.value = true


            val result = allUseCases.videoUseCases.getAllDriveVideos(
                paginationState = paginationState,
                userEmail = userEmail
            )
            if (result is UseCaseTaskResult.Success) {
                val currentVideos = getSateValueFromMapAndCreateIfNotThere(
                    key = userEmail,
                    map = videosForEmails,
                    defaultValueForState = emptyList()
                )
                if (freshLoad) {
                    currentVideos.value = result.data
                } else {
                    currentVideos.value += result.data
                }
            }
            else{
                showToast(context = context, message = "something went wrong")
            }
            paginationState.isLoading = false
            currentUiLoadingState.value = false
        }
    }

    fun loadVideoForEmailIfNotLoaded(userEmail: String) {
        val currentVideos = videosForEmails[userEmail]
        Log.d("DRIVE*", "loadVideoForEmailIfNotLoaded: $currentVideos")
        if (currentVideos == null || currentVideos.value.isEmpty()){
            Log.d("DRIVE*", "loadVideoForEmailIfNotLoaded: ")
            loadMoreVideos(true, userEmail)
        }
    }


}