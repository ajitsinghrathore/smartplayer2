package com.asr.smartaplayer.presentation.components.videoPlayer

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.media.AudioManager
import android.os.Handler
import android.os.HandlerThread
import android.provider.Settings
import android.util.Log
import android.view.PixelCopy
import android.view.SurfaceView
import android.view.WindowManager
import androidx.documentfile.provider.DocumentFile
import androidx.media3.common.util.UnstableApi
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.presentation.components.videoPlayer.backend.SmartExoPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


fun getCurrentBrightness(context: Context): Float {

    val contentResolver = context.contentResolver
    val settingsBrightness = Settings.System.getFloat(
        contentResolver, Settings.System.SCREEN_BRIGHTNESS, 0f
    )
    return settingsBrightness / 255.toFloat()
}

fun getCurrentVolume(context: Context): Int {
    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
}

fun getDeviceMaxVolume(context: Context): Int {
    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    return audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
}

fun getDeviceMinVolume(context: Context): Int {
    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    return audioManager.getStreamMinVolume(AudioManager.STREAM_MUSIC)
}


fun updateSystemBrightness(context: Context, brightness: Float) {
    val window = context.getActivity()?.window!!
    val layoutParams: WindowManager.LayoutParams = window.attributes
    layoutParams.screenBrightness = brightness
    window.attributes = layoutParams
}


fun updateSystemVolume(context: Context, volume: Float) {
    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    Log.d("VOLUME", "updateSystemVolume: $volume updating to ...")
    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume.toInt(), 0)
}

enum class ExoPlayerControlsUiState {
    ShowAllControls,
    ShowBrightnessChange,
    ShowVolumeChange,
    ShowSeekChange,
    ShowPlayBackSpeedChangeControl,
    ShowVideoComments,
    ShowEqualizerSettings,
    ShowSubtitleSettings,
    ShowVisualEffectsSettings,
    HideAllControls
}

fun Context.getActivity(): Activity? {
    var currentContext = this
    while (currentContext is ContextWrapper) {
        if (currentContext is Activity) {
            return currentContext
        }
        currentContext = currentContext.baseContext
    }
    return null
}

@UnstableApi
suspend fun createVideoCommentItemForVideo(
    context: Context,
    videoItem: BaseVideoItem,
    comment: String,
    position: Long,
    smartExoPlayer: SmartExoPlayer,
    videoCommentCallback: (VideoComment) -> Unit
) {
    if (videoItem is LocalVideoItem) {
        withContext(Dispatchers.Main) {
            videoCommentCallback(VideoComment(comment = comment, playBackPosition = position))
        }

    } else {
        val targetFolder = context.getExternalFilesDir(null)!!
        var targetFile = DocumentFile.fromFile(targetFolder)
            .createFile("image/png", "$position${System.currentTimeMillis()}")
        if (targetFile == null) {
            targetFile = DocumentFile.fromFile(targetFolder).findFile("$position $videoItem")!!
        }

        val videoSurfaceView = smartExoPlayer.playerView?.videoSurfaceView as SurfaceView

        val bitmap: Bitmap = Bitmap.createBitmap(
            videoSurfaceView.width,
            videoSurfaceView.height,
            Bitmap.Config.ARGB_8888
        )

        try {
            val handlerThread = HandlerThread("PixelCopier")
            handlerThread.start()

            PixelCopy.request(
                videoSurfaceView, bitmap,
                { copyResult ->
                    if (copyResult == PixelCopy.SUCCESS) {
                        context.contentResolver.openOutputStream(targetFile.uri)?.use { output ->
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output)
                            val videoComment = VideoComment(
                                playBackPosition = position,
                                comment = comment,
                                thumbNail = targetFile.uri.toString()
                            )
                            videoCommentCallback(videoComment)
                        }
                    }
                    handlerThread.quitSafely()
                },
                Handler(handlerThread.looper)
            )
        } catch (e: IllegalArgumentException) {

            e.printStackTrace()
        }

    }

}



fun readableHertz(millihertz: Int): String = "${millihertz / 1000}Hz"
fun readableDb(milliBel: Short): String = "${milliBel / 100}dB"