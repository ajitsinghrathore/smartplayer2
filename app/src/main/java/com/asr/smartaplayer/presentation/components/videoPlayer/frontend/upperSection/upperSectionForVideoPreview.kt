package com.asr.smartaplayer.presentation.components.videoPlayer.frontend.upperSection

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton


@Composable
fun UpperSectionForVideoPreview(
    modifier: Modifier,
    onCloseMiniPlayerRequested: () -> Unit,
    onFullScreenModeRequested: () -> Unit,
) {


    Row(
        modifier = modifier
            .background(
                Brush.verticalGradient(
                    listOf(MaterialTheme.colorScheme.background, Color.Transparent)
                )
            )
            .padding(PaddingValues(all = 10.dp))
            .fillMaxWidth()
    ) {
        GeneralIconButton(
            modifier = Modifier
                .height(50.dp)
                .aspectRatio(1f),
            onClick = onCloseMiniPlayerRequested,
            iconId = R.drawable.baseline_close_24,
            iconColor = MaterialTheme.colorScheme.onBackground,
            contentDescription = "Close Button",
            paddingValues = PaddingValues(14.dp)
        )


        Box(
            modifier = Modifier
                .weight(1f),
            contentAlignment = Alignment.CenterEnd
        ) {
            GeneralIconButton(
                modifier = Modifier
                    .height(50.dp)
                    .aspectRatio(1f),
                onClick = onFullScreenModeRequested,
                iconColor = MaterialTheme.colorScheme.onBackground,
                iconId = R.drawable.baseline_fullscreen_24,
                contentDescription = "Enter immersive mode",
                paddingValues = PaddingValues(14.dp)
            )
        }
    }


}