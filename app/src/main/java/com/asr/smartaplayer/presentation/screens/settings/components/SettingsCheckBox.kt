package com.asr.smartaplayer.presentation.screens.settings.components


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material3.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxColors
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp

//
@Composable
fun SettingsCheckbox(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    icon: Int? = null,
    title: String,
    description: String? = null,
    checkboxColors: CheckboxColors = CheckboxDefaults.colors(),
    onCheckedChange: (Boolean) -> Unit,
    checked: Boolean,
) {

    Row(
        modifier = modifier
            .fillMaxWidth()
            .toggleable(
                enabled = enabled,
                value = checked,
                role = Role.Switch,
                onValueChange = { onCheckedChange(!checked) }
            ),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        SettingsTileIcon(icon = {
            if (icon != null) {
                Image(
                    modifier = Modifier
                        .size(20.dp),
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }

        })
        SettingsTileTexts(
            modifier = Modifier.weight(1f),
            title = {
                Text(
                    text = title,
                    color = MaterialTheme.colorScheme.onBackground,
                    style = MaterialTheme.typography.bodyLarge
                )
            },
            subtitle = {
                description?.let {
                    Text(
                        text = description,
                        color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.8f),
                        style = MaterialTheme.typography.bodySmall
                    )
                }

            }
        )
            Checkbox(
                enabled = enabled,
                checked = checked,
                onCheckedChange = { onCheckedChange(!checked) },
                modifier = Modifier.padding(end = 8.dp),
                colors = checkboxColors,
            )


    }

}
