package com.asr.smartaplayer.presentation.components.videoPlayer.frontend

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.asr.smartaplayer.R
import com.asr.smartaplayer.presentation.components.videoPlayer.ExoPlayerControlsUiState
import com.asr.smartaplayer.presentation.components.videoPlayer.frontend.basic_components.GeneralIconButton


@Composable
fun LeftSection(
    modifier: Modifier,
    onEqualizerSettingsClicked: () -> Unit,
    onPlayBackSpeedAndPitchSettingsClicked: () -> Unit
){


    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        GeneralIconButton(
            modifier = Modifier
                .size(50.dp)
                .background(
                    Brush.radialGradient(
                        listOf(MaterialTheme.colorScheme.background, Color.Transparent)
                    )
                ),
            onClick = onEqualizerSettingsClicked,
            iconColor = MaterialTheme.colorScheme.onBackground,
            iconId = R.drawable.baseline_equalizer_24,
            contentDescription = "Adjust audio equalizer",
            backGround =null,
            paddingValues = PaddingValues(10.dp)
        )


        Spacer(modifier = Modifier.height(15.dp))

        GeneralIconButton(
            modifier = Modifier
                .size(50.dp)
                .background(
                    Brush.radialGradient(
                        listOf(MaterialTheme.colorScheme.background, Color.Transparent)
                    )
                ),
            onClick = onPlayBackSpeedAndPitchSettingsClicked,
            iconColor = MaterialTheme.colorScheme.onBackground,
            iconId = R.drawable.baseline_speed_24,
            contentDescription = "Adjust audio speed and pitch",
            backGround =null,
            paddingValues = PaddingValues(10.dp)
        )


    }



}