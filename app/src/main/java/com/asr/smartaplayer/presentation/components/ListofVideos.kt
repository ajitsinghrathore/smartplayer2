package com.asr.smartaplayer.presentation.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.asr.smartaplayer.R
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem


@Composable
fun ListOfVideos(
    modifier: Modifier,
    videos: List<BaseVideoItem>,
    paddingValues: PaddingValues,
    listState: LazyListState,
    heightForHeader: Dp,
    isOneBiometricAuthEnabled: Boolean,
    onPlayRequested: (BaseVideoItem) -> Unit,
    onToggleIsVideoPrivate: (BaseVideoItem, Boolean) -> Unit,
    loadMoreVideos: () -> Unit,
    isLoading: Boolean
) {

    LazyColumn(
        contentPadding = paddingValues,
        state = listState,
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
        ) {


        item {
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(heightForHeader)
            )
        }

        itemsIndexed(items = videos) { index, item ->
            if (index >= videos.size - 10) {
                loadMoreVideos()
            }
            when (item) {

                is DriveVideoItem -> {
                    DriveVideoItem(
                        videoItem = item,
                        onVideoItemSelected = onPlayRequested
                    )
                }

                is LocalVideoItem -> {
                        LocalVideoItem(
                            videoItem = item,
                            isOneBiometricAuthEnabled = isOneBiometricAuthEnabled,
                            onVideoItemSelected = onPlayRequested,
                            onToggleIsVideoPrivate = {
                                onToggleIsVideoPrivate(item, it)
                            }
                        )
                }
            }
        }


        if (isLoading){
            items(count = 10){
                VideoLoadingItem()
            }
        }


        if (!isLoading && videos.isEmpty()){

            item {
                val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.no_data_available))
                LottieAnimation(
                    modifier = modifier,
                    composition = composition,
                    iterations = LottieConstants.IterateForever,
                )
            }

        }

    }

}