package com.asr.smartaplayer.presentation

import androidx.navigation.NamedNavArgument
import com.asr.smartaplayer.R


sealed class AppDestination(
    val route: String,
    val iconResource: Int? = null,
    val name: String,
    val index: Int? = null,
    val isFullScreenDestination: Boolean = false,
    val requiredNavArguments: List<NamedNavArgument> = emptyList(),
) {

    object HomeScreen : AppDestination(
        route = "home",
        iconResource = R.drawable.baseline_home_24,
        name = "Home",
        index = 0,
    ) {
        fun getCompleteRoute(): String {
            return route
        }
    }

    object DriveScreen : AppDestination(
        route = "drive",
        iconResource = R.drawable.drive,
        name = "Drive",
        index = 1,
    ) {
        fun getCompleteRoute(): String {
            return route
        }
    }


    object PlayListScreen : AppDestination(
        route = "playList",
        iconResource = R.drawable.baseline_playlist_play_24,
        name = "Playlist",
        index = 2
    ) {
        fun getCompleteRoute(): String {
            return route
        }
    }

    object SettingsScreen : AppDestination(
        route = "settings",
        iconResource = R.drawable.baseline_settings_24,
        name = "Settings",
        index = 2,
    ) {
        fun getCompleteRoute(): String {
            return route
        }
    }


    object ImmersiveModeScreen : AppDestination(
        route = "immersiveVideoPlayback",
        name = "immersiveVideoPlayBack",
        isFullScreenDestination = true
    )


    companion object {
        fun getAllBottomNavDestinations(): List<AppDestination> {
            return listOf(
                HomeScreen,
                DriveScreen,
                SettingsScreen
            )
        }


        fun getDestinationFromRoute(route: String?): AppDestination {
            return when (route) {
                "home" -> HomeScreen
                "drive" -> DriveScreen
                "playList" -> PlayListScreen
                "settings" -> SettingsScreen
                "immersiveVideoPlayback" -> ImmersiveModeScreen
                else -> HomeScreen
            }
        }

    }

}
