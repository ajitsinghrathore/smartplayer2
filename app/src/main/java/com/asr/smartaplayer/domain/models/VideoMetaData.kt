package com.asr.smartaplayer.domain.models

data class VideoMetaData(
    val lastPlayedPosition: Long,
    val brightnessIntensity: Float,
    val playBackSpeed: Float,
    val playBackPitch: Float,
    val totalDuration: Long,
    val audioEqualizerSettings: AudioEqualizerSettings,
    val visualEffectsSettings: VisualEffectsSettings,
    val isSubtitlesEnabled: Boolean,
    val selectedSubtitle: Subtitle?
) {

    fun applyGlobalConfigOnNonPreservedProperties(
        preservedProperties: Set<OptionalPreservableVideoProperties>,
        globalPreservedVideoMetaData: GlobalVideoMetadata
    ): VideoMetaData {

        val playBackSpeed =
            if (OptionalPreservableVideoProperties.PLAYBACK_SPEED !in preservedProperties) {
                globalPreservedVideoMetaData.playBackSpeed
            } else {
                this.playBackSpeed
            }

        val playBackPitch =
            if (OptionalPreservableVideoProperties.PLAYBACK_PITCH !in preservedProperties) {
                globalPreservedVideoMetaData.playBackPitch
            } else {
                this.playBackPitch
            }

        val audioEqualizerSettings =
            if (OptionalPreservableVideoProperties.AUDIO_EQUALIZER_CONFIGURATION !in preservedProperties) {
                globalPreservedVideoMetaData.audioEqualizerSettings
            } else {
                this.audioEqualizerSettings
            }

        val visualEffectsSettings =
            if (OptionalPreservableVideoProperties.VIDEO_VISUAL_EFFECTS !in preservedProperties) {
                globalPreservedVideoMetaData.visualEffectsSettings
            } else {
                this.visualEffectsSettings
            }

        return VideoMetaData(
            lastPlayedPosition = lastPlayedPosition,
            brightnessIntensity = brightnessIntensity,
            playBackSpeed = playBackSpeed,
            totalDuration = totalDuration,
            audioEqualizerSettings = audioEqualizerSettings,
            playBackPitch = playBackPitch,
            isSubtitlesEnabled = isSubtitlesEnabled,
            selectedSubtitle = selectedSubtitle,
            visualEffectsSettings = visualEffectsSettings
        )
    }

}


data class GlobalVideoMetadata(
    val playBackSpeed: Float,
    val playBackPitch: Float,
    val audioEqualizerSettings: AudioEqualizerSettings,
    val visualEffectsSettings: VisualEffectsSettings
)


data class AudioEqualizerSettings(
    val audioPresetMode: Int,
    val audioReverbMode: Int,
    val audioBassBoost: Int,
    val audioLoudnessBoost: Int,
    val customAudioBandValues: Map<String, Short>
)

data class VisualEffectsSettings(
    val enabled: Boolean,
    val grayScaleEnabled: Boolean,
    val invertedColorsEnabled: Boolean,
    val videoTotalRotation: Float,
    val contrast: Float,
    val hue: Float,
    val saturation: Float
)