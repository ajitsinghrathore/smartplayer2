package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetAllVideoComments @Inject constructor(private val allVideosRepository: AllVideosRepository) {


    suspend operator fun invoke(videoItem: BaseVideoItem): UseCaseTaskResult<List<VideoComment>> =
        withContext(Dispatchers.IO) {
            try {
                UseCaseTaskResult.Success(
                    allVideosRepository.getAllCommentsForVideo(videoItem = videoItem)
                )
            }catch (e: Exception){
                UseCaseTaskResult.Failure(message = e.message.toString())
            }

        }


}