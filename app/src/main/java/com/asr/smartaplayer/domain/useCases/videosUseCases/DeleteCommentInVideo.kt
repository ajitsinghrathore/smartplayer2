package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DeleteCommentInVideo @Inject constructor(private val repository: AllVideosRepository) {


    suspend operator fun invoke(videoItem: BaseVideoItem, comment: VideoComment): Boolean =
        withContext(Dispatchers.IO) {
            repository.deleteCommentInVideo(videoItem, comment)
        }


}