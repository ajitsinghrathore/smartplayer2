package com.asr.smartaplayer.domain.repositoryContracts

import kotlinx.coroutines.flow.Flow
import java.lang.reflect.Type
import kotlin.reflect.KClass

interface SettingsRepository {

    suspend fun <DataType> getDataStoreValueForKey(key: String, classType: Type): DataType?
    suspend fun <DataType> getDataStoreValueForKeyAsFlow(key: String, classType: Type): Flow<DataType?>
    suspend fun <DataType> updateDataStoreValueForKey(key: String, value: DataType): Boolean


}