package com.asr.smartaplayer.domain.utils

sealed class UseCaseTaskResult<T>{



    data class Success<T>(val data: T): UseCaseTaskResult<T>()

    data class Failure<T>(val message: String): UseCaseTaskResult<T>()

    data class Progress<T>(val progress: Float): UseCaseTaskResult<T>()
}
