package com.asr.smartaplayer.domain.models

import kotlin.Boolean

abstract class BaseVideoItem(
    open val videoUrl: String,
    open val videoName: String,
    open val videoDuration: String,
    open val videoSize: String
)


data class LocalVideoItem(
    override val videoUrl: String,
    override val videoName: String,
    override val videoDuration: String,
    override val videoSize: String,
    val isPrivate: Boolean
) : BaseVideoItem(
    videoDuration = videoDuration,
    videoSize = videoSize,
    videoName = videoName,
    videoUrl = videoUrl
)


data class DriveVideoItem(
    override val videoDuration: String,
    override val videoName: String,
    override val videoSize: String,
    override val videoUrl: String,
    val videoId: String,
    val userDriveEmail: String,
    val videoThumbNailLink: String
) : BaseVideoItem(
    videoDuration = videoDuration,
    videoSize = videoSize,
    videoName = videoName,
    videoUrl = videoUrl
)

