package com.asr.smartaplayer.domain.useCases.preferences

import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.repositoryContracts.SettingsRepository
import com.google.gson.reflect.TypeToken
import javax.inject.Inject


class GetAppSettingsForKey @Inject constructor(val settingsRepository: SettingsRepository) {

    suspend inline operator fun <reified T: Any>  invoke(settingsKey: SettingsKey<T>): T{
        val type = object: TypeToken<T>() {}.type
        return settingsRepository.getDataStoreValueForKey(key = settingsKey.key, classType = type)?:settingsKey.defaultValue
    }

}





