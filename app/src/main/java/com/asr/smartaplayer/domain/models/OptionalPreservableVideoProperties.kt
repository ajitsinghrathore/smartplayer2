package com.asr.smartaplayer.domain.models

enum class OptionalPreservableVideoProperties {
    PLAYBACK_SPEED,
    PLAYBACK_PITCH,
    AUDIO_EQUALIZER_CONFIGURATION,
    VIDEO_VISUAL_EFFECTS
}