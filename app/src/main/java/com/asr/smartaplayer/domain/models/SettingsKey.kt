package com.asr.smartaplayer.domain.models

sealed class SettingsKey<T> {
    abstract val defaultValue: T
    open val key: String
        get() = this::class.simpleName ?: ""

    object DrowsinessSensitivity : SettingsKey<Float>() {
        override val defaultValue: Float
            get() = 20f
    }

    object SwipeToDismissVideoPreview : SettingsKey<Boolean>() {
        override val defaultValue: Boolean
            get() = true
    }

    object VideosOptionalPreservableProperties : SettingsKey<List<OptionalPreservableVideoProperties>>() {
        override val defaultValue: List<OptionalPreservableVideoProperties>
            get() = listOf(
                OptionalPreservableVideoProperties.VIDEO_VISUAL_EFFECTS
            )
    }

    object VideosSortingPreference : SettingsKey<VideosSortPreference>() {
        override val defaultValue: VideosSortPreference
            get() = VideosSortPreference(
                orderVideosIn = OrderVideosIn.DESCENDING,
                orderVideosBy = OrderVideosBy.LAST_MODIFIED
            )

    }

    object IsOneBiometricAuthEnabled : SettingsKey<Boolean>() {
        override val defaultValue: Boolean
            get() = true
    }


    object AllUserEmails : SettingsKey<List<String>>() {
        override val defaultValue: List<String>
            get() = emptyList()

    }


    object DriveVideoCacheSize : SettingsKey<Long>() {
        override val defaultValue: Long
            get() = 1000 * 1024 * 1024

    }

    object UserLeaveHintActionKey : SettingsKey<UserLeaveHintAction>() {
        override val defaultValue: UserLeaveHintAction
            get() = UserLeaveHintAction.PIP

    }

    object GlobalSubtitlesDisplaySettings : SettingsKey<SubtitlesDisplaySettings>() {
        override val defaultValue: SubtitlesDisplaySettings
            get() = SubtitlesDisplaySettings(
                textSize = 16f, textColor = "#FFFFFF", backgroundColor = "#000000"
            )
    }

    object GlobalOptionalPreservableVideoMetaData : SettingsKey<GlobalVideoMetadata>() {
        override val defaultValue: GlobalVideoMetadata
            get() = GlobalVideoMetadata(
                playBackSpeed = 1f,
                playBackPitch = 1f,
                audioEqualizerSettings = AudioEqualizerSettings(
                    audioPresetMode = 0,
                    audioReverbMode = 0,
                    audioBassBoost = 0,
                    customAudioBandValues = emptyMap(),
                    audioLoudnessBoost = 0
                ),
                visualEffectsSettings = VisualEffectsSettings(
                    enabled = false,
                    grayScaleEnabled = false,
                    videoTotalRotation = 0f,
                    invertedColorsEnabled = false,
                    contrast = 0f,
                    hue = 0f,
                    saturation = 0f
                )
            )

    }

}