package com.asr.smartaplayer.domain.useCases.preferences

import android.util.Log
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.repositoryContracts.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UpdateAppSettingsForKey @Inject constructor(private val settingsRepository: SettingsRepository) {

    suspend operator fun <T> invoke(settingsKey: SettingsKey<T>, value: T): Boolean {
        return try {
            withContext(Dispatchers.IO) {
                settingsRepository.updateDataStoreValueForKey(
                    key = settingsKey.key,
                    value = value
                )
            }
        } catch (e: Exception) {
            Log.e("PreferencesUseCase", "invoke: UpdateAppSettingsForKey ${e.message}")
            false
        }
    }

}