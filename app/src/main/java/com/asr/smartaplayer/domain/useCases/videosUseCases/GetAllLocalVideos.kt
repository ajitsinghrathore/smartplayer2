package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.useCases.preferences.GetAppSettingsForKey
import com.asr.smartaplayer.domain.utils.PaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class GetAllLocalVideos @Inject constructor(
    private val allVideosRepository: AllVideosRepository,
    private val getAppSettingsForKey: GetAppSettingsForKey
) {
    suspend operator fun invoke(
        paginationState: PaginationState,
        hasAccessToSeePrivateVideos: Boolean,
        searchText: String
    ): UseCaseTaskResult<List<LocalVideoItem>> = withContext(Dispatchers.IO) {
        try {
            paginationState.isLoading = true
            val sortPreference = getAppSettingsForKey(SettingsKey.VideosSortingPreference)

            var privateCount = 0
            var batchCount = 0
            val videosList = allVideosRepository.getAllLocalVideos(
                    sortPreference,
                    paginationState,
                    includePrivateVideos = hasAccessToSeePrivateVideos,
                searchText = searchText
                )
            videosList.map { video ->
                batchCount += 1
                if (video.isPrivate) {
                    privateCount += 1
                }
            }

            paginationState.paginationCompleted = batchCount < paginationState.pageLoadSize
            paginationState.alreadyLoadedItems += batchCount
            paginationState.privateVideosCount += privateCount
            paginationState.isLoading = false

            UseCaseTaskResult.Success(videosList)
        } catch (e: Exception) {
            paginationState.paginationCompleted = true
            paginationState.isLoading = false
            UseCaseTaskResult.Failure(message = e.message.toString())
        }
    }


}
