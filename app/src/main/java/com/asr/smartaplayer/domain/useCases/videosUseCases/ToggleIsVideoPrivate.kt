package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ToggleIsVideoPrivate @Inject constructor(
    private val allVideosRepository: AllVideosRepository
) {
    suspend operator fun invoke(
        videoItem: LocalVideoItem,
        isPrivate: Boolean
    ): Flow<UseCaseTaskResult<LocalVideoItem>> =
        allVideosRepository.toggleIsVideoPrivate(videoItem, isPrivate).flowOn(Dispatchers.IO)


}