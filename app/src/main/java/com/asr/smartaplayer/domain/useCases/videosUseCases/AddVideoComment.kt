package com.asr.smartaplayer.domain.useCases.videosUseCases

import android.util.Log
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AddVideoComment @Inject constructor(private val repository: AllVideosRepository) {


    suspend operator fun invoke(
        videoItem: BaseVideoItem,
        videoComment: VideoComment
    ): Boolean = withContext(Dispatchers.IO) {
        try{
            repository.addCommentInVideo(video = videoItem, comment = videoComment)
        }catch (e: Exception){
            Log.d("ADD VIDEO COMMENT", "invoke: ${e.message}")
            false
        }
    }
}