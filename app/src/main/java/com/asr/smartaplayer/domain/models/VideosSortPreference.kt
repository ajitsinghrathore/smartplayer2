package com.asr.smartaplayer.domain.models

data class VideosSortPreference(
    val orderVideosIn: OrderVideosIn,
    val orderVideosBy: OrderVideosBy,
)

enum class OrderVideosBy {
    NAME,
    SIZE,
    LAST_MODIFIED
}

enum class OrderVideosIn {
    ASCENDING,
    DESCENDING
}
