package com.asr.smartaplayer.domain.utils

data class KeyBasedPaginationState(
    var isLoading: Boolean= false,
    var paginationCompleted: Boolean = false,
    var loadPageSize: Int = 30,
    var lastLoadedPageKey: String?  = null
)