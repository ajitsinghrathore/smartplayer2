package com.asr.smartaplayer.domain.utils

data class PaginationState(
    var isLoading: Boolean = false,
    var alreadyLoadedItems: Int = 0,
    var paginationCompleted: Boolean = false,
    var privateVideosCount: Int = 0,
    val pageLoadSize: Int = 30
)


data class GenericOffsetBasedPaginationState(
    var isLoading: Boolean = false,
    var alreadyLoadedItems: Int = 0,
    var paginationCompleted: Boolean = false,
    val pageLoadSize: Int = 30
)