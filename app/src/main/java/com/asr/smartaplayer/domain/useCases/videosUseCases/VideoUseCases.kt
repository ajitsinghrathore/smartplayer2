package com.asr.smartaplayer.domain.useCases.videosUseCases

import javax.inject.Inject

data class VideoUseCases @Inject constructor(
    val toggleIsVideoPrivate: ToggleIsVideoPrivate,
    val  getAllLocalVideos: GetAllLocalVideos,
    val getVideoMetaData: GetVideoMetaData,
    val updateLocalVideoMetaData: UpdateVideoMetaData,
    val getAllVideoComments: GetAllVideoComments,
    val addVideoComment: AddVideoComment,
    val deleteCommentInVideo: DeleteCommentInVideo,
    val getAllDriveVideos: GetAllDriveVideos,
    val getVideoAccessToken: GetVideoAccessToken,
    val getAllSubtitles: GetAllSubtitles
)
