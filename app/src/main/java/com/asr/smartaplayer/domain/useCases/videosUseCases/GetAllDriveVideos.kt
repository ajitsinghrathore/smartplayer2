package com.asr.smartaplayer.domain.useCases.videosUseCases

import android.util.Log
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.useCases.preferences.GetAppSettingsForKey
import com.asr.smartaplayer.domain.utils.KeyBasedPaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetAllDriveVideos @Inject constructor(
    private val repository: AllVideosRepository,
    private val getAppSettingsForKey: GetAppSettingsForKey
) {

    suspend operator fun invoke(
        userEmail: String,
        paginationState: KeyBasedPaginationState,
    ): UseCaseTaskResult<List<DriveVideoItem>>  = withContext(Dispatchers.IO){
        try {
            paginationState.isLoading = true
            val sortPreference = getAppSettingsForKey(settingsKey = SettingsKey.VideosSortingPreference)
            val response = repository.getAllVideosForUserEmail(
                userEmail,
                sortPreference,
                paginationState
            )
            paginationState.isLoading = false
            paginationState.paginationCompleted = response.first == null
            paginationState.lastLoadedPageKey = response.first

            UseCaseTaskResult.Success(response.second)
        }catch (e: Exception){
            Log.d("DRIVE", "invoke: ${e.message}")
            paginationState.isLoading = false
            paginationState.paginationCompleted = true
            paginationState.lastLoadedPageKey = null
            UseCaseTaskResult.Failure(message = e.message.toString())
        }
    }



}