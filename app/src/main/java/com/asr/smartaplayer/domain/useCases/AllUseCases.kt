package com.asr.smartaplayer.domain.useCases

import com.asr.smartaplayer.domain.useCases.preferences.PreferencesUseCases
import com.asr.smartaplayer.domain.useCases.videosUseCases.AddVideoComment
import com.asr.smartaplayer.domain.useCases.videosUseCases.DeleteCommentInVideo
import com.asr.smartaplayer.domain.useCases.videosUseCases.GetAllDriveVideos
import com.asr.smartaplayer.domain.useCases.videosUseCases.GetAllLocalVideos
import com.asr.smartaplayer.domain.useCases.videosUseCases.GetAllVideoComments
import com.asr.smartaplayer.domain.useCases.videosUseCases.GetVideoAccessToken
import com.asr.smartaplayer.domain.useCases.videosUseCases.GetVideoMetaData
import com.asr.smartaplayer.domain.useCases.videosUseCases.ToggleIsVideoPrivate
import com.asr.smartaplayer.domain.useCases.videosUseCases.UpdateVideoMetaData
import com.asr.smartaplayer.domain.useCases.videosUseCases.VideoUseCases
import javax.inject.Inject

data class AllUseCases @Inject constructor(
    val videoUseCases: VideoUseCases,
    val preferencesUseCases: PreferencesUseCases
)
