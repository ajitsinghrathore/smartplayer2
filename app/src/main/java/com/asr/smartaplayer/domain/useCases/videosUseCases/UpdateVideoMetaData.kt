package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UpdateVideoMetaData @Inject constructor(private val allVideosRepository: AllVideosRepository) {


    suspend operator fun invoke(videoMetaData: VideoMetaData, videoItem: BaseVideoItem): Boolean =
        withContext(Dispatchers.IO) {
            allVideosRepository.updateVideoItemMetaData(videoMetaData, videoItem)
        }
}