package com.asr.smartaplayer.domain.models

import android.net.Uri

data class Subtitle(
    val uri: Uri,
    val displayName: String,
    val mimeType: String
)


