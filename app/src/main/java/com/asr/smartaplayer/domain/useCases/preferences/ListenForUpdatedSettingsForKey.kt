package com.asr.smartaplayer.domain.useCases.preferences

import android.util.Log
import com.asr.smartaplayer.domain.models.SettingsKey
import com.asr.smartaplayer.domain.repositoryContracts.SettingsRepository
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ListenForUpdatedSettingsForKey @Inject constructor(val settingsRepository: SettingsRepository) {


    inline operator  fun <reified T: Any> invoke(settingsKey: SettingsKey<T>): Flow<T> {
        return flow {
            val type = object: TypeToken<T>() {}.type
            settingsRepository.getDataStoreValueForKeyAsFlow<T>(settingsKey.key, classType = type).collect {
                if (it == null) {
                    emit(settingsKey.defaultValue)
                } else {
                    emit(it)
                }
            }

        }
    }

}