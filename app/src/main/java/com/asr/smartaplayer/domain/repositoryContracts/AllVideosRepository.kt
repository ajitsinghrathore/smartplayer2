package com.asr.smartaplayer.domain.repositoryContracts

import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.models.VideosSortPreference
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState
import com.asr.smartaplayer.domain.utils.KeyBasedPaginationState
import com.asr.smartaplayer.domain.utils.PaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.flow.Flow

interface AllVideosRepository {
    suspend fun getAllLocalVideos(
        sortPreference: VideosSortPreference,
        paginationState: PaginationState,
        includePrivateVideos: Boolean,
        searchText: String
    ): List<LocalVideoItem>


    suspend fun getAllSubtitles(
        paginationState: GenericOffsetBasedPaginationState,
        searchText: String
    ): List<Subtitle>



    suspend fun getAllVideosForUserEmail(
        userEmail: String,
        sortPreference: VideosSortPreference,
        paginationState: KeyBasedPaginationState,
    ): Pair<String?, List<DriveVideoItem>>

    suspend fun getVideoAccessToken(videoItem: DriveVideoItem): String?
    suspend fun toggleIsVideoPrivate(videoItem: LocalVideoItem, makePrivate: Boolean): Flow<UseCaseTaskResult<LocalVideoItem>>
    suspend fun getVideoItemMetaData(videoItem: BaseVideoItem): VideoMetaData?
    suspend fun updateVideoItemMetaData(videoMetaData: VideoMetaData, videoItem: BaseVideoItem): Boolean
    suspend fun getAllCommentsForVideo(videoItem: BaseVideoItem): List<VideoComment>
    suspend fun addCommentInVideo(video: BaseVideoItem, comment: VideoComment): Boolean
    suspend fun deleteCommentInVideo(video: BaseVideoItem, comment: VideoComment): Boolean
}