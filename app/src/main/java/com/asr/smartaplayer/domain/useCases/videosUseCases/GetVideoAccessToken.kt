package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetVideoAccessToken @Inject constructor(private val repository: AllVideosRepository) {


    suspend operator fun invoke(videoItem: DriveVideoItem): String? =
        withContext(Dispatchers.IO) {
            repository.getVideoAccessToken(videoItem)
        }

}