package com.asr.smartaplayer.domain.useCases.videosUseCases

import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState
import com.asr.smartaplayer.domain.utils.PaginationState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetAllSubtitles @Inject constructor(private val videosRepository: AllVideosRepository) {


    suspend operator fun invoke(paginationState: GenericOffsetBasedPaginationState, searchText: String): List<Subtitle> =
        withContext(Dispatchers.IO) {
            paginationState.isLoading = true
            val subtitles = videosRepository.getAllSubtitles(paginationState, searchText)

            paginationState.paginationCompleted = subtitles.size < paginationState.pageLoadSize
            paginationState.alreadyLoadedItems += subtitles.size
            paginationState.isLoading = false
            subtitles
        }

}