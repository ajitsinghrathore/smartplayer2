package com.asr.smartaplayer.domain.models

data class SubtitlesDisplaySettings(
    val textSize: Float,
    val backgroundColor: String,
    val textColor: String
)
