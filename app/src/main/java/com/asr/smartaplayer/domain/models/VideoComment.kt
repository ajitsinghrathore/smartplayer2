package com.asr.smartaplayer.domain.models

data class VideoComment(
    val playBackPosition: Long,
    val comment: String,
    val thumbNail: String? = null
)
