package com.asr.smartaplayer.domain.useCases.preferences

import javax.inject.Inject

data class PreferencesUseCases @Inject constructor(
    val updateAppSettingsForKey: UpdateAppSettingsForKey,
    val listenForUpdatedSettingsForKey: ListenForUpdatedSettingsForKey,
    val getAppSettingsForKey: GetAppSettingsForKey
)
