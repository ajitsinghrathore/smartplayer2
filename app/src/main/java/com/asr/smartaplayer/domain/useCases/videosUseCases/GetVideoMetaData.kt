package com.asr.smartaplayer.domain.useCases.videosUseCases

import android.util.Log
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetVideoMetaData @Inject constructor(private val allVideosRepository: AllVideosRepository) {


    suspend operator fun invoke(videoItem: BaseVideoItem): VideoMetaData? =
        withContext(Dispatchers.IO) {
            val metaData = allVideosRepository.getVideoItemMetaData(videoItem = videoItem)
            Log.d("METADATA", "invoke: $metaData")
            metaData
        }


}