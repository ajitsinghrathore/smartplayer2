package com.asr.smartaplayer.domain.models



enum class UserLeaveHintAction(val description: String, val displayName: String) {
    PAUSE("Pause Video Playback: Videos will be paused when you leave full-screen mode and resume when you return.", "Pause the video"),
    PIP("Picture-in-Picture (PiP) Mode: Watch videos in a floating window while using other apps.", "Enter Picture-in-Picture (PiP) Mode"),
    AUDIO_ONLY("Audio-Only Mode: Save bandwidth and battery by listening to the audio while the video is turned off.", "Play only audio")
}