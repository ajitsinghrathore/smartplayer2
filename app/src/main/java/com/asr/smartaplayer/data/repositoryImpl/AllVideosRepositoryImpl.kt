package com.asr.smartaplayer.data.repositoryImpl

import android.content.Context
import android.util.Log
import com.asr.smartaplayer.data.SmartPlayerDatabase
import com.asr.smartaplayer.data.dataSources.MediaStoreSubtitleDataSource
import com.asr.smartaplayer.data.dataSources.MediaStoreVideoDataSource
import com.asr.smartaplayer.data.dataSources.PrivateVideosDataSource
import com.asr.smartaplayer.data.dataSources.RemoteVideosDataSource
import com.asr.smartaplayer.data.dataSources.mergeMediaStoreAndPrivateVideos
import com.asr.smartaplayer.data.models.DataLayerVideoComment
import com.asr.smartaplayer.data.models.DataLayerVideoMetaData
import com.asr.smartaplayer.data.models.MediaStoreLocalVideoItem
import com.asr.smartaplayer.data.models.PrivateLocalVideoItem
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoComment
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.models.VideosSortPreference
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState
import com.asr.smartaplayer.domain.utils.KeyBasedPaginationState
import com.asr.smartaplayer.domain.utils.PaginationState
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AllVideosRepositoryImpl @Inject constructor(
    @ApplicationContext private val applicationContext: Context,
    private val smartPlayerDatabase: SmartPlayerDatabase
) : AllVideosRepository {

    override suspend fun getAllLocalVideos(
        sortPreference: VideosSortPreference,
        paginationState: PaginationState,
        includePrivateVideos: Boolean,
        searchText: String
    ): List<LocalVideoItem> {

        val mediaStoreVideos = MediaStoreVideoDataSource(applicationContext.contentResolver).fetch(
            searchText = searchText,
            sortPreference = sortPreference,
            offset = paginationState.alreadyLoadedItems - paginationState.privateVideosCount,
            pageSize = paginationState.pageLoadSize
        )
        var privateVideos = emptyList<PrivateLocalVideoItem>()
        if (includePrivateVideos) {
            privateVideos = PrivateVideosDataSource(
                smartPlayerDatabase.privateVideoDao(),
                smartPlayerDatabase.transactionalPrivateVideosDao()
            ).fetch(
                limit = paginationState.pageLoadSize,
                offset = paginationState.privateVideosCount,
                videosSortPreference = sortPreference
            )
        }


        val finalCombinedVideos = mergeMediaStoreAndPrivateVideos(
            mediaStoreVideos = mediaStoreVideos,
            privateVideos = privateVideos,
            videosSortPreference = sortPreference,
            size = paginationState.pageLoadSize
        )

        return finalCombinedVideos.map {
            it.toLocalVideoItem(applicationContext)
        }

    }

    override suspend fun getAllSubtitles(paginationState: GenericOffsetBasedPaginationState, searchText: String): List<Subtitle> {
        return MediaStoreSubtitleDataSource(
            contentResolver = applicationContext.contentResolver,
            queryString = searchText,
            paginationState = paginationState
        ).fetch()
    }

    override suspend fun getAllVideosForUserEmail(
        userEmail: String,
        sortPreference: VideosSortPreference,
        paginationState: KeyBasedPaginationState
    ): Pair<String?, List<DriveVideoItem>> {

        val dataSource = RemoteVideosDataSource(userEmail = userEmail, context = applicationContext)

        return dataSource.fetch(
            videosSortPreference = sortPreference,
            lastPageKey = paginationState.lastLoadedPageKey,
            limit = paginationState.loadPageSize
        )
    }

    override suspend fun getVideoAccessToken(videoItem: DriveVideoItem): String {
        return RemoteVideosDataSource(
            userEmail = videoItem.userDriveEmail,
            context = applicationContext
        ).getAccessToken()
    }

    override suspend fun toggleIsVideoPrivate(
        videoItem: LocalVideoItem,
        makePrivate: Boolean
    ): Flow<UseCaseTaskResult<LocalVideoItem>> = flow {
        val privateVideoDataSource = PrivateVideosDataSource(
            smartPlayerDatabase.privateVideoDao(),
            smartPlayerDatabase.transactionalPrivateVideosDao()
        )
        if (makePrivate) {
            if (videoItem.isPrivate) {
                emit(UseCaseTaskResult.Success(videoItem))
                return@flow
            }
            val currentDetails =
                MediaStoreVideoDataSource(applicationContext.contentResolver).getVideoDetails(
                    videoId = MediaStoreLocalVideoItem.getVideoIdFromUri(uri = videoItem.videoUrl)
                )
            if (currentDetails == null) {
                emit(UseCaseTaskResult.Success(videoItem))
                return@flow
            }

            privateVideoDataSource.makeMediaStoreVideoPrivate(
                context = applicationContext,
                videoItem = currentDetails
            ).collect {
                when (it) {
                    is UseCaseTaskResult.Failure -> {}
                    else -> {
                        emit(it)
                    }
                }
            }

            Log.d("DRIVE*", "toggleIsVideoPrivate: completed flow in repository ")


        } else {
            if (!videoItem.isPrivate) {
                emit(UseCaseTaskResult.Success(videoItem))
                return@flow
            }
            val videoDetails =
                privateVideoDataSource.getVideoDetails(videoId = videoItem.videoUrl)
            if (videoDetails == null) {
                emit(UseCaseTaskResult.Success(videoItem))
                return@flow
            }
            privateVideoDataSource.makePrivateVideoPublic(
                context = applicationContext,
                videoItem = videoDetails
            ).collect {
                when (it) {
                    is UseCaseTaskResult.Failure -> {}
                    else -> emit(it)
                }
            }

            Log.d("DRIVE*", "toggleIsVideoPrivate: completed in repository")
        }
    }


    private fun getVideoIdForVideo(videoItem: BaseVideoItem): String? {
        var videoId: String? = null
        if (videoItem is DriveVideoItem) {
            videoId = videoItem.videoId
        } else if (videoItem is LocalVideoItem) {
            videoId = if (videoItem.isPrivate) {
                PrivateLocalVideoItem.getVideoIdFromUri(uri = videoItem.videoUrl)
            } else {
                MediaStoreLocalVideoItem.getVideoIdFromUri(uri = videoItem.videoUrl)
            }
        }
        return videoId
    }

    override suspend fun getVideoItemMetaData(videoItem: BaseVideoItem): VideoMetaData? {
        val videoId = getVideoIdForVideo(videoItem) ?: return null

        val currentMetaData =
            smartPlayerDatabase.localVideoMetadataDaoDao().getVideoMetadata(videoId = videoId)
                ?: return null
        return  currentMetaData.videoMetaData

    }

    override suspend fun updateVideoItemMetaData(
        videoMetaData: VideoMetaData,
        videoItem: BaseVideoItem
    ): Boolean {
        val videoId = getVideoIdForVideo(videoItem) ?: return false
        val metaData =  DataLayerVideoMetaData(videoId = videoId, videoMetaData = videoMetaData)

        smartPlayerDatabase.localVideoMetadataDaoDao().upsertVideoMetaData(
            dataLayerVideoMetaData = metaData
        )
        return true


    }

    override suspend fun getAllCommentsForVideo(videoItem: BaseVideoItem): List<VideoComment> {
        val videoId = getVideoIdForVideo(videoItem) ?: return emptyList()

        return smartPlayerDatabase.dataLayerVideoCommentsDao().getVideoComments(videoId).map {
            VideoComment(
                playBackPosition = it.position,
                comment = it.comment,
                thumbNail = it.previewFrameUri
            )
        }
    }

    override suspend fun addCommentInVideo(video: BaseVideoItem, comment: VideoComment): Boolean {
        val videoId = getVideoIdForVideo(video) ?: return false
        smartPlayerDatabase.dataLayerVideoCommentsDao().addVideoComment(
            DataLayerVideoComment(
                videoId = videoId,
                position = comment.playBackPosition,
                comment = comment.comment,
                previewFrameUri = comment.thumbNail
            )
        )
        return true
    }

    override suspend fun deleteCommentInVideo(
        video: BaseVideoItem,
        comment: VideoComment
    ): Boolean {
        val videoId = getVideoIdForVideo(video) ?: return false
        smartPlayerDatabase.dataLayerVideoCommentsDao()
            .deleteCommentInVideo(videoId = videoId, position = comment.playBackPosition)
        return true

    }


}