package com.asr.smartaplayer.data.models

import androidx.room.Entity

@Entity(primaryKeys = ["videoId", "position"])
data class DataLayerVideoComment(
    val videoId: String,
    val comment: String,
    val position: Long,
    val previewFrameUri: String?
)

