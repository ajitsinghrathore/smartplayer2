package com.asr.smartaplayer.data.dataSources

import android.content.Context
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import com.asr.smartaplayer.SmartPlayerApplication
import com.asr.smartaplayer.data.humanReadableDuration
import com.asr.smartaplayer.data.humanReadableSize
import com.asr.smartaplayer.domain.models.DriveVideoItem
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.VideosSortPreference
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow


class RemoteVideosDataSource(
    private val userEmail: String,
    private val context: Context
) {


    private var driveService: Drive
    private val googleAccountCredential: GoogleAccountCredential =
        GoogleAccountCredential.usingOAuth2(
            context,
            listOf(
                DriveScopes.DRIVE,
                DriveScopes.DRIVE_READONLY
            )
        )


    fun getAccessToken(): String{
        return try{
            googleAccountCredential.token
        }catch (e: Exception){
            Log.d("DRIVE", "getAccessToken: ${e.message} ${googleAccountCredential.selectedAccountName}")
            ""
        }
    }

    init {
        googleAccountCredential.selectedAccountName = userEmail
        Log.d("DRIVE", ":  setting data source for email $userEmail   ${googleAccountCredential.selectedAccountName}")
        driveService = Drive.Builder(
            AndroidHttp.newCompatibleTransport(),
            JacksonFactory.getDefaultInstance(),
            googleAccountCredential
        ).setApplicationName("Smart Player").build()
    }


    private fun googleSignIn(): kotlinx.coroutines.flow.Flow<String?> = callbackFlow {
        val signInBuilder = GoogleSignInOptions
            .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .setAccountName(userEmail)
            .requestScopes(Scope(DriveScopes.DRIVE), Scope(DriveScopes.DRIVE_READONLY))
            .build()
        val signInClient = GoogleSignIn.getClient(context, signInBuilder)
        val application = context as SmartPlayerApplication
        fun callbackDef(result: ActivityResult?) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(result?.data)
            try{
                val account = task.result
                if (account != null){
                    Log.d("DRIVE*", "callbackDef: Successful login ")
                    googleAccountCredential.selectedAccountName = userEmail
                    trySend("success")
                    close()
                }else{
                    Toast.makeText(application.applicationContext, "We need your drive files download access to play and list down all your videos", Toast.LENGTH_LONG).show()
                    Log.d("DRIVE*", "callbackDef:  retriving account nulll")
                    trySend(null)
                    close()
                }
            }catch (e: Exception){
                Toast.makeText(application.applicationContext, "We need your drive files download access to play and list down all your videos", Toast.LENGTH_LONG).show()
                Log.d("DRIVE*", "callbackDef: ERROR in retriving account ${e.message}")
                trySend(null)
                close()
            }
        }

        val callback = ::callbackDef
        application.executeIntentForResult(signInClient.signInIntent, callback)
        awaitClose{

        }
    }

    suspend fun fetch(
        videosSortPreference: VideosSortPreference,
        lastPageKey: String?,
        limit: Int,
        retryCount : Int = 0
    ): Pair<String?, List<DriveVideoItem>> {
        var token = getAccessToken()
        Log.d("DRIVE*", "fetch: token $token")
        if (token.isBlank()){
            googleSignIn().collect{
                token = it ?: ""
                Log.d("DRIVE*", "fetch: token in sign in collect $token ")
            }
        }

        Log.d("DRIVE*", "fetch: Token aftter sign in collect $token")

        if (token.isBlank()){
            return Pair(null, emptyList())
        }

        val sortIn = when (videosSortPreference.orderVideosIn) {
            OrderVideosIn.ASCENDING -> "asc"
            OrderVideosIn.DESCENDING -> "desc"
        }

        val orderBy = when (videosSortPreference.orderVideosBy) {
            OrderVideosBy.NAME -> "name"
            OrderVideosBy.SIZE -> "quotaBytesUsed"
            OrderVideosBy.LAST_MODIFIED -> "modifiedTime"
        }

        val orderByString = "$orderBy $sortIn"


        try {
            var fileListQuery = driveService.Files().list()
                .setQ("mimeType contains 'video/'")
                .setFields("nextPageToken, files(id, name, mimeType, thumbnailLink, webContentLink, size, videoMediaMetadata, modifiedTime, capabilities)")
                .setIncludeTeamDriveItems(true)
                .setSupportsTeamDrives(true)
                .setOrderBy(orderByString)
                .setPageSize(limit)

            if (lastPageKey != null) {
                fileListQuery = fileListQuery.setPageToken(lastPageKey)
            }

            val fileList = fileListQuery.execute()
            var videoFiles = fileList.files

            videoFiles = videoFiles.filter {
                it.capabilities?.canDownload == true
            }

            val updatedFiles = videoFiles.map {
                Log.d("DRIVE", "getAllVideos:  ${it.capabilities?.canDownload} $it")
                val url = Uri.parse(
                    "https://www.googleapis.com/drive/v3/files/${it.id}?supportsAllDrives=True&alt=media"
                )
                val duration = try{
                    humanReadableDuration(it.videoMediaMetadata.durationMillis)
                }catch (e: Exception){
                    ""
                }

                DriveVideoItem(
                    videoDuration = duration,
                    videoId = it.id,
                    videoUrl = url.toString(),
                    videoName = it.name,
                    videoSize = humanReadableSize(context, it.getSize()),
                    videoThumbNailLink = it.thumbnailLink?:"",
                    userDriveEmail = userEmail
                )
            }

            return Pair(fileList.nextPageToken, updatedFiles)

        } catch (e: UserRecoverableAuthIOException) {
            if (retryCount < 1) {
                Log.d("DRIVE*", "fetch: retrying with new token")
                return fetch(videosSortPreference, lastPageKey, limit, retryCount = retryCount + 1)
            }
            return Pair(null, emptyList())
        }


    }


}