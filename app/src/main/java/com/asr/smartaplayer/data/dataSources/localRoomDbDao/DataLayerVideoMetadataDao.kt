package com.asr.smartaplayer.data.dataSources.localRoomDbDao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.asr.smartaplayer.data.models.DataLayerVideoMetaData


@Dao
interface DataLayerVideoMetadataDao {

    @Query("select * from DataLayerVideoMetaData where videoId = :videoId")
    suspend fun getVideoMetadata(videoId: String): DataLayerVideoMetaData?

    @Query("UPDATE DataLayerVideoMetaData set videoId = :newId where videoId = :oldId")
    suspend fun updateVideoIdForVideo(oldId: String, newId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertVideoMetaData(dataLayerVideoMetaData: DataLayerVideoMetaData)

}