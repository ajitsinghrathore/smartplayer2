package com.asr.smartaplayer.data.dataSources

import android.content.ContentResolver
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.asr.smartaplayer.data.models.MediaStoreLocalVideoItem
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.VideosSortPreference


class MediaStoreVideoDataSource(private val contentResolver: ContentResolver) {

    private val collection = MediaStore.Video.Media.EXTERNAL_CONTENT_URI

    private val projection = arrayOf(
        MediaStore.Video.Media._ID,
        MediaStore.Video.Media.DISPLAY_NAME,
        MediaStore.Video.Media.DURATION,
        MediaStore.Video.Media.SIZE,
        MediaStore.Video.Media.DATE_ADDED
    )


    fun fetch(
        sortPreference: VideosSortPreference,
        offset: Int,
        pageSize: Int,
        searchText: String
    ): List<MediaStoreLocalVideoItem> {

        val videos: MutableList<MediaStoreLocalVideoItem> = mutableListOf()

        val query = contentResolver.query(
            collection,
            projection,
            getBundleForQueryArgs(
                sortPreference, pageSize, offset, searchText
            ),
            null
        )


        query?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
            val nameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME)
            val durationColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)
            val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE)
            val dateModifiedColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED)

            while (cursor.moveToNext()) {
                val id = cursor.getLong(idColumn)
                val name = cursor.getString(nameColumn)
                val duration = cursor.getInt(durationColumn)
                val size = cursor.getInt(sizeColumn)
                val dateModified = cursor.getLong(dateModifiedColumn)

                videos += MediaStoreLocalVideoItem(
                    videoId = id.toString(),
                    name = name,
                    duration = duration.toLong(),
                    size = size.toLong(),
                    lastUpdated = dateModified
                )

                Log.d("TAG___", "fetch: ${videos.last()}")
            }

        }

        return videos


    }

    private fun getBundleForQueryArgs(
        sortPreference: VideosSortPreference,
        size: Int,
        offset: Int,
        searchText: String
    ): Bundle {

        val bundle = Bundle()

        val sortOrder = when (sortPreference.orderVideosBy) {
            OrderVideosBy.NAME -> MediaStore.Video.Media.DISPLAY_NAME
            OrderVideosBy.SIZE -> MediaStore.Video.Media.SIZE
            OrderVideosBy.LAST_MODIFIED -> MediaStore.Video.Media.DATE_MODIFIED
        }

        bundle.putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS, arrayOf(sortOrder))

        val sortDirection = when (sortPreference.orderVideosIn) {
            OrderVideosIn.ASCENDING -> ContentResolver.QUERY_SORT_DIRECTION_ASCENDING
            OrderVideosIn.DESCENDING -> ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
        }

        bundle.putInt(ContentResolver.QUERY_ARG_SORT_DIRECTION, sortDirection)

        bundle.putInt(ContentResolver.QUERY_ARG_LIMIT, size)
        bundle.putInt(ContentResolver.QUERY_ARG_OFFSET, offset)

        if (searchText.isNotBlank()) {

            val selection = "LOWER(${MediaStore.Video.Media.DISPLAY_NAME}) LIKE ?"

            val selectionArgs = arrayOf(
                "%${searchText.lowercase()}%"
            )

            bundle.putString(ContentResolver.QUERY_ARG_SQL_SELECTION, selection)
            bundle.putStringArray(ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS, selectionArgs)
        }
        return bundle
    }


    fun getVideoDetails(videoId: String): MediaStoreLocalVideoItem? {
        val selection = MediaStore.Video.Media._ID + "=?"
        val selectionArgs = arrayOf(videoId)
        contentResolver.query(
            collection,
            projection,
            selection,
            selectionArgs,
            null
        )?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
            val nameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME)
            val durationColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)
            val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE)
            val dateModifiedColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED)

            while (cursor.moveToNext()) {
                val id = cursor.getLong(idColumn)
                val name = cursor.getString(nameColumn)
                val duration = cursor.getInt(durationColumn)
                val size = cursor.getInt(sizeColumn)
                val dateModified = cursor.getLong(dateModifiedColumn)
                return MediaStoreLocalVideoItem(
                    videoId = id.toString(),
                    name = name,
                    duration = duration.toLong(),
                    size = size.toLong(),
                    lastUpdated = dateModified
                )
            }

        }
        return null
    }


}
