package com.asr.smartaplayer.data.dataSources

import android.content.ContentResolver
import android.content.ContentUris
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.utils.GenericOffsetBasedPaginationState

class MediaStoreSubtitleDataSource(
    private val contentResolver: ContentResolver,
    private val queryString: String,
    private val paginationState: GenericOffsetBasedPaginationState
) {
    private val contentUri: Uri = MediaStore.Files.getContentUri("external")


    private val projection = arrayOf(
        MediaStore.Files.FileColumns._ID,
        MediaStore.Files.FileColumns.DISPLAY_NAME,
        MediaStore.Files.FileColumns.MIME_TYPE
    )

    fun fetch(): List<Subtitle>{

        val query: Cursor? = contentResolver.query(
            contentUri, projection, getBundleForQueryArgs(), null
        )

        val subtitles = mutableListOf<Subtitle>()
        query?.use { cursor ->
            while (cursor.moveToNext()) {
                // Get the _ID column index
                val idIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID)
                val id = cursor.getLong(idIndex)

                // Get the DISPLAY_NAME column index
                val displayNameIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME)
                val displayName = cursor.getString(displayNameIndex)

                // Get the MIME_TYPE column index
                val mimeTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MIME_TYPE)
                val mimeType = cursor.getString(mimeTypeIndex)
                subtitles.add(
                    Subtitle(uri = ContentUris.withAppendedId(contentUri, id), displayName = displayName, mimeType = mimeType)
                )
            }
        }

        return subtitles
    }



    private fun getBundleForQueryArgs(
    ): Bundle {

        val bundle = Bundle()

        bundle.putInt(ContentResolver.QUERY_ARG_LIMIT, paginationState.pageLoadSize)
        bundle.putInt(ContentResolver.QUERY_ARG_OFFSET, paginationState.alreadyLoadedItems)

        if (queryString.isNotBlank()) {
            val selection = "(${MediaStore.Files.FileColumns.MIME_TYPE}=? OR ${MediaStore.Files.FileColumns.MIME_TYPE}=?) " +
                    "AND LOWER(${MediaStore.Files.FileColumns.DISPLAY_NAME}) LIKE ?"

            val selectionArgs = arrayOf(
                "text/vtt",
                "application/x-subrip",
                "%${queryString.lowercase()}%"
            )

            bundle.putString(ContentResolver.QUERY_ARG_SQL_SELECTION, selection)
            bundle.putStringArray(ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS, selectionArgs)
        } else {
            val selection = "(${MediaStore.Files.FileColumns.MIME_TYPE}=? OR ${MediaStore.Files.FileColumns.MIME_TYPE}=?)"
            val selectionArgs = arrayOf(
                "text/vtt",
                "application/x-subrip"
            )

            bundle.putString(ContentResolver.QUERY_ARG_SQL_SELECTION, selection)
            bundle.putStringArray(ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS, selectionArgs)
        }

        return bundle
    }
}