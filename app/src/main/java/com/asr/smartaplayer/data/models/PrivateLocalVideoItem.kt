package com.asr.smartaplayer.data.models

import android.content.Context
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.asr.smartaplayer.data.humanReadableDuration
import com.asr.smartaplayer.data.humanReadableSize
import com.asr.smartaplayer.domain.models.LocalVideoItem


@Entity
data class PrivateLocalVideoItem(
    override val url: String,
    override val name: String,
    override val duration: Long,
    override val size: Long,
    override val lastUpdated: Long,
    @PrimaryKey
    override val videoId: String = url
):DataLayerLocalVideoItem(){

    override fun toLocalVideoItem(context: Context): LocalVideoItem {
        return LocalVideoItem(
            videoUrl = this.url,
            videoName = this.name,
            videoDuration = humanReadableDuration(this.duration),
            videoSize = humanReadableSize(context, this.size),
            isPrivate = true
        )
    }


    companion object{
        fun getVideoIdFromUri(uri: String): String {
            return uri
        }
        fun getVideoUriFromId(videoId: String): String {
            return  videoId
        }
    }





}