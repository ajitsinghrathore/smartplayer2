package com.asr.smartaplayer.data.di


import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import com.asr.smartaplayer.SmartPlayerApplication
import com.asr.smartaplayer.data.SmartPlayerDatabase
import com.asr.smartaplayer.data.repositoryImpl.AllVideosRepositoryImpl
import com.asr.smartaplayer.data.repositoryImpl.SettingRepositoryImpl
import com.asr.smartaplayer.domain.repositoryContracts.AllVideosRepository
import com.asr.smartaplayer.domain.repositoryContracts.SettingsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.Executors
import javax.inject.Singleton





class MyCallback: RoomDatabase.QueryCallback{
    override fun onQuery(sqlQuery: String, bindArgs: List<Any?>) {
        Log.d("TAG____", "onQuery: $sqlQuery  $bindArgs")
    }

}



@Module
@InstallIn(SingletonComponent::class)
class DataLayerModule {

    @Provides
    @Singleton
    fun getSharedPreference(@ApplicationContext context: Context): SharedPreferences{
        return  context.getSharedPreferences("SMART_PLAYER_PREFERENCES",Context.MODE_PRIVATE)
    }


    @Provides
    @Singleton
    fun getRoomDatabase(@ApplicationContext context: Context): SmartPlayerDatabase{
        val dbBuilder =  Room.databaseBuilder(
            context,
            SmartPlayerDatabase::class.java, "SmartPlayer-DB"
        ).fallbackToDestructiveMigration()
        dbBuilder.setQueryCallback(MyCallback(), Executors.newSingleThreadExecutor())
            return dbBuilder.build()
    }

    @Provides
    @Singleton
    fun getLocalVideosRepository(localVideosRepositoryImpl: AllVideosRepositoryImpl): AllVideosRepository {
        return localVideosRepositoryImpl
    }

    @Provides
    @Singleton
    fun getSettingsRepository(settingsRepositoryImpl: SettingRepositoryImpl): SettingsRepository {
        return settingsRepositoryImpl
    }

    @Provides
    fun getApplication(@ApplicationContext context: Context): SmartPlayerApplication{
        return context as SmartPlayerApplication
    }
}