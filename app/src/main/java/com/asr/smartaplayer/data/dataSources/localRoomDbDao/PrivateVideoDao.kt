package com.asr.smartaplayer.data.dataSources.localRoomDbDao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.asr.smartaplayer.data.SmartPlayerDatabase
import com.asr.smartaplayer.data.models.MediaStoreLocalVideoItem
import com.asr.smartaplayer.data.models.PrivateLocalVideoItem


@Dao
interface PrivateVideoDao {

    @Query(
        "SELECT * FROM PrivateLocalVideoItem ORDER BY " +
                "CASE WHEN :sortIn = 'ASC' THEN lastUpdated END ASC, " +
                "CASE WHEN :sortIn = 'DESC' THEN lastUpdated END DESC " +
                "LIMIT :limit OFFSET :offset"
    )
    suspend fun getPrivateDetailsAccordingToLastUpdated(
        limit: Int,
        offset: Int,
        sortIn: String
    ): List<PrivateLocalVideoItem>?


    @Query(
        "SELECT * FROM PrivateLocalVideoItem ORDER BY " +
                "CASE WHEN :sortIn = 'ASC' THEN name END ASC, " +
                "CASE WHEN :sortIn = 'DESC' THEN name END DESC " +
                "LIMIT :limit OFFSET :offset"
    )
    suspend fun getPrivateDetailsAccordingToName(
        limit: Int,
        offset: Int,
        sortIn: String
    ): List<PrivateLocalVideoItem>?


    @Query(
        "SELECT * FROM PrivateLocalVideoItem ORDER BY " +
                "CASE WHEN :sortIn = 'ASC' THEN size END ASC, " +
                "CASE WHEN :sortIn = 'DESC' THEN size END DESC " +
                "LIMIT :limit OFFSET :offset"
    )
    suspend fun getPrivateDetailsAccordingToSize(
        limit: Int,
        offset: Int,
        sortIn: String
    ): List<PrivateLocalVideoItem>?

    @Query("DELETE FROM PrivateLocalVideoItem WHERE videoId = :videoId")
    suspend fun removeVideoDetails(videoId: String): Int

    @Query("select * from PrivateLocalVideoItem where videoId =:videoId")
    suspend fun getVideoDetails(videoId: String): PrivateLocalVideoItem?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPrivateVideo(privateVideoItem: PrivateLocalVideoItem)


}


@Dao
abstract class TransactionalPrivateVideoDao(
    private val smartPlayerDatabase: SmartPlayerDatabase
) {

    private var privateVideoDao: PrivateVideoDao = smartPlayerDatabase.privateVideoDao()
    private var dataLayerVideoCommentDao: DataLayerVideoCommentDao = smartPlayerDatabase.dataLayerVideoCommentsDao()
    private var dataLayerVideoMetadataDao: DataLayerVideoMetadataDao = smartPlayerDatabase.localVideoMetadataDaoDao()

    @Transaction
    open suspend fun makeVideoPrivate(
        oldVideoItem: MediaStoreLocalVideoItem,
        newVideoItem: PrivateLocalVideoItem
    ) {
        privateVideoDao.insertPrivateVideo(privateVideoItem = newVideoItem)
        dataLayerVideoMetadataDao.updateVideoIdForVideo(
            oldId = oldVideoItem.videoId, newId = newVideoItem.videoId
        )
        dataLayerVideoCommentDao.updateVideoIdsForComments(
            oldId = oldVideoItem.videoId, newId = newVideoItem.videoId
        )
    }


    @Transaction
    open suspend fun makePrivateVideoPublic(
        oldVideoItem: PrivateLocalVideoItem,
        newVideoItem: MediaStoreLocalVideoItem
    ){
        privateVideoDao.removeVideoDetails(videoId = oldVideoItem.videoId)
        dataLayerVideoMetadataDao.updateVideoIdForVideo(
            oldId = oldVideoItem.videoId, newId = newVideoItem.videoId
        )
        dataLayerVideoCommentDao.updateVideoIdsForComments(
            oldId = oldVideoItem.videoId, newId = newVideoItem.videoId
        )
    }


}