package com.asr.smartaplayer.data

import android.content.Context
import android.text.format.Formatter
import java.util.TimeZone


fun humanReadableDuration(duration: Long): String =
    String.format("%tT", duration - TimeZone.getDefault().rawOffset)


fun humanReadableSize(context: Context, size: Long): String = Formatter.formatFileSize(context, size)
