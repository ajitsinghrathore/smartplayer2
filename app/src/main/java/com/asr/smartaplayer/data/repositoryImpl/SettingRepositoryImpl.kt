package com.asr.smartaplayer.data.repositoryImpl

import android.content.SharedPreferences
import android.util.Log
import com.asr.smartaplayer.domain.repositoryContracts.SettingsRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import java.lang.reflect.Type
import javax.inject.Inject
import kotlin.reflect.KClass


class SettingRepositoryImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences,
): SettingsRepository{


    private val stateFlowMaps: MutableMap<String, MutableStateFlow<*>> = mutableMapOf()

    override suspend fun <DataType > getDataStoreValueForKey(
        key: String,
        classType: Type
    ): DataType? {
        val value = sharedPreferences.getString(key, null)
        return Gson().fromJson(value, classType)
    }

    override suspend fun <DataType > getDataStoreValueForKeyAsFlow(
        key: String,
        classType: Type
    ): Flow<DataType?> {
        val stateFlow: MutableStateFlow<DataType>? = stateFlowMaps[key] as MutableStateFlow<DataType>?
        return if (stateFlow == null) {
            val value = getDataStoreValueForKey<DataType>(key, classType = classType)
            val flow = MutableStateFlow(value)
            stateFlowMaps[key] = flow
            flow
        }else{
            stateFlow
        }
    }


    override suspend fun <DataType> updateDataStoreValueForKey(
        key: String,
        value: DataType
    ): Boolean {
        return try {
            sharedPreferences
                .edit()
                .putString(key, Gson().toJson(value))
                .apply()
            val stateFlow: MutableStateFlow<DataType>? = stateFlowMaps[key] as MutableStateFlow<DataType>?
            if (stateFlow != null){
                stateFlow.value = value
            }
            true
        }
        catch (e: Exception){
            false
        }
    }

}












