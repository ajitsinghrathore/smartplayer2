package com.asr.smartaplayer.data.models

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import com.asr.smartaplayer.data.humanReadableDuration
import com.asr.smartaplayer.data.humanReadableSize
import com.asr.smartaplayer.domain.models.LocalVideoItem

data class MediaStoreLocalVideoItem(
    override val name: String,
    override val duration: Long,
    override val size: Long,
    override val lastUpdated: Long,
    override val videoId: String,
    override val url: String = getVideoUriFromId(videoId = videoId),
): DataLayerLocalVideoItem(){

    companion object{
        fun getVideoIdFromUri(uri: String): String {
            return ContentUris.parseId(Uri.parse(uri)).toString()
        }
        fun getVideoUriFromId(videoId: String): String {
            return ContentUris.withAppendedId(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                videoId.toLong()
            ).toString()
        }

    }

    override fun toLocalVideoItem(context: Context): LocalVideoItem{

        return LocalVideoItem(
            videoUrl = this.url,
            videoName = this.name,
            videoDuration = humanReadableDuration(this.duration),
            videoSize = humanReadableSize(context, this.size),
            isPrivate = false
        )

    }


}


