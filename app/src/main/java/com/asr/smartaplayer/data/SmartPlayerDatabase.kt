package com.asr.smartaplayer.data

import android.net.Uri
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.DataLayerVideoCommentDao
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.DataLayerVideoMetadataDao
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.PrivateVideoDao
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.TransactionalPrivateVideoDao
import com.asr.smartaplayer.data.models.DataLayerVideoComment
import com.asr.smartaplayer.data.models.DataLayerVideoMetaData
import com.asr.smartaplayer.data.models.PrivateLocalVideoItem
import com.asr.smartaplayer.domain.models.AudioEqualizerSettings
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.models.VisualEffectsSettings
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter


@Database(entities = [PrivateLocalVideoItem::class, DataLayerVideoMetaData::class, DataLayerVideoComment::class], version = 16)
@TypeConverters(Converters::class)
abstract class SmartPlayerDatabase: RoomDatabase() {

    abstract fun localVideoMetadataDaoDao(): DataLayerVideoMetadataDao

    abstract fun privateVideoDao(): PrivateVideoDao

    abstract fun transactionalPrivateVideosDao(): TransactionalPrivateVideoDao

    abstract fun dataLayerVideoCommentsDao(): DataLayerVideoCommentDao

}

class Converters {

    @TypeConverter
    fun toVideoMetaData(videoMetaData: String): VideoMetaData{
        val typeToken = object: TypeToken<VideoMetaData>() {}.type
        val gson = GsonBuilder()
            .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
            .create()
        return gson.fromJson(videoMetaData, typeToken)
    }

    @TypeConverter
    fun fromVideoMetaData(videoMetaData: VideoMetaData): String{
        val gson = GsonBuilder()
            .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
            .create()
        return gson.toJson(videoMetaData)
    }


}





class UriTypeAdapter : TypeAdapter<Uri>() {
    override fun write(out: JsonWriter, value: Uri?) {
        out.value(value?.toString())
    }

    override fun read(`in`: JsonReader): Uri? {
        if (`in`.peek() == JsonToken.NULL) {
            `in`.nextNull()
            return null
        }
        return Uri.parse(`in`.nextString())
    }
}