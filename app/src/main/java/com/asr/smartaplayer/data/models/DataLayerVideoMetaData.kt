package com.asr.smartaplayer.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.asr.smartaplayer.domain.models.AudioEqualizerSettings
import com.asr.smartaplayer.domain.models.Subtitle
import com.asr.smartaplayer.domain.models.VideoMetaData
import com.asr.smartaplayer.domain.models.VisualEffectsSettings


@Entity
data class DataLayerVideoMetaData(
    @PrimaryKey
    val videoId: String,
    val videoMetaData: VideoMetaData
)



