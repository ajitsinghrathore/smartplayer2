package com.asr.smartaplayer.data.dataSources

import android.app.Activity
import android.app.RecoverableSecurityException
import android.content.ContentValues
import android.content.Context
import android.content.IntentSender
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import com.asr.smartaplayer.SmartPlayerApplication
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.PrivateVideoDao
import com.asr.smartaplayer.data.dataSources.localRoomDbDao.TransactionalPrivateVideoDao
import com.asr.smartaplayer.data.models.MediaStoreLocalVideoItem
import com.asr.smartaplayer.data.models.PrivateLocalVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.VideosSortPreference
import com.asr.smartaplayer.domain.utils.UseCaseTaskResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import java.io.File


sealed class VideoShiftingStatus<T> {
    data class Success<T>(val data: T) : VideoShiftingStatus<T>()
    data class Progress<T>(val progress: Float) : VideoShiftingStatus<T>()
}


class PrivateVideosDataSource(
    private val privateVideoDao: PrivateVideoDao,
    private val transactionalPrivateVideoDao: TransactionalPrivateVideoDao
) {


    suspend fun fetch(
        videosSortPreference: VideosSortPreference,
        offset: Int,
        limit: Int
    ): List<PrivateLocalVideoItem> {

        val sortIn = when (videosSortPreference.orderVideosIn) {
            OrderVideosIn.ASCENDING -> "ASC"
            OrderVideosIn.DESCENDING -> "DESC"
        }
        return when (videosSortPreference.orderVideosBy) {
            OrderVideosBy.NAME -> privateVideoDao.getPrivateDetailsAccordingToName(
                limit,
                offset,
                sortIn
            ) ?: emptyList()

            OrderVideosBy.SIZE -> privateVideoDao.getPrivateDetailsAccordingToSize(
                limit,
                offset,
                sortIn
            ) ?: emptyList()

            OrderVideosBy.LAST_MODIFIED -> privateVideoDao.getPrivateDetailsAccordingToLastUpdated(
                limit,
                offset,
                sortIn
            ) ?: emptyList()
        }
    }

    suspend fun getVideoDetails(videoId: String): PrivateLocalVideoItem? {
        return privateVideoDao.getVideoDetails(videoId = videoId)
    }

    private fun moveVideoToPrivateStorage(
        context: Context,
        videoItem: MediaStoreLocalVideoItem,
    ): Flow<VideoShiftingStatus<Uri?>> = callbackFlow {
        val targetFolder = context.filesDir
        val targetFile = File(targetFolder, videoItem.name)
        context.contentResolver.openInputStream(Uri.parse(videoItem.url))?.use { input ->
            context.contentResolver.openOutputStream(targetFile.toUri())?.use { output ->
                val bufferSize = 1024 * 4
                val buffer = ByteArray(bufferSize)
                var bytesRead: Int
                var totalBytesRead = 0L
                val fileSize = input.available().toLong()

                while (input.read(buffer).also { bytesRead = it } != -1) {
                    output.write(buffer, 0, bytesRead)
                    totalBytesRead += bytesRead
                    val progress = totalBytesRead.toFloat() / fileSize.toFloat()

                    trySend(VideoShiftingStatus.Progress(progress))
                }
            }
        }
        val resolver = context.contentResolver
        try {
            resolver.delete(Uri.parse(videoItem.url), null, null)
            trySend(VideoShiftingStatus.Success(targetFile.toUri()))
            close()
        } catch (e: Exception) {
            val intentSender: IntentSender? = when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                    MediaStore.createDeleteRequest(
                        resolver,
                        listOf(Uri.parse(videoItem.url))
                    ).intentSender
                }

                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                    val recoverableSecurityException = e as? RecoverableSecurityException
                    recoverableSecurityException?.userAction?.actionIntent?.intentSender
                }

                else -> null
            }

            fun callbackDef(result: ActivityResult?) {
                if (result?.resultCode == Activity.RESULT_OK) {
                    trySend(VideoShiftingStatus.Success(targetFile.toUri()))
                    close()
                } else {
                    targetFile.delete()
                    trySend(VideoShiftingStatus.Success(null))
                    close()
                }
            }

            val callback = ::callbackDef

            intentSender?.let {
                val intentSenderRequest = IntentSenderRequest.Builder(it).build()
                val application = context as SmartPlayerApplication

                application.executeOnIntentSender(
                    intentSenderRequest = intentSenderRequest,
                    callback = callback
                )
            }

        }
        awaitClose { }

    }


    fun makeMediaStoreVideoPrivate(
        context: Context,
        videoItem: MediaStoreLocalVideoItem
    ): Flow<UseCaseTaskResult<LocalVideoItem>> = flow {
        var updatedNewUriForVideo: Uri? = null
        val flow =  moveVideoToPrivateStorage(context, videoItem)
        flow.collect{
            when(it){
                is VideoShiftingStatus.Progress -> {
                    emit(UseCaseTaskResult.Progress(progress =it.progress))
                }
                is VideoShiftingStatus.Success ->{
                    updatedNewUriForVideo = it.data
                }
            }
        }

        Log.d("DRIVE*", "makeMediaStoreVideoPrivate:  Completed flow in data source ")
        if (updatedNewUriForVideo == null){
            emit(UseCaseTaskResult.Success(videoItem.toLocalVideoItem(context)))
            return@flow
        }
        val newPrivateVideoItem = PrivateLocalVideoItem(
            url = updatedNewUriForVideo.toString(),
            name = videoItem.name,
            duration = videoItem.duration,
            size = videoItem.size,
            lastUpdated = videoItem.lastUpdated
        )
        transactionalPrivateVideoDao.makeVideoPrivate(
            oldVideoItem = videoItem,
            newVideoItem = newPrivateVideoItem
        )
        emit(UseCaseTaskResult.Success(newPrivateVideoItem.toLocalVideoItem(context)))

    }



    fun makePrivateVideoPublic(
        context: Context,
        videoItem: PrivateLocalVideoItem
    ): Flow<UseCaseTaskResult<LocalVideoItem>> = flow {
        var updatedNewUriForVideo: Uri? = null
        moveVideoToMediaStore(context, videoItem).collect{
            when(it){
                is VideoShiftingStatus.Progress -> {
                    emit(UseCaseTaskResult.Progress(progress = it.progress))
                }
                is VideoShiftingStatus.Success ->{
                    Log.d("DRIVE*", "makePrivateVideoPublic: completed in data source $updatedNewUriForVideo")
                    updatedNewUriForVideo = it.data
                }
            }
        }
        Log.d("DRIVE*", "makePrivateVideoPublic: completed in data source $updatedNewUriForVideo")
        if (updatedNewUriForVideo == null){
            emit(UseCaseTaskResult.Success(videoItem.toLocalVideoItem(context)))
            return@flow
        }
        val newMediaStorePublicVideo = MediaStoreLocalVideoItem(
            url = updatedNewUriForVideo.toString(),
            name = videoItem.name,
            duration = videoItem.duration,
            size = videoItem.size,
            lastUpdated = videoItem.lastUpdated,
            videoId = MediaStoreLocalVideoItem.getVideoIdFromUri(updatedNewUriForVideo.toString())
        )
        transactionalPrivateVideoDao.makePrivateVideoPublic(
            oldVideoItem = videoItem,
            newVideoItem = newMediaStorePublicVideo
        )
        emit(UseCaseTaskResult.Success(newMediaStorePublicVideo.toLocalVideoItem(context)))

    }

    private fun moveVideoToMediaStore(
        context: Context,
        privateVideoItem: PrivateLocalVideoItem
    ): Flow<VideoShiftingStatus<Uri?>> = flow {
        val videoFile = DocumentFile.fromSingleUri(context, Uri.parse(privateVideoItem.url))
        val videoFileName = privateVideoItem.name
        val videoMimeType = videoFile?.type ?: "video/mp4"
        val contentValues = ContentValues().apply {
            put(MediaStore.Video.Media.DISPLAY_NAME, videoFileName)
            put(MediaStore.Video.Media.MIME_TYPE, videoMimeType)
            put(MediaStore.Video.Media.DATE_ADDED, privateVideoItem.lastUpdated)
            put(MediaStore.Video.Media.DATE_TAKEN, privateVideoItem.lastUpdated)
            put(MediaStore.Video.Media.DATE_MODIFIED, privateVideoItem.lastUpdated)
        }

        Log.d("TAG___", "moveVideoToMediaStore: $contentValues $privateVideoItem")
        val resolver = context.contentResolver

        val mediaUri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues)
        if (mediaUri == null){
            emit(VideoShiftingStatus.Success(null))
            return@flow
        }
        resolver.openOutputStream(mediaUri)?.use { output ->
            context.contentResolver.openInputStream(Uri.parse(privateVideoItem.url))?.use { input ->
                val bufferSize = 1024 * 4
                val buffer = ByteArray(bufferSize)
                var bytesRead: Int
                var totalBytesRead = 0L
                val fileSize = input.available().toLong()

                while (input.read(buffer).also { bytesRead = it } != -1) {
                    output.write(buffer, 0, bytesRead)
                    totalBytesRead += bytesRead
                    val progress = totalBytesRead.toFloat() / fileSize.toFloat()

                    emit(VideoShiftingStatus.Progress(progress))
                }
            }
        }
        Log.d("DRIVE*", "moveVideoToMediaStore: comleted moving in core")
        val deleted = File(Uri.parse(privateVideoItem.url).path!!).delete()
        Log.d("DRIVE*", "moveVideoToMediaStore: $deleted")
        emit(
            VideoShiftingStatus.Success(mediaUri)
        )
    }


}