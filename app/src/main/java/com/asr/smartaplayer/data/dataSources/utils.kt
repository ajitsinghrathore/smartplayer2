package com.asr.smartaplayer.data.dataSources

import android.util.Log
import com.asr.smartaplayer.data.models.DataLayerLocalVideoItem
import com.asr.smartaplayer.data.models.MediaStoreLocalVideoItem
import com.asr.smartaplayer.data.models.PrivateLocalVideoItem
import com.asr.smartaplayer.domain.models.OrderVideosBy
import com.asr.smartaplayer.domain.models.OrderVideosIn
import com.asr.smartaplayer.domain.models.VideosSortPreference


fun mergeMediaStoreAndPrivateVideos(
    mediaStoreVideos: List<MediaStoreLocalVideoItem>,
    privateVideos: List<PrivateLocalVideoItem>,
    size: Int,
    videosSortPreference: VideosSortPreference
): List<DataLayerLocalVideoItem> {

    val finalCombinedVideos = mutableListOf<DataLayerLocalVideoItem>()

    var privateVideoPointer = 0
    var mediaStoreVideoPointer = 0

    Log.d("TAG____", "mergeMediaStoreAndPrivateVideos: $privateVideos")


    while (
        finalCombinedVideos.size < size &&
        privateVideoPointer < privateVideos.size &&
        mediaStoreVideoPointer < mediaStoreVideos.size
    ) {
        val privateVideo = privateVideos[privateVideoPointer]
        val mediaStoreVideo = mediaStoreVideos[mediaStoreVideoPointer]

        val comparisonResult = compareVideos(
            mediaStoreVideo = mediaStoreVideo,
            privateVideo = privateVideo,
            sortPreference = videosSortPreference
        )

        if (videosSortPreference.orderVideosIn == OrderVideosIn.DESCENDING) {
            if (comparisonResult >= 0) {
                finalCombinedVideos.add(mediaStoreVideo)
                mediaStoreVideoPointer += 1
                Log.d("TAG____", "mergeMediaStoreAndPrivateVideos: mediastore taken ${mediaStoreVideo.lastUpdated} ${privateVideo.lastUpdated}")
            } else {
                finalCombinedVideos.add(privateVideo)
                privateVideoPointer += 1
                Log.d("TAG____", "mergeMediaStoreAndPrivateVideos: private takens  ${mediaStoreVideo.lastUpdated}  ${privateVideo.lastUpdated}")
            }
        } else {
            if (comparisonResult >= 0) {
                finalCombinedVideos.add(privateVideo)
                privateVideoPointer += 1
            } else {
                finalCombinedVideos.add(mediaStoreVideo)
                mediaStoreVideoPointer += 1
            }
        }
    }


    // Add any remaining elements from privateVideos
    while (privateVideoPointer < privateVideos.size && finalCombinedVideos.size < size) {
        finalCombinedVideos.add(privateVideos[privateVideoPointer])
        privateVideoPointer++
    }

    // Add any remaining elements from mediaStoreVideos
    while (mediaStoreVideoPointer < mediaStoreVideos.size && finalCombinedVideos.size < size) {
        finalCombinedVideos.add(mediaStoreVideos[mediaStoreVideoPointer])
        mediaStoreVideoPointer++
    }

    return finalCombinedVideos
}


private fun compareVideos(
    mediaStoreVideo: MediaStoreLocalVideoItem,
    privateVideo: PrivateLocalVideoItem,
    sortPreference: VideosSortPreference
): Int {

    return when (sortPreference.orderVideosBy) {
        OrderVideosBy.NAME -> mediaStoreVideo.name.compareTo(privateVideo.name)
        OrderVideosBy.SIZE -> mediaStoreVideo.size.compareTo(privateVideo.size)
        OrderVideosBy.LAST_MODIFIED -> mediaStoreVideo.lastUpdated.compareTo(privateVideo.lastUpdated)
    }

}