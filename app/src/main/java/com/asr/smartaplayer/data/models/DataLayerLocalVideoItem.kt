package com.asr.smartaplayer.data.models

import android.content.Context
import android.text.format.Formatter
import com.asr.smartaplayer.domain.models.BaseVideoItem
import com.asr.smartaplayer.domain.models.LocalVideoItem
import java.util.TimeZone

abstract class DataLayerLocalVideoItem {
    abstract val videoId: String
    abstract val url: String
    abstract val name: String
    abstract val duration: Long
    abstract val size: Long
    abstract val lastUpdated: Long

    abstract fun toLocalVideoItem(context: Context): LocalVideoItem

}








