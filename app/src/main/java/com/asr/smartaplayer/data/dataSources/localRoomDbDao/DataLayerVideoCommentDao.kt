package com.asr.smartaplayer.data.dataSources.localRoomDbDao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.asr.smartaplayer.data.models.DataLayerVideoComment

@Dao
interface DataLayerVideoCommentDao {

    @Query("SELECT * FROM DataLayerVideoComment where videoId = :videoId order by position ASC")
    suspend fun getVideoComments(videoId: String): List<DataLayerVideoComment>

    @Query("UPDATE DataLayerVideoComment set videoId =:newId where videoId =:oldId")
    suspend fun updateVideoIdsForComments(oldId: String, newId: String)


    @Query("DELETE FROM DataLayerVideoComment where videoId =:videoId and position = :position")
    suspend fun deleteCommentInVideo(videoId: String, position: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addVideoComment(dataLayerVideoComment: DataLayerVideoComment)
}