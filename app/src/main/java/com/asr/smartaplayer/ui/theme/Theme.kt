package com.asr.smartaplayer.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val darkColorScheme = darkColorScheme(
    primary = brandColor,
    onPrimary = black,
    secondary = PurpleGrey80,
    tertiary = Pink80,
    background = black,
    onBackground = White,
    surface = surfaceColor,
    inverseSurface = inverseSurfaceColor,
    inversePrimary = lightBrandColor,
    inverseOnSurface = inverseOnSurfaceColor,
    onSurface = White,
    onSurfaceVariant = brandColor
)

private val lightColorScheme = lightColorScheme(
    primary = brandColor,
    onPrimary = black,
    secondary = PurpleGrey80,
    tertiary = Pink80,
    background = White,
    onBackground = black,
    surface = lightSurfaceColor,
    inverseSurface = lightInverseSurfaceColor,
    inversePrimary = lightBrandColor,
    inverseOnSurface = lightInverseOnSurfaceColor,
    onSurface = black,
    onSurfaceVariant = Color.Blue
)

@Composable
fun SmartPlayerAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    dynamicColor: Boolean = false,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> darkColorScheme
        else -> lightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.background.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = AppTypography,
        content = content
    )
}