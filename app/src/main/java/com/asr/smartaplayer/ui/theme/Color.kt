package com.asr.smartaplayer.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val black = Color(0xFF000000)
val brandColor =  Color(0xFFF9AA33)
val lightBrandColor =  Color(0x80F9AA33)
val surfaceColor = Color(0xFF1F1E1E)
val inverseSurfaceColor = Color(0xFFD1C4E9)
val inverseOnSurfaceColor = Color(0xFF333333)
val lightWhite = Color(0xCCFFFFFF)
val White = Color(0xFFFFFFFF)

val lightSurfaceColor =  Color(0xFFC9C6C6)
val lightInverseSurfaceColor =  Color(0xFF222121)
val lightInverseOnSurfaceColor =  Color(0xFFBEBABA)
val orange = Color(0xFFFFA500)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)